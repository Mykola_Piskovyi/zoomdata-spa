var path = require('path');
var webpack = require('webpack');
var Clean = require('clean-webpack-plugin');
var Copy = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var IS_PROD = process.env.NODE_ENV === 'production';

var INPUT_FILE = './src/index.ts';
var OUT_PATH = 'dist';
var INPUT_FOLDER = './src/';

module.exports = {
	entry: {
		Core: ['./src/Core/index'],
		Api: ['./src/Api/index'],
		Webmodules: ['./src/Webmodules/index']
	},

	output: {
		path: path.join(__dirname, OUT_PATH),
		filename: '[name]/index.js',
		library: ['[name]'],
		libraryTarget: 'umd'
	},

	resolve: {
		root: path.resolve(__dirname),
		extensions: ['.ts', '.js', '.tsx', '.jsx', '.es6', '.json', ''],
		alias: {
			sinon: 'sinon/pkg/sinon'
		}
	},

	module: {
		loaders: [{
			test: /\.js$|\.jsx$|\.es6$/,
			exclude: /node_modules/,
			loaders: ['babel'],
		}, {
			test: /\.ts$|\.tsx$/,
			exclude: /node_modules/,
			loaders: ['awesome-typescript-loader'],
		}, {
			test: /\.svg/,
			loader: 'svg-url-loader'
		}, {
			test: /\.scss$/,
			exclude: /node_modules/,
			loader: IS_PROD ? 'style!css!sass' : ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader')
		}],
		noParse: [
			/sinon/
		]
	},

	externals: {
		'react': 'react',
		'react-dom': 'react-dom',
		'zd-components': 'zd-components',
		'moment': 'moment',
		'backbone': 'backbone',
		'jquery': 'jquery',
		'rx': 'rx',
		'promise': 'promise',
		'underscore': 'underscore'
	},

	plugins: [
		// clean all data in dist before
		new Clean([OUT_PATH]),
		new webpack.optimize.UglifyJsPlugin({minimize: true, compress: {warnings: false}}),
		new webpack.optimize.DedupePlugin(),
		new Copy([
			{ from: './src/Core/package.json', to: 'Core' },
			{ from: './src/Api/package.json', to: 'Api' },
			{ from: './src/Webmodules/package.json', to: 'Webmodules' },
			{ from: './src/index.d.ts' },
			{ from: './src/index.js' }
		]),
		new webpack.DefinePlugin({
			'process.env': {'NODE_ENV': JSON.stringify(process.env.NODE_ENV)}
		})
	]

};
