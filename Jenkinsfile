branchPath = "${env.JOB_NAME}".replace('%2F', '-')

def notifyBuild(String buildStatus) {
    // build status of null means successful
    buildStatus = buildStatus ?: 'SUCCESS'

    // Default values
    def subject = "${buildStatus}: Job '${branchPath} [${env.BUILD_NUMBER}]'"
    def summary = "${subject} (${env.BUILD_URL})"
    def slackChannel = "#zdc-jenkins"
    def slackToken = "d642d1f6-51a6-4a76-ac3f-bc0425d52cf2"

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        colorCode = '#FFFF00'
        bitbucketStatus = 'INPROGRESS'
    } else if (buildStatus == 'SUCCESS') {
        colorCode = '#00FF00'
        bitbucketStatus = 'SUCCESSFUL'
    } else {
        colorCode = '#FF0000'
        bitbucketStatus = 'FAILED'
    }

    // Send notifications
    slackSend (tokenCredentialId: slackToken, channel: slackChannel, color: colorCode, message: summary)
    bitbucketStatusNotify(buildState: bitbucketStatus)
}

node {
    notifyBuild('STARTED')
    ws("workspace/${branchPath}") {
        stage('preparation') {
            step([$class: 'WsCleanup'])
            checkout scm
            env.GIT_COMMIT = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
        }

        try {
            stage('build and test') {
               withEnv(["PATH=${tool 'node-5.11.1'}/bin:${env.PATH}"]) {
                    print "PATH is : ${env.PATH}"
                    sh "npm cache clean"
                    sh "npm install backbone@1.3.3"
                    sh "npm install backbone.marionette@3.0.0"
                    sh "npm install"
                    sh "npm run test"
                }
                currentBuild.result = 'SUCCESS'
            }
        } catch (e) {
            // If there was an exception thrown, the build failed
            currentBuild.result = "FAILED"
            throw e
        } finally {
            notifyBuild(currentBuild.result)
        }

        stage('report') {
            junit testResults: 'dist/reports/*.xml', allowEmptyResults: true

            step([$class: 'CoberturaPublisher',
                  coberturaReportFile: 'dist/reports/coverage/cobertura/cobertura.xml',
                  failNoReports: false])
        }
    }
}
