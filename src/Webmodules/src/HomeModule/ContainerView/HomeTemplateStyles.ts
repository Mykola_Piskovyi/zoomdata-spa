
export default {
    'zdView-ContainerView': {

        'text-rendering': 'optimizeLegibility',
        'font-smoothing': 'antialiased',

        '& .main-content-container': {
            '&.initial': {
                position: 'absolute',
                height: '100%',
                width: '100%'
            }
        },

    },

    'options-pane-nav': {

    },

    'divide-line': {
        'margin': '5px 0',
        'border-top': 'none',
        'height': '1px',
        'background-color': '#000000',
        'opacity': '0.20',
        'border-bottom': '1px solid #ffffff'
    },

    'zdView-ContentComponent': {
        position: 'relative',
        height: 'inherit',

        '& .main-content-container-body': {
            overflow: 'auto',
            '-ms-overflow-style': 'none',

            '&::-webkit-scrollbar': {
                '-webkit-appearance': 'none',
                width: 0
            }
        }
    }
};
