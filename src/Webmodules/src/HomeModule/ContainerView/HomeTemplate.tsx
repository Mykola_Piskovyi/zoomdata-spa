/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import {isEmpty} from 'lodash';
import * as React from 'react';

const { Theme } = require('zd-components');
const classNames = require('classnames');

import AddNew, {IAddNewProps} from '../components/AddNew/AddNew';
import FastFilters, {IFastFiltersProps} from '../components/FastFilters/FastFilters';
import SearchFilter, {ISearchFilterProps} from '../components/SearchFilter/SearchFilter';
import ContentTop, {IContentTopProps} from '../components/ContentTop/ContentTop';
import ContentBody, {IContentBodyProps} from '../components/ContentBody/ContentBody';
import LoadingSpinner from '../components/LoadingSpinner/LoadingSpinner';

import {injectSheet} from '../../utilities/jssSetup/JssUtils';
import HomeTemplateStyles from './HomeTemplateStyles';
import { TEmptyState } from '../model/types';
import NoCharts from './EmptyState/NoCharts/NoCharts';
import NoDataSource from './EmptyState/NoDataSource/NoDataSource';
import NoAccess from './EmptyState/NoAccess/NoAccess';

import './styles.scss';

const LEFT_PANE_W = 145;

const leftPanelStyles = {
    'position': 'absolute',
    'boxSizing': 'borderBox',
    'width': LEFT_PANE_W,
    'height': '100%',
    'backgroundColor': '#595959',
    'borderRadius': '0',
    'fontSize': '14px'
};

const mainContainerStyles = {
    'position': 'absolute',
    'width': `calc(100% - ${LEFT_PANE_W}px)`,
    'left': LEFT_PANE_W
};

export interface IHomeProps extends
    IAddNewProps,
    IFastFiltersProps,
    ISearchFilterProps,
    IContentTopProps,
    IContentBodyProps {
    sheet?: any;
    sources: Array<any>;
    pending: boolean;
    initial: boolean;
    emptyState: TEmptyState;
}

export class HomeTemplate extends React.Component<IHomeProps, {}> {

    private _getInitialContent(pending) {
        const {classes} = this.props.sheet;
        const { noAccess } = this.props.emptyState;

        let initialContent = pending ? <LoadingSpinner /> : <NoDataSource />;

        if ( noAccess ) {
            initialContent = <NoAccess />;
        }

        return (
            <Theme>
                <div className={classes['zdView-ContainerView']}>
                    <div className='main-content-container initial'>
                        { initialContent }
                    </div>
                </div>
            </Theme>
        );
    }

    private _getContentContainer(noDataSource, noCharts, ContentComponent) {
        if (noDataSource) {
            return <NoDataSource />;
        } else if (noCharts) {
            return <NoCharts />;
        } else {
            return ContentComponent;
        }
    }

    render() {
        const { noCharts, noDataSource } = this.props.emptyState;
        const {addNewItems, pending, initial, layout} = this.props;

        const addNewProps: IAddNewProps = {
            addNewItems: addNewItems,
            onNewDialogItemClick: this.props.onNewDialogItemClick,
            onPopoverItemClick: this.props.onPopoverItemClick,
            sources: this.props.sources
        };

        const fastFilterProps: IFastFiltersProps = {
            route: this.props.route,
            fastFilters: this.props.fastFilters,
            onFilterClick: this.props.onFilterClick,
        };

        const searchProps: ISearchFilterProps = {
            searchLabel: this.props.searchLabel,
            onSearch: this.props.onSearch
        };

        const bodyProps: IContentBodyProps = {
            route: this.props.route,
            layout: this.props.layout,
            pending: this.props.pending,
            dataItems: this.props.dataItems,
            onItemClick: this.props.onItemClick,
            onItemDelete: this.props.onItemDelete,
            onItemFavorite: this.props.onItemFavorite,
            onSortChange: this.props.onSortChange,
            emptyState: this.props.emptyState,
            sortObject: this.props.sortObject
        };

        const topProps: IContentTopProps = {
            hideTopSwitcher: this.props.hideTopSwitcher,
            fastFilter: this.props.fastFilter,
            sortObject: this.props.sortObject,
            onLayoutChange: this.props.onLayoutChange,
            onSortChange: this.props.onSortChange,
            layout: this.props.layout
        };

        const { classes } = this.props.sheet;

        if (initial) {
            return this._getInitialContent(pending);
        } else {

            const ContentComponent = (
                <div className={ classes['zdView-ContentComponent'] }>
                    <div className='main-content-container-top'>
                        <ContentTop {...topProps} />
                    </div>
                    <div className='main-content-container-body'>
                        <ContentBody {...bodyProps} />
                    </div>
                </div>
            );

            return (
                <Theme>
                    <div
                        className={ classNames(classes['zdView-ContainerView'], `${ layout }-layout`) }>
                        <div className='options-pane-nav'
                             style={leftPanelStyles}>
                            {!isEmpty(addNewItems) ?
                                <AddNew {...addNewProps} /> : null}
                            <FastFilters {...fastFilterProps} />
                            <hr className={classes['divide-line']}/>
                            <SearchFilter {...searchProps} />
                        </div>
                        <div className='main-content-container'
                             style={mainContainerStyles}>
                            {this._getContentContainer(noDataSource, noCharts, ContentComponent)}
                        </div>
                    </div>
                </Theme>
            );

        }
    }
}

export default injectSheet(HomeTemplateStyles)(HomeTemplate);
