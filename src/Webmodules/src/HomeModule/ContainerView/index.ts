/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import ContainerView from './ContainerView';
export * from './ContainerView';
export default ContainerView;
