import {assign} from 'lodash';
import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';

import {ADD_NEW_DIALOG_ITEMS, FAST_FILTERS} from '../../SETTINGS';
import {ESort, ESortOrder, ELayout, EFastFilterValue} from '../../model/types';

import HomeTemplate from '../HomeTemplate';

import NoDataSource from '../EmptyState/NoDataSource/NoDataSource';
import NoCharts from '../EmptyState/NoCharts/NoCharts';
import NoAccess from '../EmptyState/NoAccess/NoAccess';

import LoadingSpinner from '../../components/LoadingSpinner/LoadingSpinner';

import AddNew from '../../components/AddNew/AddNew';
import FastFilters from '../../components/FastFilters/FastFilters';
import SearchFilter from '../../components/SearchFilter/SearchFilter';
import ContentTop from '../../components/ContentTop/ContentTop';
import ContentBody from '../../components/ContentBody/ContentBody';

describe('HomeTemplate Unit Tests: ', () => {

    let sandbox;
    let props;
    let wrapper;
    let emptyState;

    const getProps = (props) => {
        const FAST_FILTER = FAST_FILTERS[0];
        return assign({}, {
            layout: ELayout.CARDS,
            pending: false,
            hideTopSwitcher: false,
            sortObject: {
                sort: ESort.MODIFIED,
                sortOrder: ESortOrder.DESC
            },
            fastFilter: FAST_FILTER,
            fastFilters: FAST_FILTERS,
            addNewItems: ADD_NEW_DIALOG_ITEMS,
            route: FAST_FILTER.value,
            searchLabel: FAST_FILTER.searchLabel,
            dataItems: [],
            emptyState: {
                noAccess: false,
                noDataSource: false,
                noCharts: false,
                noRequestedItems: false,
                noSearchResults: false
            },
            initial: false,
            sources: [],
            onFilterClick: sandbox.stub(),
            onSearch: sandbox.stub(),
            onNewDialogItemClick: sandbox.stub(),
            onLayoutChange: sandbox.stub(),
            onSortChange: sandbox.stub(),
            onItemClick: sandbox.stub(),
            onPopoverItemClick: sandbox.stub(),
            onItemDelete: sandbox.stub(),
            onItemFavorite: sandbox.stub(),
            sheet: {
                classes: {
                    'zdView-ContentComponent': {}
                }
            }
        }, props);
    };

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        props = getProps();

        wrapper = mount(<HomeTemplate {...props}/>);
    });

    afterEach(() => {
        sandbox.restore();
        wrapper.unmount();
    });

    it('should be defined: ', () => {
        expect(HomeTemplate).to.exist;
        expect(wrapper).to.exist;
    });

    describe('Rendering "initial: true" cases: ', () => {

        beforeEach(() => {
            emptyState = getProps().emptyState;
        });

        it ('should render "LoadingSpinner" for initial render and pending: ', () => {
            props = getProps({initial: true, pending: true});
            wrapper = mount(<HomeTemplate {...props} />);

            expect(wrapper.find(LoadingSpinner)).to.have.length(1);

            expect(wrapper.find(NoDataSource)).to.have.length(0);
            expect(wrapper.find(AddNew)).to.have.length(0);
            expect(wrapper.find(ContentTop)).to.have.length(0);
            expect(wrapper.find(ContentBody)).to.have.length(0);
        });

        it ('should render "NoDataSource" for initial render: ', () => {
            props = getProps({initial: true, pending: false});
            wrapper = mount(<HomeTemplate {...props} />);

            expect(wrapper.find(NoDataSource)).to.have.length(1);

            expect(wrapper.find(LoadingSpinner)).to.have.length(0);
            expect(wrapper.find(AddNew)).to.have.length(0);
            expect(wrapper.find(ContentTop)).to.have.length(0);
            expect(wrapper.find(ContentBody)).to.have.length(0);
        });

        it ('should render "NoAccess" for initial render if user has no access (disabled): ', () => {
            props = getProps({
                initial: true,
                emptyState: assign({}, emptyState, {noAccess: true})
            });
            wrapper = mount(<HomeTemplate {...props}/>);

            expect(wrapper.find(NoAccess)).to.have.length(1);

            expect(wrapper.find(LoadingSpinner)).to.have.length(0);
            expect(wrapper.find(AddNew)).to.have.length(0);
            expect(wrapper.find(ContentTop)).to.have.length(0);
            expect(wrapper.find(ContentBody)).to.have.length(0);
        });
    });

    describe('Rendering "emptyState" cases and LeftPane for all: ', () => {

        beforeEach(() => {
            emptyState = getProps().emptyState;
        });

        it ('should render LeftPane, ContentBody, ContentTop: ', () => {
            expect(wrapper.find(NoDataSource)).to.have.length(0);
            expect(wrapper.find(NoCharts)).to.have.length(0);
            expect(wrapper.find('.options-pane-nav')).to.have.length(1);
            expect(wrapper.find(ContentTop)).to.have.length(1);
            expect(wrapper.find(ContentBody)).to.have.length(1);
        });

        it ('should render LeftPane, "NoDataSource" for noDataSource state: ', () => {
            props = getProps({
                emptyState: assign({}, emptyState, {noDataSource: true})
            });
            wrapper = mount(<HomeTemplate {...props}/>);

            expect(wrapper.find(NoDataSource)).to.have.length(1);
            expect(wrapper.find(NoCharts)).to.have.length(0);
            expect(wrapper.find('.options-pane-nav')).to.have.length(1);
        });

        it ('should render LeftPane, "NoDataSource" for noDataSource and noCharts state: ', () => {
            props = getProps({
                emptyState: assign({}, emptyState, {noDataSource: true, noCharts: true})
            });
            wrapper = mount(<HomeTemplate {...props}/>);

            expect(wrapper.find(NoDataSource)).to.have.length(1);
            expect(wrapper.find(NoCharts)).to.have.length(0);
            expect(wrapper.find('.options-pane-nav')).to.have.length(1);
        });

        it ('should render LeftPane, "NoCharts" for noCharts state: ', () => {
            props = getProps({
                emptyState: assign({}, emptyState, {noCharts: true})
            });
            wrapper = mount(<HomeTemplate {...props}/>);

            expect(wrapper.find(NoDataSource)).to.have.length(0);
            expect(wrapper.find(NoCharts)).to.have.length(1);
            expect(wrapper.find('.options-pane-nav')).to.have.length(1);
        });

    });
});
