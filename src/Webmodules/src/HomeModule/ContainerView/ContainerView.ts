/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import * as React from 'react';
import {View} from '../../../../Core';

import {ELayout, TDataItem, TSortByType, EAddNewValue} from '../model/types';
import HomeTemplate, {IHomeProps} from './HomeTemplate';

export interface SRenderViewOptions extends IHomeProps {

}

const EVENTS = {
    CHANGE_ROUTE: 'changeRoute',
    SEARCH: 'onSearch',
    NEW_DIALOG_EVENT: 'onNewDialogEvent',
    LAYOUT_CHANGE: 'onLayoutChange',
    SORT_CHANGE: 'onSortChange',
    CHART_OPEN: 'onChartOpen',
    VIS_OPEN: 'onVisualizationOpen',
    CHART_DELETE: 'onChartDelete',
    CHART_FAVORITE: 'onChartFavorite'
};

class HomeView extends View {

    static EVENTS = EVENTS;
    public EVENTS = EVENTS;

    render(options: SRenderViewOptions): HomeView {
        this.renderComponent(HomeTemplate, this._getHomeProps(options));
        return this;
    }

    private _getHomeProps(options): IHomeProps {
        return {
            layout: options.layout,
            pending: options.pending,
            hideTopSwitcher: options.hideTopSwitcher,
            sortObject: options.sortObject,
            fastFilter: options.fastFilter,
            fastFilters: options.fastFilters,
            addNewItems: options.addNewItems,
            route: options.route,
            searchLabel: options.searchLabel,
            dataItems: options.dataItems,
            emptyState: options.emptyState,
            initial: options.initial,
            sources: options.sources,
            onFilterClick: (value: string) => this.trigger(EVENTS.CHANGE_ROUTE, value),
            onSearch: (value: string) => this.trigger(EVENTS.SEARCH, value),
            onNewDialogItemClick: (value: EAddNewValue) => this.trigger(EVENTS.NEW_DIALOG_EVENT, value),
            onLayoutChange: (value: ELayout) => this.trigger(EVENTS.LAYOUT_CHANGE, value),
            onSortChange: (sortType: TSortByType) => this.trigger(EVENTS.SORT_CHANGE, sortType),
            onItemClick: (item: TDataItem) => this.trigger(EVENTS.CHART_OPEN, item),
            onPopoverItemClick: (item: TDataItem) => this.trigger(EVENTS.VIS_OPEN, item),
            onItemDelete: (item: TDataItem) => this.trigger(EVENTS.CHART_DELETE, item),
            onItemFavorite: (item: TDataItem) => this.trigger(EVENTS.CHART_FAVORITE, item)
        };
    }

}

export default HomeView;
