/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';

import NoAccess from '../NoAccess';
import {TEXT_DATA} from '../../../../SETTINGS';

describe('NoAccess Unit Tests: ', () => {

    let wrapper;
    beforeEach(() => {
        wrapper = mount(<NoAccess />);
    });

    afterEach(() => {
        wrapper.unmount();
    });

    it('should render NO_DATA_SOURCE desc', () => {
        expect(wrapper.find('.description').text()).to.equal(TEXT_DATA.NO_ACCESS.DESC);
    });

});
