/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';
import {injectSheet} from '../../../../utilities/jssSetup/JssUtils';

import { TEXT_DATA } from '../../../SETTINGS';
import EmptyStateStyles from '../EmptyStateStyles';

const classnames = require('classnames');

export interface INoAccessProps {
    sheet?: any;
}

export const NoAccess = (props: INoAccessProps) => {
    const { classes } = props.sheet;
    const { NO_ACCESS } = TEXT_DATA;

    return (
        <div className={ classnames(classes['zdView-EmptyState'], 'no-access-view') }>
            <div className='description'>
                { NO_ACCESS.DESC }
            </div>
        </div>
    );
};

export default injectSheet(EmptyStateStyles)(NoAccess);
