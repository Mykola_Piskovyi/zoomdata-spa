/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import * as React from 'react';
import './NoCharts.scss';

import { injectSheet } from '../../../../utilities/jssSetup/JssUtils';
import NoChartsStyles from './NoChartsStyles';
import { TEXT_DATA } from '../../../SETTINGS';

export interface INoChartsProps {
    sheet?: any;
}

export const NoCharts = (props: INoChartsProps) => {
    const { classes } = props.sheet;
    const { NO_CHARTS } = TEXT_DATA;

    return (
        <div className={classes['zdView-NoCharts']}>
            <div className='helper-image'>
                <div className='add-chart-description'>{ NO_CHARTS.DESC }</div>
            </div>
            <a target='_blank' className='more-info-title' href={ NO_CHARTS.LINK }>{ NO_CHARTS.LABEL }</a>
        </div>
    );
};

export default injectSheet(NoChartsStyles)(NoCharts);
