/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

export default {
    'zdView-NoCharts': {
        width: '356px',
        height: 'auto',
        margin: '13px 0 0 5px',
        position: 'relative',
        fontFamily: 'Source Sans Pro, sans-serif',

        '& .helper-image': {
            position: 'relative',
            width: '100%',
            height: '140px',
            backgroundRepeat: 'no-repeat',

            '& .add-chart-description': {
                width: '242px',
                height: '72px',
                fontWeight: '300',
                color: '#323232',
                fontSize: '33px',
                lineHeight: '34px',
                textAlign: 'center',
                position: 'absolute',
                top: '34px',
                right: '0px'
            }
        },

        '& .more-info-title': {
            marginLeft: '185px',
            fontWeight: '100',
            color: '#0095b6',
            fontSize: '14px',
            lineHeight: '18px',
            cursor: 'pointer'
        }
    }
};
