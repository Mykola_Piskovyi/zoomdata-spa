/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

const NO_SEARCH_SETTINGS = {
    icon: 'search',
    description: 'No results found',
    help: 'Try another Search?'
};
export { NO_SEARCH_SETTINGS };
