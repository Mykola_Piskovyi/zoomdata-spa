/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import React from 'react';
import { find } from 'lodash';
import { shallow, mount } from 'enzyme';
import NoSearchResults from '../NoSearchResults';
import { NO_SEARCH_SETTINGS } from '../NoSearchResults.settings';

describe('NoSearchResults Unit Tests: ', ()=> {
    let wrapper;

    describe('WHEN initialize: ', () => {
        it('should exist', () => {
            wrapper = shallow(<NoSearchResults />);
            expect(wrapper).to.exist;
            wrapper.unmount();
        });
    });

    describe('WHEN render', () => {
        it('should render correct description and help', () => {
            const { description, help } = NO_SEARCH_SETTINGS;

            wrapper = mount(<NoSearchResults />);
            expect(wrapper.find('.description').text()).to.equal(description);
            expect(wrapper.find('.help').text()).to.equal(help);
            wrapper.unmount();
        });
    });
});
