/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';
import {injectSheet} from '../../../../utilities/jssSetup/JssUtils';
import EmptyStateStyles from '../EmptyStateStyles';
import { NO_SEARCH_SETTINGS } from './NoSearchResults.settings';

const { SvgIcon } = require('zd-components');

export interface INoSearchResultsProps {
    sheet?: any;
    description?: string;
    help?: string;
    icon?: string;
}

export const NoSearchResults: React.SFC<INoSearchResultsProps> = props => {
    const { classes } = props.sheet;
    const { description, help, icon } = props;

    return (
        <div className={ classes['zdView-EmptyState'] }>
            <SvgIcon className='zd-icon' name={ icon } />
            <div className='description'>{ description }</div>
            <div className='help'>{ help }</div>
        </div>
    );
};


NoSearchResults.defaultProps = NO_SEARCH_SETTINGS;

export default injectSheet(EmptyStateStyles)(NoSearchResults);
