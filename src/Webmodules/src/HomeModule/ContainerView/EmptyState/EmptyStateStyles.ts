/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

export default {
    'zdView-EmptyState': {
        width: '400px',
        position: 'relative',
        left: 'calc(50% - 200px)',
        textAlign: 'center',
        top: 'calc(50% - 110px)',

        '& .zd-icon': {
            width: '86px',
            height: '86px',
            color: '#b2b2b2',
            paddingLeft: '5px'
        },

        '& .description': {
            fontWeight: 100,
            color: '#323232',
            fontSize: '32px',
            lineHeight: '34px',
            marginTop: '20px'
        },

        '& .help': {
            fontWeight: 100,
            color: '#595959',
            fontSize: '18px',
            lineHeight: '22px',
            textAlign: 'center',
            marginTop: '6px'
        },

        '& .more-info-link': {
            fontWeight: '100',
            color: '#0095b6',
            fontSize: '14px',
            lineHeight: '18px',
            cursor: 'pointer',
            marginTop: '6px',
            display: 'inline-block'
        },

        '&.no-access-view': {
            width: '560px',
            top: 'calc(50% - 35px)',
            left: 'calc(50% - 280px)'
        }
    }
};
