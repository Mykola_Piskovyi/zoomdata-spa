/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';

const { SvgIcon } = require('zd-components');

import NoDataSource from '../NoDataSource';
import {TEXT_DATA} from '../../../../SETTINGS';

describe('NoDataSource Unit Tests: ', () => {

    let wrapper;
    beforeEach(() => {
        wrapper = mount(<NoDataSource />);
    });

    afterEach(() => {
        wrapper.unmount();
    });

    it('should render SvgIcon', () => {
        expect(wrapper.find(SvgIcon)).to.have.length(1);
    });

    it('should render NO_DATA_SOURCE title and desc', () => {
        expect(wrapper.find('.description').text()).to.equal(TEXT_DATA.NO_DATA_SOURCE.TITLE);
        expect(wrapper.find('.help').text()).to.equal(TEXT_DATA.NO_DATA_SOURCE.DESC);
    });

});
