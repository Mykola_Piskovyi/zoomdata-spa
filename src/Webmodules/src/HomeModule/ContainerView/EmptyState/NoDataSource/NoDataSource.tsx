/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';
import { injectSheet } from '../../../../utilities/jssSetup/JssUtils';
const { SvgIcon } = require('zd-components');

import { TEXT_DATA } from '../../../SETTINGS';
import EmptyStateStyles from '../EmptyStateStyles';

export interface INoDataSourceProps {
    sheet?: any;
}

export const NoDataSource = (props: INoDataSourceProps) => {
    const { classes } = props.sheet;
    const { NO_DATA_SOURCE } = TEXT_DATA;

    return (
        <div className={ classes['zdView-EmptyState'] }>
            <SvgIcon className='zd-icon' name='sources' />
            <div className='description'>
                { NO_DATA_SOURCE.TITLE }
            </div>
            <div className='help'>
                {NO_DATA_SOURCE.DESC}
            </div>
            <a className='more-info-link' target='_blank' href={ NO_DATA_SOURCE.LINK }>{ NO_DATA_SOURCE.LABEL }</a>
        </div>
    );
};

export default injectSheet(EmptyStateStyles)(NoDataSource);
