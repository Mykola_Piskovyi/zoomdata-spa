/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';
import { find } from 'lodash';
import {injectSheet} from '../../../../utilities/jssSetup/JssUtils';
import EmptyStateStyles from '../EmptyStateStyles';
import { EFastFilterValue } from '../../../model/types';
import { EMPTY_PAGE_TYPES } from '../../../SETTINGS';

const { SvgIcon } = require('zd-components');

export interface INoRequestedItemsProps {
    sheet?: any;
    route: EFastFilterValue;
}

export const NoRequestedItems = (props: INoRequestedItemsProps) => {
    const { classes } = props.sheet;
    const { route } = props;
    let emptyPageType = find(EMPTY_PAGE_TYPES, {route: route});
    const { icon, message } = emptyPageType;

    return (
        <div className={ classes['zdView-EmptyState'] }>
            <SvgIcon className='zd-icon' name={ icon } />
            <div className='description'>{ message }</div>
        </div>
    );
};

export default injectSheet(EmptyStateStyles)(NoRequestedItems);
