/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import { EFastFilterValue } from '../../../../model/types';

const NoRequestedItemsProps = {
    route: EFastFilterValue.FAVORITES,
    sheet: {
        classes: {
            'zdView-NoRequestedItems': {}
        }
    }
};

export { NoRequestedItemsProps };
