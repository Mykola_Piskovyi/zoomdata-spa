/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import React from 'react';
import { find } from 'lodash';
import { shallow, mount } from 'enzyme';
import NoRequestedItems from '../NoRequestedItems';
import { NoRequestedItemsProps } from './NoRequestedItems.mock';
import { EMPTY_PAGE_TYPES } from '../../../../SETTINGS';

const EMPTY_PAGE_TYPE = find(EMPTY_PAGE_TYPES, {route: NoRequestedItemsProps.route});

describe('NoRequestedItems Unit Tests: ', ()=> {
    let wrapper;

    describe('WHEN initialize: ', () => {
        it('should exist', () => {
            wrapper = shallow(<NoRequestedItems {...NoRequestedItemsProps} />);
            expect(wrapper).to.exist;
            wrapper.unmount();
        });
    });

    describe('WHEN render', () => {
       it('should render correct description', () => {
           wrapper = mount(<NoRequestedItems {...NoRequestedItemsProps} />);
           expect(wrapper.find('.description').text()).to.equal(EMPTY_PAGE_TYPE.message);
           wrapper.unmount();
       });
    });
});