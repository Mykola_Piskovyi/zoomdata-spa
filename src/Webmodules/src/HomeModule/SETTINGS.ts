/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import {
    TAddNewDialogItem, TFastFilter,
    EAddNewValue, EFastFilterValue, ELayout, ESort, ESortOrder, ESortTable,
    TLayoutItem, TSortItem, TTableColumn, TTimePatterns, TEmptyPageType
} from './model/types';

const HELP_LINK = 'http://zoomdata.com/inapphelp/charts-101-exploring-your-data.html';

const IMAGE_LOAD_ERROR_TEXT = 'Dashboard image not found';

const TEXT_DATA = {
    NO_DATA_SOURCE: {
        TITLE: 'You need a Data Source!',
        DESC: 'You currently do not have access to any Data Sources. Contact your company’s Admin to get started.',
        LABEL: 'More information',
        LINK: HELP_LINK
    },
    NO_ACCESS: {
       DESC: 'Please contact your account administrator. \n You are not a member of this account.'
    },
    NO_CHARTS: {
        DESC: 'Add your first \nChart here!',
        LABEL: 'More information',
        LINK: HELP_LINK
    }
};

const HOME_USER_PREFERENCES = {
    KEY: 'HOME_USER_PREFERENCES',
    VERSION: '1'
};

const FAST_FILTERS: Array<TFastFilter> = [{
    label: 'View All',
    icon: 'view_all',
    value: EFastFilterValue.ALL,
    searchLabel: 'Search All'
}, {
    label: 'Favorites',
    icon: 'favorite_dark',
    value: EFastFilterValue.FAVORITES,
    searchLabel: 'Search Favorites'
}, {
    label: 'My Items',
    icon: 'user',
    value: EFastFilterValue.MY_ITEMS,
    searchLabel: 'Search My Items'
}, {
    label: 'Shared with Me',
    icon: 'group',
    value: EFastFilterValue.SHARED,
    searchLabel: 'Search Shared'
}];

const ADD_NEW_DIALOG_ITEMS: Array<TAddNewDialogItem> = [{
    title: 'Chart & Dashboards',
    icon: 'visualization_type',
    value: EAddNewValue.NEW_CHARTS
}, {
    title: 'Data Source',
    icon: 'sources',
    value: EAddNewValue.NEW_SOURCE
}, {
    title: 'Import Dashboard',
    icon: 'import',
    value: EAddNewValue.NEW_IMPORT_DASHBOARD
}];

const LAYOUT_TYPES: Array<TLayoutItem> = [{
    layout: ELayout.CARDS,
    icon: 'collections'
}, {
    layout: ELayout.TABLE,
    icon: 'attribute_list'
}];

const SORTING_TYPES: Array<TSortItem> = [{
    title: 'Date Modified',
    value: ESort.MODIFIED
}, {
    title: 'Name',
    value: ESort.NAME
}, {
    title: 'Data Source',
    value: ESort.ASSOCIATED_ITEMS
}, {
    title: 'Owner',
    value: ESort.OWNER
}];

const TIME_PATTERNS: TTimePatterns = {
    LAST_MODIFIED: 'MMM D, YYYY, hh:mm A'
};

// Sorting corresponds to the ZD-Components table sorting options
// Sortable: Is the column sortable or disabled
// ActiveColor: Color code for the active sorting toggle arrow
// InactiveColor: Color code for the inactive sorting toggle arrow
// ShowSortToggle: Do we display the sort toggle for this column
const activeColor = '#595959';
const inactiveColor = '#f1f1f1';

const TABLE_COLUMNS: Array<TTableColumn> = [{
    name: 'Fav',
    key: ESortTable.FAVORITES,
    sorting: {
        sortable: true,
        activeColor,
        inactiveColor,
        showSortToggle: false
    },
    width: 30,
    minWidth: 70
},
    {
        name: 'Name',
        key: ESortTable.NAME,
        sorting: {
            sortable: true,
            activeColor,
            inactiveColor,
            showSortToggle: false
        },
        width: 100,
        minWidth: 55
    },
    {
        name: 'Data Source',
        key: ESortTable.ASSOCIATED_ITEMS,
        sorting: {
            sortable: true,
            activeColor,
            inactiveColor,
            showSortToggle: false
        },
        width: 100,
        minWidth: 55
    },
    {
        name: 'Owner',
        key: ESortTable.OWNER,
        sorting: {
            sortable: true,
            activeColor,
            inactiveColor,
            showSortToggle: false
        },
        width: 100,
        minWidth: 55
    },
    {
        name: 'Date Modified',
        key: ESortTable.MODIFIED,
        sorting: {
            sortable: true,
            activeColor,
            inactiveColor,
            showSortToggle: false
        },
        width: 100,
        minWidth: 55
    },
    {
        name: 'Delete',
        key: 'deleteButton',
        sorting: {
            sortable: false,
            activeColor,
            inactiveColor,
            showSortToggle: false
        },
        width: 45,
        minWidth: 55
    }];

const EMPTY_PAGE_TYPES: Array<TEmptyPageType> = [{
    route: EFastFilterValue.FAVORITES,
    message: 'No favorite items',
    icon: 'visualization_type'
}, {
    route: EFastFilterValue.MY_ITEMS,
    message: 'You haven\'t created any charts or dashboards',
    icon: 'visualization_type'
}, {
    route: EFastFilterValue.SHARED,
    message: 'No shared items',
    icon: 'visualization_type'
}];

const ANIMATION_TIMEOUT_150MS = 150;

export {
    TEXT_DATA,
    HOME_USER_PREFERENCES,
    ADD_NEW_DIALOG_ITEMS,
    FAST_FILTERS,
    LAYOUT_TYPES,
    SORTING_TYPES,
    TABLE_COLUMNS,
    TIME_PATTERNS,
    EMPTY_PAGE_TYPES,
    HELP_LINK,
    IMAGE_LOAD_ERROR_TEXT,
    ANIMATION_TIMEOUT_150MS
};
