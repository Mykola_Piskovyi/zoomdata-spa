/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as _ from 'lodash';
import * as React from 'react';
const { Button, SvgIcon, Popover, OutsideClickHandler } = require('zd-components');
import AddNewDialog, {IAddNewDialogProps} from './AddNewDialog';
import HomePopover from '../HomePopover/HomePopover';

import {injectSheet} from '../../../utilities/jssSetup/JssUtils';
import AddNewStyles from './AddNewStyles';

const buttonStyle = {
    margin: 0,
    borderRadius: 3
};

export interface IAddNewProps extends IAddNewDialogProps {
    sources: Array<any>;
    onPopoverItemClick(item: any): any;
    sheet?: any;
}

const AddNewButton = ({onClick}) => {
    const buttonProps = {
        icon: <SvgIcon name='add_light' />,
        className: 'add-new-button',
        iconAlign: 'LEFT',
        text: 'New',
        size: 'SMALL',
        width: 'FULL',
        style: buttonStyle,
        onClick: onClick
    };

    return (
        <Button {...buttonProps} />
    );
};

export class AddNew extends React.Component<IAddNewProps, any> {

    constructor(options) {
        super(options);
        this.state = {open: false};
        _.bindAll(this, [
            'onNewToggle',
            'onClose',
            'handleDialogClick',
            'getPopoverContent'
        ]);
    }

    onNewToggle(event) {
        const { open } = this.state;
        if (open) {
            this.onClose();
        } else {
            this.setState({open: true, anchorEl: event.currentTarget});
        }
    }

    onClose() {
        this.setState({open: false, currentPopoverContent: 'ADD_NEW_DIALOG'});
    }

    handleDialogClick(value, currentStep) {
        if (value === 'NEW_CHARTS') {
            this.setState(({currentPopoverContent: 'HOME_POPOVER'}));
        }
        if (currentStep === 2) {
            this.props.onPopoverItemClick(value);
        }
        if (this.props.onNewDialogItemClick) {
            this.props.onNewDialogItemClick(value);
        }
    }

    getPopoverContent(currentPopoverContent) {
        const { classes } = this.props.sheet;
        // classes['zdView-AddNew']
        const dialogProps = {
            className: classes['zdView-AddNewDialog'], // 'add-new-dialog',
            addNewItems: this.props.addNewItems,
            onNewDialogItemClick: this.handleDialogClick
        };

        return currentPopoverContent === 'HOME_POPOVER' ?
            (<HomePopover
                onClose={this.onClose}
                handlePopoverClick={this.handleDialogClick}
                sources={this.props.sources}
            />)
            : (<AddNewDialog {...dialogProps} />);
    }

    render() {
        const { open, anchorEl, currentPopoverContent} = this.state;
        const { classes } = this.props.sheet;

        return (
            <div className={classes['zdView-AddNew']} >
                <AddNewButton onClick={this.onNewToggle} />
                <Popover
                    open={open}
                    anchorEl={anchorEl}
                    anchorOrigin={{'horizontal': 'left', 'vertical': 'bottom'}}
                    targetOrigin={{'horizontal': 'left', 'vertical': 'top'}}
                    onRequestClose={this.onClose}
                    style={{zIndex: 10000}}
                >
                    <OutsideClickHandler onOutsideClick={this.onClose}>
                        {this.getPopoverContent(currentPopoverContent)}
                    </OutsideClickHandler>
                </Popover>
            </div>
        );
    }
}

export default injectSheet(AddNewStyles)(AddNew);
