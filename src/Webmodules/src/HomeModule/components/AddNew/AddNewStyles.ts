/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

export default {
    'zdView-AddNew': {
        textAlign: 'center',
        padding: '8px 10px',

        '& button': {
            '&.add-new-button': {
                outline: 'none',

                '&:focus': {
                    background: '#0086a3'
                },

                '& svg.left': {
                    float: 'inherit !important',
                    verticalAlign: 'top !important',
                    marginRight: '5px !important'
                },

                '& span': {
                    '&:after': {
                        content: 'none'
                    }
                }
            }
        }
    },

    'zdView-AddNewDialog': {
        boxShadow: 'rgba(0, 0, 0, 0.3) 2px 2px 6px 0px',

        '& .add-new-dialog-item': {

            cursor: 'pointer',
            color: '#595959',
            padding: '0px 8px',
            marginLeft: '0px',
            border: 'none',
            height: '30px',
            textRendering: 'optimizeLegibility',
            fontSmoothing: 'antialiased',

            '& .item-icon': {
                color: '#595959',
                width: '14px',
                height: '30px',
                paddingRight: '8px'
            },

            '& .item-text': {
                fontWeight: 400,
                lineHeight: '30px'
            },

            '& > div': {
                paddingLeft: '4px',
                top: '0px' // override zd-styles only this way
            },

            '&:hover': {
                color: '#595959',
                backgroundColor: '#f1f1f1',
                borderRadius: 'inherit',

                '& .item-text': {
                    fontWeight: '600'
                }
            }
        }
    }
};
