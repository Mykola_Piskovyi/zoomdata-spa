/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as _ from 'lodash';
import * as React from 'react';
const { List } = require('zd-components');

import {TAddNewDialogItem, EAddNewValue} from '../../model/types';
import {ADD_NEW_DIALOG_ITEMS} from '../../SETTINGS';

import AddNewDialogItem from './AddNewDialogItem';

const listStyle = {
    position: 'absolute',
    backgroundColor: '#d2d2d2',
    width: 160,
    marginTop: 4,
    borderRadius: 3,
    zIndex: 100,
    fontSize: 14
};

export interface IAddNewDialogProps {
    className?: string;
    addNewItems: Array<TAddNewDialogItem>;
    onNewDialogItemClick(value: EAddNewValue, currentStep?: any): any;
}

class AddNewDialog extends React.Component<IAddNewDialogProps, {}> {

    constructor(options) {
        super(options);

        _.bindAll(this, 'onItemClick');
    }

    render() {
        const {className} = this.props;
        const dialogItems = this.props.addNewItems.map((item: TAddNewDialogItem, i) => {
            const props = {item, i, onClick: this.onItemClick};
            return (
                <AddNewDialogItem key={i} {...props} />
            );
        });

        return (
            <List className={className} listStyle='COMPACT' style={listStyle} >
                {dialogItems}
            </List>
        );
    }

    onItemClick(value) {
        this.props.onNewDialogItemClick(value);
    }

}

export default AddNewDialog;
