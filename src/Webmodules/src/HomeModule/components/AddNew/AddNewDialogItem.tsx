/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import {assign} from 'lodash';
import * as React from 'react';
const { SvgIcon, ListItem } = require('zd-components');

import {TAddNewDialogItem, EAddNewValue} from '../../model/types';

export interface IAddNewDialogItemProps {
    item: TAddNewDialogItem;
    i: number;
    onClick(value: EAddNewValue): any;
}

const AddNewDialogItem = ({item, i, onClick}: IAddNewDialogItemProps) => {

    const svgProps = {
        className: 'item-icon',
        name: item.icon,
        key: `${i}-icon`
    };

    if (item.icon === 'visualization_type') {
        assign(svgProps, {viewBox: '-200 0 2000 2000'});
    }

    const content = [
        <SvgIcon {...svgProps} />,
        <div className='item-text' key={`${i}-text`} >{item.title}</div>
    ];

    const props = {
        leftContents: content,
        key: item.title,
        selectable: true,
        onClick: () => (onClick(item.value))
    };

    return (
        <ListItem {...props} className='add-new-dialog-item'/>
    );

};

export default AddNewDialogItem;
