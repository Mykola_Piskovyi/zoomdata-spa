import * as mocha from 'mocha';
import { expect } from 'chai';
import * as sinon from 'sinon';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

describe('AddNewDialogItem Unit Tests', function() {

    beforeEach(() => {});

    afterEach(() => {});

    it('can instantiate');

    it('can render');

    it('can render SvgIcon');

    it('can handle item click');

    it('can handle a variety of "icon", "i", and "title" values');

    it('can unmount');

});