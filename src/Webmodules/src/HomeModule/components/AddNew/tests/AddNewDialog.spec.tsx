import * as mocha from 'mocha';
import { expect } from 'chai';
import * as sinon from 'sinon';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

describe('AddNewDialog Unit Tests', function() {

    beforeEach(() => {});

    afterEach(() => {});

    it('can instantiate');

    it('can render with no AddNewDialogItem');

    it('can render single AddNewDialogItem');

    it('can render multiple AddNewDialogItem');

    it('can handle item click');

    it('can unmount');

});