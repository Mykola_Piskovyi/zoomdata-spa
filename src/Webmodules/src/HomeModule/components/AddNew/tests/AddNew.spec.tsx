import * as _ from 'lodash';
import * as mocha from 'mocha';
import { expect } from 'chai';
import * as sinon from 'sinon';
import { shallow, mount, render } from 'enzyme';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as TestUtils from 'react-addons-test-utils';
import AddNew, { IAddNewProps } from '../AddNew';
import AddNewDialog, { IAddNewDialogProps } from '../AddNewDialog';
import AddNewDialogItem from '../AddNewDialog';
import { TAddNewDialogItem, EAddNewValue } from '../../../model/types';
import { ADD_NEW_DIALOG_ITEMS } from '../../../SETTINGS';

const { Button, SvgIcon } = require('zd-components');

describe('AddNew Unit Tests', () => {

    let sandbox: any;
    let clickHandlerStub: any;
    let clickHandlerSpy: any;
    let props: any;
    let wrapper: any;

    const getProps = (config = {}) => {
        return _.assign({
            addNewItems: ADD_NEW_DIALOG_ITEMS,
            sources: [],
            onNewDialogItemClick: clickHandlerStub,
            onPopoverItemClick: () => {}
        }, config);
    };

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        clickHandlerStub = (value: EAddNewValue): any => {};
        clickHandlerSpy = sandbox.spy(clickHandlerStub);
        props = getProps();

        wrapper = mount(<AddNew {...props} />);
    });

    afterEach(() => {
        sandbox.restore();
        wrapper.unmount();
    });

    it('can instantiate', () => {
        expect(wrapper).to.exist;
    });

    it('can render', () => {
        expect(wrapper.find('AddNew')).to.have.length(1);
    });

    it('can render Button', () => {
        expect(wrapper.find('Button')).to.have.length(1);
    });

    it('doesn\'t display AddNewDialog by default', () => {
        expect(wrapper.find('AddNewDialog')).to.have.length(0);

    });

    it('can render Popover on AddNew click', () => {
        expect(wrapper.find('Popover').props().open).to.be.false;
        wrapper.find('button').simulate('click');
        expect(wrapper.find('Popover').props().open).to.be.true;
    });

    xit('can invoke onNewDialogItemClick', () => {
        props = getProps({onNewDialogItemClick: clickHandlerSpy});
        wrapper = mount(<AddNew {...props} />);

        wrapper.find('Button').simulate('click');
        expect(wrapper.state('open')).to.be.true;

        expect(wrapper.find('AddNewDialog')).to.have.length(1);
        // invoke the click handler on the child component
        wrapper.find('AddNewDialog').prop('onNewDialogItemClick')('TEST');
        expect(clickHandlerSpy.called).to.be.true;
    });
});