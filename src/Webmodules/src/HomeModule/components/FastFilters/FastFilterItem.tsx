/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as _ from 'lodash';
import * as React from 'react';
const classNames = require('classnames');
const { SvgIcon } = require('zd-components');

import { injectSheet } from '../../../utilities/jssSetup/JssUtils';
import { TFastFilter, EFastFilterValue } from '../../model/types';
import FastFilterItemStyles from './FastFilterItemStyles';

export interface IFastFilterItemProps {
    item: TFastFilter;
    active?: boolean;
    onClick(value: EFastFilterValue): any;
    sheet?: any;
}

const FastFilterItem = ({item, onClick, active, sheet}: IFastFilterItemProps) => {
    const { classes } = sheet;
    const itemProps = {
        key: item.label,
        onClick: () => (onClick(item.value))
    };
    const iconProps = {
        className: 'item-icon',
        name: item.icon,
        key: `${item.value}-icon`,
        preserveAspectRatio: item.icon !== 'view_all' ? 'xMidYMid meet' : 'xMinYMid meet'
    };

    return (
        <div className={classNames(classes['zdView-FastFilterItem'], 'fast-filter-item', {'active': active})} {...itemProps} >
            <SvgIcon {...iconProps} />
            <div className='item-text'>{ item.label }</div>
        </div>
    );

};

export default injectSheet(FastFilterItemStyles)(FastFilterItem);
