
import {assign} from 'lodash';
import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';

import FastFilters from '../FastFilters';
import FastFilterItem from '../FastFilterItem';
import {FAST_FILTERS} from '../../../SETTINGS';
import {EFastFilterValue} from '../../../model/types';

describe('FastFilters Unit Tests: ', function() {

    let sandbox;
    let props;
    let filtersComponent;

    const getProps = (props) => {
        return assign({
            route: EFastFilterValue.ALL,
            fastFilters: FAST_FILTERS,
            onFilterClick: sandbox.stub()
        }, props);
    };

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        props = getProps();

        filtersComponent = shallow(<FastFilters {...props}/>);
    });

    afterEach(() => {
        sandbox.restore();
        filtersComponent.unmount();
    });

    it('should be defined: ', () => {
        expect(FastFilters).to.exist;
        expect(filtersComponent).to.exist;
    });

    describe('Rendering: ', () => {

        it (`should render "${FAST_FILTERS.length}" FastFilterItem (FAST_FILTERS): `, () => {
            expect(filtersComponent.find(FastFilterItem))
                .to.have.length(FAST_FILTERS.length);
        });

        it('should render items according to order in SETTINGS: ', () => {
            let items = filtersComponent.find(FastFilterItem);
            FAST_FILTERS.forEach((item, i) => {
                expect(items.get(i).props.item).to.equal(item);
            });
        });

        it(`should have active route "${EFastFilterValue.ALL}" and other "false": `, () => {
            let items = filtersComponent.find(FastFilterItem);

            FAST_FILTERS.forEach((item, i) => {
                if (item.value === EFastFilterValue.ALL) {
                    expect(items.get(i).props.active).to.equal(true);
                } else {
                    expect(items.get(i).props.active).to.equal(false);
                }
            });
        });

        it(`should have active route "${EFastFilterValue.FAVORITES}" and other "false": `, () => {
            props = getProps({route: EFastFilterValue.FAVORITES});
            filtersComponent = shallow(<FastFilters {...props}/>);
            let items = filtersComponent.find(FastFilterItem);

            FAST_FILTERS.forEach((item, i) => {
                if (item.value === EFastFilterValue.FAVORITES) {
                    expect(items.get(i).props.active).to.equal(true);
                } else {
                    expect(items.get(i).props.active).to.equal(false);
                }
            });
        });

        it('can render with no items', () => {
            props = getProps({fastFilters: []});
            filtersComponent = shallow(<FastFilters {...props}/>);

            expect(filtersComponent.find(FastFilterItem)).to.have.length(0);
        });

        describe('Handle filter click: ', () => {

            FAST_FILTERS.forEach((item, i) => {
                it(`should handle click on filter item with "${item.value}" value:`, () => {
                    filtersComponent.find(FastFilterItem).at(i).simulate('click');
                    expect(props.onFilterClick.called).to.be.true;
                    expect(props.onFilterClick.getCall(0).args).to.eql([item.value]);
                });
            });
        });

    });

});