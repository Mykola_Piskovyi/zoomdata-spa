import {assign} from 'lodash';
import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';

import FastFilterItem from '../FastFilterItem';
import {FAST_FILTERS} from '../../../SETTINGS';
import {EFastFilterValue} from '../../../model/types';

describe('FastFilterItem Unit Tests: ', function() {

    let sandbox;
    let props;
    let itemComponent;

    const getProps = (props) => {
        return assign({
            item: FAST_FILTERS[0],
            active: false,
            onClick: sandbox.stub()
        }, props);
    };

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        props = getProps();

        itemComponent = mount(<FastFilterItem {...props}/>);
    });

    afterEach(() => {
        sandbox.restore();
        itemComponent.unmount();
    });

    it('should be defined: ', () => {
        expect(FastFilterItem).to.exist;
        expect(itemComponent).to.exist;
    });

    describe('Rendering: ', () => {

        it('should render proper filter item "label": ', () => {
            expect(itemComponent.find('.item-text').text())
                .to.eql(FAST_FILTERS[0].label);
        });

        it('should NOT be active "selected" according: ', () => {
            expect(itemComponent.find('.fast-filter-item').hasClass('active')).to.eql(false);
        });

        it('should be active "selected" according prop "active: true" : ', () => {
            props = getProps({active: true});
            itemComponent = mount(<FastFilterItem {...props}/>);

            expect(itemComponent.find('.fast-filter-item').hasClass('active')).to.eql(true);
        });

    });

    describe('Handle events: ', () => {

        it('should handle onClick call props.onClick() with value: ', () => {
            itemComponent.simulate('click');

            expect(props.onClick.called).to.be.true;
            expect(props.onClick.getCall(0).args).to.eql([props.item.value]);
        });

    });

});