/*
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */

export default {
    'zdView-FastFilterItem': {
        cursor: 'pointer',
        color: '#d2d2d2',
        padding: '7px 0px 7px 10px',
        border: 'none',
        height: '18px',
        marginBottom: '1px',

        '&:last-child': {
            marginBottom: 0
        },

        '& svg': {
            width: '20px',
            height: '14px',
            verticalAlign: 'initial',
            color: '#d2d2d2',
            padding: '2px 4px 2px 0px',
            float: 'left'
        },

        '& .item-text': {
            height: '18px',
            lineHeight: '18px',
            display: 'inline-block',
            float: 'left'
        },

        '&.active': {
            background: '#323232',
            fontWeight: 600,

            '& .item-icon': {
                color: '#d6df23 !important'
            },

            '& .item-text': {
                color: '#e6e6e6 !important'
            }
        },

        '&:hover': {
            '& .item-icon': {
                color: '#b8b8b8'
            },

            '& .item-text': {
                color: '#b8b8b8'
            }
        }
    }
};
