/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as _ from 'lodash';
import * as React from 'react';
const { Theme, List } = require('zd-components');

import {TFastFilter, EFastFilterValue} from '../../model/types';

import FastFilterItem, {IFastFilterItemProps} from './FastFilterItem';

const listStyles = {
    'borderRadius': 0,
    'background': 'inherit'
};

export interface IFastFiltersProps {
    route: EFastFilterValue;
    fastFilters: Array<TFastFilter>;
    onFilterClick(value: string): any;
}

class FastFilters extends React.Component<IFastFiltersProps, {}> {

    constructor(options) {
        super(options);

        _.bindAll(this, '_onItemClick');
    }

    render() {
        const {route} = this.props;
        const items = this.props.fastFilters.map((item, i) => {
            const props = {
                item,
                // i,
                active: route === item.value,
                onClick: () => (this._onItemClick(item.value))
            };
            return (
                <FastFilterItem key={i} {...props} />
            );
        });

        return (
            <Theme>
                <List listStyle='COMPACT' style={listStyles}>
                    {items}
                </List>
            </Theme>
        );
    }

    _onItemClick(value) {
        this.props.onFilterClick(value);
    }

}

export default FastFilters;
