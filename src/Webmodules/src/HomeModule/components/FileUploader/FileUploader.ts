/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

const Rx = require('rx');
const EVENTS = {
    CHANGE: 'change'
};

class FileUploader {
    fileInput: HTMLElement;
    fileObservable;
    constructor() {
        this.fileInput = document.createElement('input');
        this.fileInput.setAttribute('type', 'file');
        this.fileInput.setAttribute('accept', 'application/json');

        this.fileObservable = Rx.Observable
            .fromEvent(this.fileInput, EVENTS.CHANGE);
    }

    getObservable() {
        return this.fileObservable;
    }

    init(): FileUploader {
        this.fileInput.click();

        return this;
    }

    remove(): void {
        this.fileObservable.dispose();
        this.fileInput.remove();
    }
}

export default FileUploader;
