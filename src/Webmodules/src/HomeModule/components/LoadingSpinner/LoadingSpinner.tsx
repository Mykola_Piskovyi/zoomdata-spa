/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as _ from 'lodash';
import * as React from 'react';
const {Spinner} = require('zd-components');

export type LoadingProps = {
    size?: number;
    style?: any;
    transparent?: boolean;
};

const LoadingSpinner = ({size = 36, style = {}, transparent}: LoadingProps) => {

    const styles = _.assign({
        'backgroundColor': transparent ? 'rgba(230,230,230,0.75)' : '',
        'width': '100%',
        'height': 'inherit',
        'zIndex': 99
    }, style);

    const styleSpinner = {
        height: size,
        width: size,
        position: 'relative',
        top: `calc(50% - ${size - 12}px)`,
        left: `calc(50% - ${size - 12}px)`
    };

    return (
        <div style={styles} className='spinner-container'>
            <Spinner style={styleSpinner} className='spinner-loader'/>
        </div>
    );
};

export default LoadingSpinner;
