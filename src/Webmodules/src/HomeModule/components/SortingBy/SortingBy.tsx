/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';
import { isEmpty } from 'lodash';
import { bindAll, head, isEqual } from 'lodash';
import { SORTING_TYPES } from '../../SETTINGS';
import { TSortByType, ESort, ESortOrder, TSortItem } from '../../model/types';
import { injectSheet } from '../../../utilities/jssSetup/JssUtils';
import SortingByStyles from './SortingByStyles';

const { SortToggle, Dropdown, DropdownItem } = require('zd-components');

export const SORTING_TYPES_ERROR = 'At least one sorting type should be specified';

export interface ISortingByProps {
    sortObject: TSortByType;
    sortingTypes?: Array<TSortItem>;
    onSortChange(value: TSortByType): void;
    sheet?: any;
}

// README: Did this way, because SortToggle could has 'null' value
export interface IState {
    sortOrder: ESortOrder;
}

export class SortingBy extends React.Component<ISortingByProps, IState> {
    static defaultProps = {
        sortingTypes: SORTING_TYPES
    };

    constructor(options) {
        super(options);

        this.state = {
            sortOrder: options.sortObject.sortOrder
        };

        bindAll(this, '_onSortTypeChange', '_onSortDirChange');
    }

    componentWillMount() {
        const { sortingTypes } = this.props;

        if (isEmpty(sortingTypes)) {
            throw new Error(SORTING_TYPES_ERROR);
        }
    }

    private _onSortTypeChange(e: React.SyntheticEvent<any>, sort: ESort): void {
        this.props.onSortChange({
            sort,
            sortOrder: this.props.sortObject.sortOrder
        });
    }

    private _onSortDirChange(sortOrder: ESortOrder): void {

        // README: Did this way, because SortToggle could has 'null' value
        if (sortOrder === null) {
            sortOrder = ESortOrder.ASC;
        }

        this.setState({sortOrder});

        this.props.onSortChange({
            sort: this.props.sortObject.sort,
            sortOrder
        });
    }

    render() {
        const { sortObject, sortingTypes } = this.props;
        const {sortOrder} = this.state;
        const { classes } = this.props.sheet;

        let sortingByItems = sortingTypes.map((item, index) => {
            return <DropdownItem
                isDefault={ isEqual(sortObject.sort, item.value) }
                text={ item.title }
                value={ item.value }
                key={ index } />;
        });

        return (
            <div className={ classes['zdView-SortingBy'] }>
                <div className='sortby-toggle-wrapper'>
                    <SortToggle ref='sortToggle' onClick={this._onSortDirChange} initialSort={sortOrder} showBorder />
                </div>
                <div className='sortby-select-wrapper'>
                    <div className='sortby-label'>Sort By</div>
                    <Dropdown ref='dropdown' className='sortby-select' isLink onChange={this._onSortTypeChange}>{ sortingByItems }</Dropdown>
                </div>
            </div>
        );
    }
}

export default injectSheet(SortingByStyles)(SortingBy);
