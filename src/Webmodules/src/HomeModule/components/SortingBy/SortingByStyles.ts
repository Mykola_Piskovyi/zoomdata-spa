/*
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */

export default {
    'zdView-SortingBy': {
        height: '24px',
        display: 'inline-block',

        '& > div': {
            float: 'left'
        },

        '& .sortby-toggle-wrapper': {

            '& .circleBorder': {
                borderColor: '#b2b2b2',
                marginBottom: 0,

                '&:hover': {
                    borderColor: '#7a7a7a',

                    '& div': {
                        '&:first-child': {
                            borderBottom: '8px solid #999999',

                            '&.selected': {
                                borderBottom: '8px solid #404040'
                            }
                        },

                        '&:last-child': {
                            borderTop: '8px solid #999999',

                            '&.selected': {
                                borderTop: '8px solid #404040'
                            }
                        }
                    }
                },

                '& div': {
                    '&:first-child': {
                        borderBottom: '8px solid #b2b2b2',

                        '&.selected': {
                            borderBottom: '8px solid #595959'
                        }
                    },

                    '&:last-child': {
                        borderTop: '8px solid #b2b2b2',

                        '&.selected': {
                            borderTop: '8px solid #595959'
                        }
                    }
                }
            }
        },

        '& .sortby-select-wrapper': {
            '& > div': {
                float: 'left'
            },

            '& .linkArrow': {
                display: 'none'
            },

            '& .sortby-label': {
                margin: '0 4px',
                fontSize: '14px',
                color: '#595959',
                display: 'inline-block',
                lineHeight: '24px',
            },

            '& .sortby-select': {
                margin: 0,
                display: 'block',

                '& .link': {
                    fontSize: '14px',

                    '& .linkLabel': {
                        color: '#595959',
                        fontWeight: 'bold',

                        '&:hover': {
                            color: '#595959'
                        }
                    }
                }
            }
        }
    }
};
