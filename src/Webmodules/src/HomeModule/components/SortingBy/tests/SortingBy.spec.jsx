/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import React from 'react';
import sinon from 'sinon';
import { assign, take } from 'lodash';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
const { SortToggle, Dropdown, DropdownItem } = require('zd-components');

import { SortingBy, SORTING_TYPES_ERROR } from '../SortingBy.tsx';
import { sortingByProps } from './SortingBy.mock.ts';

describe('SortingBy Unit Tests', () => {
    let wrapper;

    describe('WHEN SortingBy is initialized: ', () => {
        beforeEach(() => {
            wrapper = shallow(<SortingBy {...sortingByProps} />);
        });

        afterEach(() => {
            wrapper.unmount();
        });

        it('can instantiate', () => {
            expect(wrapper).to.exist;
        });
    });

    describe('WHEN SortingBy is rendered: ', () => {
        beforeEach(() => {
            wrapper = mount(<SortingBy {...sortingByProps} />);
        });

        afterEach(() => {
            wrapper.unmount();
        });

        it('can render', () => {
            expect(wrapper.children()).to.have.length(2);
            expect(wrapper.find(SortToggle)).to.have.length(1);
            expect(wrapper.find(Dropdown)).to.have.length(1);
        });

        it('can render single item list', () => {
            const { sortingTypes } = sortingByProps;
            wrapper = mount(<SortingBy {...sortingByProps}
                sortingTypes={ take(sortingTypes, 1) }/>);

            const { dropdown } = wrapper.instance().refs;
            dropdown.toggleOpen();

            expect(wrapper.find(DropdownItem)).to.have.length(1);
        });

        it('can render multiple item list', () => {
            const { dropdown } = wrapper.instance().refs;
            dropdown.toggleOpen();

            expect(wrapper.find(DropdownItem).length).to.be.above(1);
        });

        it('can handle sort change', () => {
            const onSortChangeSpy = sinon.spy();
            wrapper = mount(<SortingBy {...sortingByProps}
                onSortChange={ onSortChangeSpy }/>);
            wrapper.find(SortToggle).simulate('click');

            expect(onSortChangeSpy.called).to.equal(true);
        });

        it('can not render with no sorting types', () => {
            const props = assign({}, sortingByProps, {
                sortingTypes: []
            });

            expect(() => {
                wrapper = mount(<SortingBy {...props} />)
            }).to.throw(SORTING_TYPES_ERROR);
        });
    });
});
