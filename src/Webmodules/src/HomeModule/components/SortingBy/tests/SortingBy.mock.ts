/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import { TSortByType } from '../../../model/types';

const sortingByProps = {
    sortObject: {
        sort: 'name',
        sortOrder: 'ASC'
    },
    onSortChange: (sortObject: TSortByType) => {
    },
    sortingTypes: [{
        title: 'Date Modified',
        value: 'modified'
    }, {
        title: 'Name',
        value: 'name'
    }, {
        title: 'Owner',
        value: 'owner'
    }],
    sheet: {
        classes: {
            'zdViewSortingBy': {}
        }
    }
};

export { sortingByProps };
