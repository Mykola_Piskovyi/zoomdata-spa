/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
import * as React from 'react';
import {TDataItem} from '../../model/types';

export interface ModalProps {
    handleClose(): void;
    handleDelete(): void;
    text: string;
}

export interface DialogProps {
    handleClose(): void;
    handleDelete(): void;
    text: string;
    showModal: boolean;
}

export const Modal: React.SFC<ModalProps> = ({handleClose, handleDelete, text}) =>
    <div className='zdView-ModalWarning modal zd-warning in source-delete fade' style={{position: 'fixed'}} >
        <div className='modal-body'>
            <div className='zd-warning'>
                <div className='warn'>
                    <i className='zd icon warning'></i> <span>WARNING</span>
                </div>
                <div className='warning-description' style={{wordBreak: 'initial'}}>
                    Are you sure you want to delete dashboard <span className='zd-homeModal-source-name'>{text}</span>?
                </div>
            </div>
        </div>
        <div className='modal-footer'>
            <a className='btn btn-action source-delete-btn' onClick={handleDelete}>Delete</a>
            <a className='btn btn-action warning-leave-btn' onClick={handleClose}>Cancel</a>
        </div>
    </div>;


export const Dialog: React.SFC<DialogProps> = ({handleClose, handleDelete, text, showModal}) => {
    return showModal ? (
        <div>
            <Modal
                handleClose={handleClose}
                handleDelete={handleDelete}
                text={text}
            />
            <div className='modal-backdrop fade in' style={{position: 'fixed'}}></div>
        </div>
    ) :
        null;
};
