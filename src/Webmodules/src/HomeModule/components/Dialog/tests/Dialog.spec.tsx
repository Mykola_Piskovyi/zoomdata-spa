/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import {expect} from 'chai';
import * as sinon from 'sinon';
import {mount} from 'enzyme';
import * as React from 'react';
import {DASHBOARD_ITEM} from '../../../../HomeModule/tests/HomeModule.mock';

import {Dialog, DialogProps, Modal} from '../Dialog';

describe('Dialog Unit Tests: ', function() {

    let wrapper;
    let sandbox;
    let props;
    let getProps;

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        const initialProps: DialogProps = {
            handleClose:  sandbox.stub(),
            handleDelete:  sandbox.stub(),
            text: DASHBOARD_ITEM.name,
            showModal: false
        };
        getProps = (props?): DialogProps => ({...initialProps, ...props});
    });

    afterEach(() => {
        sandbox.restore();
        wrapper.unmount();
    });

    it('should not be open by default', () => {
        props = getProps();
        wrapper = mount(<Dialog {...props}/>);
        expect(wrapper.find(Modal).length).to.equal(0);
    });

    it('should be open if showModal is true', () => {
        props = getProps({showModal: true});
        wrapper = mount(<Dialog {...props}/>);
        expect(wrapper.find(Modal).length).to.equal(1);

    });

    it('should call handleClose if cancel clicked', () => {
        props = getProps({showModal: true});
        wrapper = mount(<Dialog {...props}/>);
        expect(wrapper.find(Modal).length).to.equal(1);

        wrapper
            .find('.warning-leave-btn')
            .simulate('click');

        expect(props.handleDelete.called).to.be.false;
        expect(props.handleClose.called).to.be.true;

    });

    it('should call handleDelete if delete button clicked', () => {
        props = getProps({showModal: true});
        wrapper = mount(<Dialog {...props}/>);
        expect(wrapper.find(Modal).length).to.equal(1);

        wrapper
            .find('.source-delete-btn')
            .simulate('click');

        expect(props.handleDelete.called).to.be.true;
    });

    it('should show the correct source name on the Dialog', () => {
        props = getProps({showModal: true});
        const sourceName = props.text;

        wrapper = mount(<Dialog {...props}/>);
        expect(wrapper.find(Modal).length).to.equal(1);

        const modalSourceName = wrapper
            .find('.zd-homeModal-source-name')
            .text();

        expect(sourceName).to.equal(modalSourceName);
        expect(props.handleDelete.called).to.be.false;
        expect(props.handleClose.called).to.be.false;

    });

    it('should only show the modal with a source selected', () => {
        props = getProps({selectedItem: null});
        wrapper = mount(<Dialog {...props}/>);
        expect(wrapper.find(Modal).length).to.equal(0);
    });

    it('should only show the modal with a source selected', () => {
        props = getProps({selectedItem: null});
        wrapper = mount(<Dialog {...props}/>);
        expect(wrapper.find(Modal).length).to.equal(0);
    });
});
