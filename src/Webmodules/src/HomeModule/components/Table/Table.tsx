/*
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */

import * as React from 'react';
import {assign, bindAll, chain, clamp, clone, findIndex, isEqual, isNull, isUndefined} from 'lodash';
import {injectSheet} from '../../../utilities/jssSetup/JssUtils';
import TableStyles from './TableStyles';
import {getVisTypeIcon} from '../../../utilities/utilities';

import {
    ESort,
    ESortOrder,
    ESortTable,
    TDataAssociatedItem,
    TDataItem,
    TSortByType,
    TTableColumn,
    TTableDataItem
} from '../../model/types';
import {TABLE_COLUMNS} from '../../SETTINGS';
import {DeleteButton, SvgIcon, Table} from 'zd-components';
const classNames = require('classnames');

export interface ITableProps {
    dataItems: Array<TDataItem>;
    selectedItem: TDataItem;
    onItemFavorite(item: TDataItem): void;
    onItemClick(item: TDataItem): void;
    onItemDelete(item: TDataItem): void;
    onSortChange(value: TSortByType): void;
    setSelectedItem(item: TDataItem): void;
    sheet?: {
        classes: any
    };
    sortObject: TSortByType;
}

export interface ITableState {
    columns: Array<TTableColumn>;
}

type TSortByTable = {
    sort: ESortTable;
    order: ESortOrder;
};

export class TableView extends React.Component<ITableProps, ITableState> {
    private pendingSelection: TDataItem;
    private pendingStartTime: number;
    private mouseEnterTimeout: number;

    constructor(options: ITableProps) {
        super(options);

        this.pendingStartTime = 0;

        // Make sure we don't mutate the SETTINGS.ts version of TABLE_COLUMNS
        this.state = {
            columns: clone(TABLE_COLUMNS)
        };

        bindAll(this, '_onSort', '_onColumnResize', '_onWindowResize',
            '_onItemMouseOver', '_onItemMouseLeave');

        this._onSort(eSortToESortTable(options.sortObject.sort),
            options.sortObject.sortOrder, false);
    }

    componentWillReceiveProps(next: ITableProps): void {
        const current = this.props;

        if (!isEqual(next.sortObject, current.sortObject)) {
            this._onSort(eSortToESortTable(next.sortObject.sort),
                next.sortObject.sortOrder);
        }
    }

    componentDidMount() {
        window.addEventListener('resize', this._onWindowResize);
    }

    private _onWindowResize() {
        this.forceUpdate();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._onWindowResize);
        window.clearTimeout(this.mouseEnterTimeout);
    }

    private _onSort(key: ESortTable, order: ESortOrder, setState: boolean = true): void {
        this.props.onSortChange({
            sort: eSortTableToESort(key),
            sortOrder: order
        });

        const columns = getNewSortingState(key, this.state.columns);

        if (setState) {
            this.setState({columns});
        }
    }

    private _onItemMouseOver(selectedItem: TDataItem): void {
        this.pendingSelection = selectedItem;
        this.pendingStartTime = Date.now();

        this.mouseEnterTimeout = window.setTimeout(() => {
            // Second clause is needed to prevent quickly hovering back and forth
            // and happening to reselect the one you had selected when the callback fires
            if (this.pendingSelection === selectedItem
                && 300 <= (Date.now() - this.pendingStartTime)) {
                this.props.setSelectedItem(selectedItem);
            }
        }, 300);
    }

    private _onItemMouseLeave(): void {
        this.pendingSelection = null;
        this.pendingStartTime = 0;
        window.clearTimeout(this.mouseEnterTimeout);
    }

    private _onColumnResize(deltaX: number, key: string): void {
        if (deltaX === 0) {
            return;
        }

        // get the column being resized and the one to the right
        const columns = clone(this.state.columns);
        const colIndex = findIndex(columns, { key });
        const adjacentColIndex = colIndex + 1;
        const col = columns[colIndex];
        const adjacentCol = columns[adjacentColIndex];

        // determine max delta allowed by column being resized
        const currentWidth = Math.round(col.width);
        const width = isUndefined(col.maxWidth) ?
            Math.max(currentWidth + deltaX, col.minWidth) :
            clamp(currentWidth + deltaX, col.minWidth, col.maxWidth);
        const clampedDelta = width - currentWidth;

        // determine max delta allowed by adjacent col
        const currentAdjacentWidth = Math.round(adjacentCol.width);
        const adjacentWidth = isUndefined(adjacentCol.maxWidth) ?
            Math.max(currentAdjacentWidth - clampedDelta, adjacentCol.minWidth) :
            clamp(currentAdjacentWidth - clampedDelta, adjacentCol.minWidth, adjacentCol.maxWidth);
        const adjacentClampedDelta = adjacentWidth - currentAdjacentWidth;

        if (adjacentWidth < adjacentCol.minWidth) {
            return;
        }

        // use the (absolute) min of the two deltas
        const {sign} = Math as any;
        const actualDelta = Math.abs((Math.abs(clampedDelta) <= Math.abs(adjacentClampedDelta))
                ? clampedDelta : adjacentClampedDelta) * sign(clampedDelta);

        col.width = currentWidth + actualDelta;
        adjacentCol.width = currentAdjacentWidth - actualDelta;

        this.setState({
            columns: columns
        });
    }

    render() {
        const {dataItems, onItemClick, onItemFavorite, onItemDelete, selectedItem, sheet, sortObject} = this.props;
        const {columns} = this.state;
        const selectedItemId = !isNull(selectedItem) ? selectedItem.inventoryItemId : null;
        const data = dataItems.map(item => convertToTableDataItem({item, selectedItemId, onItemFavorite, onItemDelete}));
        const sorting = tSortByTypeToTSortByTable(sortObject);
        const tableProps = {
            active: {
              comparator: (selected, item) => selected && selected.inventoryItemId === item.inventoryItemId,
              classes: {
                  active: sheet.classes['active'],
                  inactive: sheet.classes['inactive']
              },
              item: selectedItem
            },
            columns: columns,
            data: data,
            keyField: 'inventoryItemId',
            onColumnResize: this._onColumnResize,
            onColumnResizeEnd: this._onColumnResize,
            onRowClick: onItemClick,
            onRowMouseOver: this._onItemMouseOver,
            onRowMouseLeave: this._onItemMouseLeave,
            onSort: this._onSort,
            className: sheet.classes['preview-table'],
            sorting: {key: sorting.sort, order: sorting.order}
        };

        return (
            <Table {...tableProps} />
        );
    }
}

/**
 * Mapping function to convert between ESortTable to ESort
 * @param key the key
 * @returns {ESort} the corresponding ESort type
 */
function eSortTableToESort(key: ESortTable): ESort {
    switch (key) {
        case ESortTable.FAVORITES:
            return ESort.FAVORITES;
        case ESortTable.NAME:
            return ESort.NAME;
        case ESortTable.ASSOCIATED_ITEMS:
            return ESort.ASSOCIATED_ITEMS;
        case ESortTable.OWNER:
            return ESort.OWNER;
        case ESortTable.MODIFIED:
            return ESort.MODIFIED;
    }
}

/**
 * Mapping function to convert between ESort to ESortTable
 * @param key the key
 * @returns {ESortTable} the corresponding ESortTable type
 */
const eSortToESortTable = (key: ESort): ESortTable => {
    switch (key) {
        case ESort.ASSOCIATED_ITEMS:
            return ESortTable.ASSOCIATED_ITEMS;
        case ESort.FAVORITES:
            return ESortTable.FAVORITES;
        case ESort.NAME:
            return ESortTable.NAME;
        case ESort.OWNER:
            return ESortTable.OWNER;
        case ESort.MODIFIED:
            return ESortTable.MODIFIED;
    }
};

/**
 * Mapping function to convert between TSortByTpe to TSortByTable
 * @param key the key
 * @returns {TSortByTable} the corresponding TSortByTable type
 */
const tSortByTypeToTSortByTable = (byType: TSortByType): TSortByTable => {
    return {
        sort: eSortToESortTable(byType.sort),
        order: byType.sortOrder
    };
};

/**
 * Creates a span element with a given title with same text as the body
 * @param title the text to use as title and body
 * @returns {any} the JSX span element
 */
const createElementWithTitle = (title: string): JSX.Element => {
    return (<span title={title}>
            {title}
            </span>);
};

/**
 * Creates a data source element
 * @param item the item to create the element from
 * @returns {any}
 */
const createDataSource = (item: TDataItem): JSX.Element => {
    if (item.associatedItems.length <= 1) {
        return createElementWithTitle(item.label);
    }

    // This and accompanying styles inspired by http://stackoverflow.com/a/36470401/2813832
    const joinedNames = chain<TDataAssociatedItem>(item.associatedItems)
        .map<string>('name')
        .orderBy(name => name.toLowerCase(), ['asc'])
        .value()
        .join(', ')
        .slice(0, -2);

    return (<span
        data-content={joinedNames}
        data-length={item.associatedItems.length}
        title={joinedNames} />);
};

/**
 * Gets the new sorting state for the table
 * @param key the key to sort on
 * @param columns the columns to apply the sorting to
 * @returns {TTableColumn[]} the new columns
 */
const getNewSortingState = (key: ESortTable, columns: Array<TTableColumn>): Array<TTableColumn> => {
    return columns.map(col => {
        col.sorting.showSortToggle = col.key === key && key !== ESortTable.FAVORITES;

        return col;
    });
};

/**
 * Converts a TDataItem to a TTableDataItem
 * @param item the item to convert
 * @param onItemFavorite favoriting callback
 * @returns {({}&TDataItem)|({}&TDataItem&{favoriteIcon: any, nameElement: any, dataSourceElement: JSX.Element, ownerElement: JSX.Element, dateElement: JSX.Element})|({}&TDataItem&{favoriteIcon: any, nameElement: any, dataSourceElement: JSX.Element, ownerElement: JSX.Element, dateElement: JSX.Element}&TSource3)|({}&TDataItem&{favoriteIcon: any, nameElement: any, dataSourceElement: JSX.Element, ownerElement: JSX.Element, dateElement: JSX.Element}&TSource3&TSource4)|{}|TResult}
 */

export interface TableItemData {
    item: TDataItem;
    selectedItemId: string|null;
    onItemFavorite: (item: TDataItem) => void;
    onItemDelete: (item: TDataItem) => void;
}

const convertToTableDataItem = (config: TableItemData): TTableDataItem => {
    const {item, selectedItemId, onItemFavorite, onItemDelete} = config;
    const {inventoryItemId} = item;
    const isActiveItem = isEqual(inventoryItemId, selectedItemId);
    const favoriteIconProps = {
        name: item.favorite || isActiveItem ? 'favorite_dark' : 'favorite_light',
        className: classNames('item-fav-icon', { 'favorite': item.favorite }),
        hoverColor: item.favorite ? '#68AD45' : '#939393',
        onClick: (e) => {
            e.stopPropagation();
            onItemFavorite(item);
        }
    };

    const deleteButtonProps = {
        name: 'remove_dark',
        color: '#B2B2B2',
        hoverColor: item.deletable ?  '#D46137'  : '#B2B2B2',
        className: 'del-btn',
        onClick: (e) => {
            e.stopPropagation();
            if (item.deletable) {
                onItemDelete(item);
            }
        }
    };

    const name = (<span title={item.name}>
             <SvgIcon name={getVisTypeIcon(item.type)}/>
        {item.name}
         </span>);
    const FavIcon = (<span title={item.favorite ? 'Remove from Favorites' : 'Add to Favorites'}>
              <SvgIcon {...favoriteIconProps} />
         </span>);

    const deleteButton = (
        <span title={item.deletable ? 'Delete Item' : `You don't have permission to delete the dashboard`}
              style={{cursor: item.deletable ? 'pointer' : 'not-allowed'}}
        >
              <SvgIcon {...deleteButtonProps} />
         </span>
    );

    const dataSource = createDataSource(item);
    const owner = createElementWithTitle(item.ownerFullName);
    const date = createElementWithTitle(item.readableDate);

    return assign({}, item, {
        favoriteIcon: FavIcon,
        nameElement: name,
        dataSourceElement: dataSource,
        ownerElement: owner,
        dateElement: date,
        deleteButton,
    });
};

export default injectSheet(TableStyles)(TableView);
