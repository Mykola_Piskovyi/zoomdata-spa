export default {
    'preview-table': {
        '& > tbody > tr': {
            cursor: 'pointer'
        },

        '& > tbody': {
            '& > tr': {
                'height': '32px',
                'border': 'none',
                'border-bottom': '1px solid #E6E6E6',
            },
            '& > tr > td': {
                'padding': '6px 8px 6px 8px',
                'font-family': `'Source Sans Pro', sans-serif`,
                'font-weight': '400',
                'font-size': '14px',
                'line-height': '18px',
                'border-style': 'none',

                'text-overflow': 'ellipsis',
                'white-space': 'nowrap',
                'overflow': 'hidden',

                '& .item-fav-icon': {
                    'height': '18px',

                    '&.favorite': {
                        'color': '#68AD45'
                    }
                },

                '& > span > svg': {
                    'margin-right': '8px',
                    'height': '17px',
                    'line-height': '17px',
                    'width': '17px'
                },

                '& > span[data-content]:before': {
                    'display': 'inline-block',
                    'overflow': 'hidden',
                    'white-space': 'pre',
                    'content': 'attr(data-content)',
                    'text-overflow': 'ellipsis',
                    'max-width': 'calc(100% - 20px)'
                },

                '& > span[data-length]:after': {
                    'display': 'inline-block',
                    'min-width': '20px',
                    'overflow': 'hidden',
                    'white-space': 'pre',
                    'content': `" ("attr(data-length)")"`,
                    'text-overflow': 'clip',
                    'direction': 'rtl'
                }
            },

            '& > tr > td:nth-of-type(2):hover': {
                'text-decoration': 'underline'
            }
        },

        '& > thead': {
            'background': '#D2D2D2',
            'color': '#595959',

            '& tr > th': {
                'height': '32px',
                'border-color': '#FFFFFF',
                'border': 'none',
                'border-right': 'none',
                'padding-left': '8px',

                '&:last-child': {
                    'border-right': 'none'
                },

                'font-family': `'Source Sans Pro', sans-serif`,
                'font-size': '14px',
                'font-weight': '700',
                'color': '#595959',
                'text-align': 'left',
                'line-height': '18px',

                '& > div': {
                    'border-color': '#D2D2D2'
                }
            }
        }
    },
    'active': {
        'background-color': '#595959',
        'color': '#ffffff',

        '&:hover': {
            'background-color': '#404040'
        },

        '& > td': {
            '& .item-fav-icon': {
                color: '#FFFFFF'
            }
        }
    },
    'inactive': {
        'color': '#323232',

        '&:hover': {
            'background-color': '#f1f1f1'
        },

        '& > td:nth-of-type(2)': {
            'color': '#0095b6'
        },

        '& > td': {
            '& .item-fav-icon': {
                color: '#939393'
            }
        }
    }
};
