import {expect} from 'chai';
import * as sinon from 'sinon';
import {mount} from 'enzyme';
import * as React from 'react';
import {TableView} from '../Table';
import {DATA_ITEMS} from '../../ContentBody/tests/ContentBody.mock';
import {TABLE_COLUMNS} from '../../../SETTINGS';

import {assign} from 'lodash';
import {ESort, ESortOrder} from '../../../model/types';
const lolex = require('lolex');

describe('Table Unit Tests', () => {
    let wrapper;
    let sandbox;
    let props;
    let clock;

    const getProps = (options?) => {
        return assign({
            dataItems: DATA_ITEMS,
            selectedItem: null,
            onItemFavorite: sandbox.stub(),
            onItemDelete: sandbox.stub(),
            onItemClick: sandbox.stub(),
            onSortChange: sandbox.stub(),
            setSelectedItem: sandbox.stub(),
            sheet: {
                classes: {
                    'preview-table': {}
                }
            },
            sortObject: {
                sort: ESort.NAME,
                sortOrder: ESortOrder.ASC
            }
        }, options);
    };

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        props = getProps();
        wrapper = mount(<TableView {...props} />);
        clock = lolex.install();
    });

    afterEach(() => {
        sandbox.restore();
        wrapper.unmount();
        clock.uninstall();
    });

    describe('mousing over', () => {
        it('can mouse over to select an item', () => {
            const TableItems = wrapper.find('tbody').find('tr');

            TableItems.first().simulate('mouseOver');

            clock.tick(300);
            expect(props.setSelectedItem.called).to.be.true;
        });

        it('selects a second item', () => {
            const TableItems = wrapper.find('tbody').find('tr');

            TableItems.first().simulate('mouseOver');

            clock.tick(10);

            TableItems.first().simulate('mouseLeave');
            TableItems.last().simulate('mouseOver');

            expect(props.setSelectedItem.called).to.be.false;

            clock.tick(300);

            expect(props.setSelectedItem.called).to.be.true;
        });

        it('does not select the first item if hovering back to it before the timeout', () => {
            const TableItems = wrapper.find('tbody').find('tr');

            TableItems.first().simulate('mouseOver');

            clock.tick(50);

            TableItems.first().simulate('mouseLeave');
            TableItems.last().simulate('mouseOver');

            expect(props.setSelectedItem.called).to.be.false;

            clock.tick(10);

            expect(props.setSelectedItem.called).to.be.false;

            TableItems.last().simulate('mouseLeave');
            TableItems.first().simulate('mouseOver');

            clock.tick(210);

            expect(props.setSelectedItem.called).to.be.false;
        });
    });

    describe('sorting', () => {
        it('should have an initial sort deeply equal to TABLE_COLUMNS', () => {
            expect(wrapper.state('columns')).to.deep.equal(TABLE_COLUMNS);
        });

        describe('receiving props', () => {
            it('should not call onSortChange when sort object props are equal', () => {
               wrapper.setProps(getProps());

                expect(props.onSortChange.calledTwice).to.be.false;
            });

            it('should call onSortChange when sort object props are not equal', () => {
                const newProps = getProps();
                newProps.sortObject = { sort: ESort.MODIFIED, sortOrder: ESortOrder.DESC };

                wrapper.setProps(newProps);

                expect(props.onSortChange.called).to.be.true;
            });

        });

        it('should select an item when a row is clicked', () => {
            wrapper.setProps({selectedItem: DATA_ITEMS[0]}).find('tr').at(1).simulate('click');

            expect(props.onItemClick.args[0][0].inventoryItemId)
                .to.equal(DATA_ITEMS[0].inventoryItemId);
            expect(props.onItemClick.args[0][1]).to.equal(0);
        });

        it('should favorite an item when the star icon is clicked', () => {
            wrapper.find('tr').at(1).find('SvgIcon').first().simulate('click');

            expect(props.onItemFavorite.args[0][0].inventoryItemId)
                .to.be.equal(DATA_ITEMS[0].inventoryItemId);
        });

        it('should delete an item when the DeleteButton is clicked', () => {
            expect(props.onItemDelete.called).to.be.false;

            wrapper.find('tr')
                .at(1)
                .find('.del-btn')
                .simulate('click');

            expect(props.onItemDelete.called).to.be.true;

        });
    });
});