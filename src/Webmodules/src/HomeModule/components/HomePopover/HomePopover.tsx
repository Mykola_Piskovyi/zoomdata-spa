/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as _ from 'lodash';
import * as React from 'react';
const { AppPopover, PopoverListItem } = require('zd-components');

import {getVisTypeIcon} from '../../../utilities/utilities';

const filterCallback = (children: Array<JSX.Element>, searchVal: string = ''): Array<JSX.Element> =>
    children.filter(child => child.props.labelText.toLowerCase().indexOf(searchVal) > -1);

const sortSources = (sources) => (_.sortBy(sources, (i: any) => i.name.toLowerCase()));

export interface IHomePopoverProps {
    handlePopoverClick(value, popoverClick?: any): any;
    onClose(value, popoverClick?: any): any;
    showBackButton?: boolean;
    sources: Array<any>;
}

export default class HomePopover extends React.Component<IHomePopoverProps, any> {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: 1,
            currentList: sortSources(this.props.sources),
        };
        _.bindAll(this, ['handleClick', 'handleBackButtonClick']);
    }

    handleClick(value, currentStep) {
        if (currentStep === 1) {
            value.model.visualizations.bootstrap().then(vis => {
                this.setState({
                    currentStep: 2,
                    currentList: _.sortBy(vis, 'name'),
                    currentSourceText: value.name,
                    visualizations: value.model.visualizations,
                });
            });
        }
        if (currentStep === 2) {
            const { visId } = value;
            this.state.visualizations.findWhere({visId}).bootstrap()
                .then(value => {
                    this.props.handlePopoverClick(value, currentStep);
                });
        }
    }

    handleBackButtonClick() {
        const { currentStep } = this.state;
        if (currentStep === 2) {
            this.setState({
                currentStep: 1,
                currentList: sortSources(this.props.sources),
                currentSourceText: ''
            });
        }
    }

    render() {
        const { onClose} = this.props;
        const { currentStep, currentList, currentSourceText } = this.state;
        const items = currentList.map((f) => {
            const iconProps = currentStep === 1 ?
                {iconSrc: f.iconSrc} :
                {iconName: getVisTypeIcon(f.type)};

            return <PopoverListItem
                {...iconProps}
                key={f.name}
                value={f.name}
                labelText={f.name}
                onClick={(value, step) => this.handleClick(f, currentStep)}
            />;
        });

        return (
            <AppPopover
                onClose={onClose}
                style={{zIndex: 1000}}
                onBackButtonClick={this.handleBackButtonClick}
                showBackButton={currentStep === 2}
                headerText='Start With a Chart'
                headerSubtext={
                    currentStep === 1 ?
                    'Step 1/2: Select Data Source' :
                    'Step 2/2: Select Chart'
                }
                headerSourceText={currentSourceText}
                filterCallback={filterCallback}
                scrollable={true}
            >
                {items}
            </AppPopover>
        );
    }
}
