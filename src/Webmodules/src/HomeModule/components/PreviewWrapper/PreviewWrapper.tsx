/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import { bindAll, isNull } from 'lodash';
import * as React from 'react';

import Preview, {IPreviewProps} from './Preview/Preview';
import { TDataItem } from '../../model/types';

import {injectSheet} from '../../../utilities/jssSetup/JssUtils';
import PreviewWrapperStyles from './PreviewWrapperStyles';

const { SvgIcon } = require('zd-components');

export interface IPreviewWrapperProps extends IPreviewProps {
    sheet?: any;
}

export const PreviewInitialView = (props: IPreviewWrapperProps) => {
    const { classes } = props.sheet;
    return (
        <div className={ classes['zdView-PreviewInitialView'] }>
            <div className='icon'>
            <SvgIcon name='visualization_type' preserveAspectRatio='xMidYMid meet' />
        </div>
        <div className='description'>
            Hover on a row in the list to preview, <br />
            click an item to open.
        </div>
    </div>
    );
};

export class PreviewWrapper extends React.Component<IPreviewWrapperProps, {}> {
    constructor(options) {
        super(options);
    }

    render() {
        const { selectedItem } = this.props;
        const { classes } = this.props.sheet;

        return (
            <table className={ classes['zdView-PreviewWrapper'] }>
                <tbody>
                    <tr>
                        <th>Preview</th>
                    </tr>
                    <tr>
                        <td>
                            { isNull(selectedItem) ?
                                <PreviewInitialView {...this.props} /> :
                                <Preview {...this.props} />
                            }
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    }
}

export default injectSheet(PreviewWrapperStyles)(PreviewWrapper);
