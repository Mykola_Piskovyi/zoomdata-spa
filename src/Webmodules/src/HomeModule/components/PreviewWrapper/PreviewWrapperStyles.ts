/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

export default {
    'zdView-PreviewWrapper': {
        color: '#595959',
        height: '100%',
        width: '100%',

        '& th': {
            background: '#D2D2D2',
            width: '100%',
            height: '32px',
            padding: '0 8px 0 8px',
            textAlign: 'left',
            fontSize: '14px'
        },

        '& td': {
            background: '#FFFFFF',
            verticalAlign: 'top',
        }
    },

    'zdView-PreviewInitialView': {
        color: '#D2D2D2',
        textAlign: 'center',
        marginTop: '30px',

        '& .icon': {
            '& svg': {
                width: '80px',
                height: '80px',
                display: 'block',
                margin: '0 auto'
            }
        },

        '& .description': {
            marginTop: '16px',
            color: '#939393',
            fontSize: '13px',
            lineHeight: '16px'
        }
    }
};
