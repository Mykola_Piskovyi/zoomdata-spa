/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

export default {
    'zdView-Preview': {

        'max-width': '280px',

        '& .preview-image-wrapper': {
            width: '258px',
            height: '145px',
            margin: '10px',
            border: '1px solid #E6E6E6',
            boxShadow: 'rgba(0, 0, 0, 0.2) 2px 2px 10px 0px',
            overflow: 'hidden',
            position: 'relative',

            '& .preview-image': {
                position: 'absolute',
                margin: 'auto',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0
            }
        },

        '& .preview-info-wrapper': {
            margin: '10px',

            '& table': {
                width: '100%',

                '& tr': {

                    '&.row': {
                        width: '100%',
                        lineHeight: '18px',
                        borderBottom: '8px solid transparent'
                    },

                    '& td': {
                        fontSize: '14px',

                        '&.preview-label': {
                            fontWeight: 'bold'
                        },
                        '&:first-child': {
                            width: '40%',
                            textAlign: 'right',
                            color: '#b2b2b2'
                        },
                        '&:last-child': {
                            width: '60%',
                            wordBreak: 'break-word',
                            color: '#323232',
                            paddingLeft: '8px'
                        },
                        '& .delete-button': {
                            cursor: 'pointer',

                            '& svg': {
                                width: '17px'
                            }
                        }
                    }
                }
            }
        }
    }
};
