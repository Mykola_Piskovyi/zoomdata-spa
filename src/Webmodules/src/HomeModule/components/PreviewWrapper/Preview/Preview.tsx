/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import {bindAll, isEmpty} from 'lodash';
import * as React from 'react';

import { TDataItem } from '../../../model/types';
import {injectSheet} from '../../../../utilities/jssSetup/JssUtils';
import PreviewStyles from './PreviewStyles';
import ScreenContainer from '../../ScreenContainer/ScreenContainer';
import { ANIMATION_TIMEOUT_150MS } from '../../../SETTINGS';

export interface IPreviewProps {
    selectedItem: TDataItem;
    onItemDelete(item: TDataItem): void;
    onItemClick(item: TDataItem): void;
    sheet?: any;
}

export class Preview extends React.Component<IPreviewProps, { hovered?: boolean; clickable?: boolean; }> {
    private _mouseOverTimeout;
    constructor(options) {
        super(options);

        bindAll(this, '_onDelete', '_onMouseOver', '_onMouseLeave', '_onItemClick');

        this.state = {
            hovered: false,
            clickable: false
        };
    }

    componentWillUnmount() {
        clearTimeout(this._mouseOverTimeout);
    }

    private _onMouseOver(): void {
        this.setState({hovered: true});

        this._mouseOverTimeout = setTimeout(() => {
            this.setState({clickable: true});
        }, ANIMATION_TIMEOUT_150MS);
    }

    private _onMouseLeave(): void {
        this.setState({hovered: false, clickable: false});

        clearTimeout(this._mouseOverTimeout);
    }

    private _onDelete(): void {
        this.props.onItemDelete(this.props.selectedItem);
    }

    private _onItemClick(item: TDataItem): void {
        this.props.onItemClick(item);
    }

    render() {
        const { hovered, clickable } = this.state;
        const { selectedItem } = this.props;
        const { classes } = this.props.sheet;
        const { thumbnailImageUrl, name, typeName, ownerFullName, readableDate, deletable } = selectedItem;

        const description = !isEmpty(selectedItem.description) ?
            selectedItem.description : 'N/A';

        const sourceLabels = selectedItem.associatedItems
            .map(item => (<div key={ item.name } >{ item.name }</div>));

        const screenContainerProps = {
            item: selectedItem,
            ref: 'thumbnailImageUrl',
            src: thumbnailImageUrl,
            onOpen: this._onItemClick,
            hovered: hovered,
            clickable: clickable,
            withInfo: false
        };

        const screenWrapperProps = {
            onMouseOver: this._onMouseOver,
            onMouseLeave: this._onMouseLeave
        };

        return (
            <div className={ classes['zdView-Preview'] } key={selectedItem.name}>
                <div className='preview-image-wrapper' {...screenWrapperProps}>
                    <ScreenContainer {...screenContainerProps} />
                </div>
                <div className='preview-info-wrapper'>
                    <table>
                        <tbody>
                            <tr className='row'>
                                <td className='preview-label'>Name:</td>
                                <td className='text' ref='name'>{ name }</td>
                            </tr>
                            <tr className='row'>
                                <td className='preview-label'>Type:</td>
                                <td className='text' ref='typeName'>{ typeName }</td>
                            </tr>
                            <tr className='row'>
                                <td className='preview-label'>Data Source:</td>
                                <td className='text' ref='itemSources'>{ sourceLabels }</td>
                            </tr>
                            <tr className='row'>
                                <td className='preview-label'>Owner:</td>
                                <td className='text' ref='ownerFullName'>{ ownerFullName }</td>
                            </tr>
                            <tr className='row'>
                                <td className='preview-label'>Date Modified:</td>
                                <td className='text' ref='readableDate'>{ readableDate }</td>
                            </tr>
                            <tr className='row'>
                                <td className='preview-label'>Description:</td>
                                <td className='text' ref='description'>{ description }</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default injectSheet(PreviewStyles)(Preview);
