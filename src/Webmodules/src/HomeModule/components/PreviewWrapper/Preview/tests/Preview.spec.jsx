/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import React, { createElement } from 'react';
import { assign, clone } from 'lodash';
import { shallow, mount } from 'enzyme';

import { Preview } from '../Preview.tsx';

import { DASHBOARD_ITEM } from '../../../../tests/HomeModule.mock.ts';

describe('Preview Component unit test: ', () => {
    let previewComponent;
    let props;

    beforeEach(()=> {
        props = {
            selectedItem: DASHBOARD_ITEM,
            sheet: {
                classes: {
                    'zdView-Preview': {}
                }
            }
        }
    });

    describe('WHEN Preview component is initialized: ', () => {
        beforeEach(() => {
            previewComponent = shallow(<Preview {...props} />);
        });

        it('should be defined', () => {
            expect(previewComponent).to.exist;
        });
    });

    describe('WHEN Preview component is rendered: ', () => {

        afterEach(() => {
            previewComponent.unmount();
        });

        [{
            description: 'should render correctly',
            getProps: (props) => {
                return props;
            },
            expected: (component) => {
                let previewImage = component.ref('thumbnailImageUrl');
                const { thumbnailImageUrl, name, typeName, ownerFullName, readableDate, description } = props.selectedItem;
                const { src } = previewImage.props();

                expect(src).to.equal(thumbnailImageUrl);
                expect(component.ref('name').text()).to.equal(name);
                expect(component.ref('typeName').text()).to.equal(typeName);
                expect(component.ref('ownerFullName').text()).to.equal(ownerFullName);
                expect(component.ref('readableDate').text()).to.equal(readableDate);
                expect(component.ref('description').text()).to.equal(description);
            }

        }].map((testCase) => {
                it(`${testCase.description} `, () => {
                    previewComponent = mount(
                        <Preview {...testCase.getProps(props)} />);

                    testCase.expected(previewComponent);
                });
            });
    });
});