/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import React, { createElement } from 'react';
import { assign } from 'lodash';
import { mount, shallow } from 'enzyme';
import { PreviewWrapper, PreviewInitialView } from '../PreviewWrapper.tsx';
import { Preview } from '../Preview/Preview.tsx';

import { DASHBOARD_ITEM } from '../../../tests/HomeModule.mock.ts';

describe('PreviewWrapper Component unit test: ', () => {
    let previewWrapperComponent;
    let props;

    beforeEach(()=> {
        props = {
            selectedItem: DASHBOARD_ITEM,
            sheet: {
                classes: {
                    'zdView-PreviewWrapper': {}
                }
            }
        }
    });

    describe('WHEN PreviewWrapper component is initialized: ', () => {
        beforeEach(() => {
            previewWrapperComponent = shallow(<PreviewWrapper {...props} />);
        });

        it('should be defined', () => {
            expect(previewWrapperComponent).to.exist;
        });
    });


    describe('WHEN PreviewWrapper component is rendered: ', () => {
        [{
            description: 'should render preview initial view',
            getProps: (props) => {
                return assign({}, props, {
                    selectedItem: null
                });
            },
            expected: (component) => {
                expect(component.find(PreviewInitialView)).to.have.length(1);
            }

        }, {
            description: 'should render dashboard item preview',
            getProps: (props) => {
                return props;
            },
            expected: (component) => {
                expect(component.find(Preview)).to.have.length(1);
            }
        }].map((testCase) => {
                it(`${testCase.description} `, () => {
                    previewWrapperComponent = mount(
                        <PreviewWrapper {...testCase.getProps(props)} />);

                    testCase.expected(previewWrapperComponent);
                });
            });
    });
});
