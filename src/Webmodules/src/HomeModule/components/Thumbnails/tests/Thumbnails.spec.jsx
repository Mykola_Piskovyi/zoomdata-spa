import {assign, clone} from 'lodash';
import React  from 'react';
import {expect} from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';

import {DATA_ITEMS} from '../../../tests/HomeModule.mock';
import Thumbnails from '../Thumbnails';
import ThumbnailItem from '../../ThumbnailItem/ThumbnailItem';

describe('Thumbnails Unit Tests: ', function() {

    let sandbox;
    let props;
    let thumbnails;

    const getProps = (props) => {
        return assign({
            dataItems: clone(DATA_ITEMS),
            onItemClick: sandbox.stub(),
            onItemDelete: sandbox.stub(),
            onItemFavorite: sandbox.stub()
        }, props);
    };

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        props = getProps();

        thumbnails = shallow(<Thumbnails {...props}/>);
    });

    afterEach(() => {
        sandbox.restore();
        thumbnails.unmount();
    });

    it('should be defined: ', () => {
        expect(Thumbnails).to.exist;
        expect(thumbnails).to.exist;
    });

    describe('Rendering: ', () => {

        it('should render all dataItems: ', () => {
            expect(thumbnails.find(ThumbnailItem))
                .to.have.length(props.dataItems.length);
        });

        it('can render empty dataItems: ', () => {
            props = getProps({dataItems: []});

            thumbnails = shallow(<Thumbnails {...props}/>);
            expect(thumbnails.find(ThumbnailItem)).to.have.length(0);
        });

    });

    describe('Handle events for hovered item: ', () => {

        let item;
        let dataItem;
        beforeEach(() => {
            dataItem = props.dataItems[0];
            item = thumbnails.find(ThumbnailItem).get(0);
        });

        it('should handle "onClick" call props.onItemClick() with dataItem: ', () => {
            item.props.onClick();

            expect(props.onItemClick.called).to.be.true;
            expect(props.onItemClick.getCall(0).args).to.eql([dataItem]);
        });

        it('should handle "onDelete" call props.onItemDelete() with dataItem: ', () => {
            item.props.onDelete(new Event('click'));

            expect(props.onItemDelete.called).to.be.true;
            expect(props.onItemDelete.getCall(0).args).to.eql([dataItem]);
        });

        it('should handle "onFavorite" call props.onItemFavorite() with dataItem: ', () => {
            item.props.onFavorite(new Event('click'));

            expect(props.onItemFavorite.called).to.be.true;
            expect(props.onItemFavorite.getCall(0).args).to.eql([dataItem]);
        });
    });

});