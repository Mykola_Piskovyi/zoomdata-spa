/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';

import {TDataItem} from '../../model/types';
import ThumbnailItem from '../ThumbnailItem/ThumbnailItem';

export interface IThumbnailsProps {
    dataItems: Array<TDataItem>;
    onItemClick(item: TDataItem): void;
    onItemDelete(item: TDataItem): void;
    onItemFavorite(item: TDataItem): void;
}

class Thumbnails extends React.Component<IThumbnailsProps, {}> {

    render() {

        const {dataItems, onItemClick, onItemDelete, onItemFavorite} = this.props;
        const items = dataItems.map(item => {
            const itemProps = {
                item: item,
                key: item.name,
                onClick: () => (onItemClick(item)),
                onDelete: (e) => {
                    onItemDelete(item);
                    e.stopPropagation();
                },
                onFavorite: (e) => {
                    onItemFavorite(item);
                    e.stopPropagation();
                }
            };
            return <ThumbnailItem {...itemProps} />;
        });

        return (
            <div className='zdView-Thumbnails'>
                {items}
            </div>
        );

    }

}

export default Thumbnails;
