/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

export default {
    'zdView-ThumbnailItem': {
        display: 'inline-block',
        verticalAlign: 'top',
        marginBottom: '20px',
        marginRight: '5px',
        marginLeft: '5px',
        cursor: 'pointer',
        position: 'relative',

        '@media (max-width: 768px)': {
            '&': {
                width: 'calc(100% - 10px)'
            }
        },

        '@media (min-width: 768px) and (max-width: 880px)': {
            '&': {
                width: 'calc(50% - 10px)'
            }
        },

        '@media (min-width: 880px) and (max-width: 1160px)': {
            '&': {
                width: 'calc(33% - 8px)'
            }
        },

        '@media (min-width: 1160px) and (max-width: 1200px)': {
            '&': {
                width: 'calc(33% - 7px)'
            }
        },

        '@media (min-width: 1200px) and (max-width: 1400px)': {
            '&': {
                width: 'calc(25% - 10px)'
            }
        },

        '@media (min-width: 1400px)': {
            '&': {
                width: 'calc(20% - 10px)'
            }
        },

        '& .item-bottom-text': {
            textAlign: 'center',
            height: '100%',

            '& .item-name': {
                color: '#595859',
                lineHeight: '18px',
                paddingTop: '2px',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                width: '100%',
                whiteSpace: 'nowrap',
                fontWeight: 'bold',
                fontSize: '14px'
            },

            '& .item-sub-title': {
                maxWidth: '100%',
                color: '#939393',
                fontSize: '10px',
                lineHeight: '10px',
                textTransform: 'uppercase',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis'
            }
        },

        '& .header-item-controls': {
            opacity: 1,
            animation: 'show 150ms',
            position: 'absolute',
            width: 'inherit',
            zIndex: 3,
            right: '10px',
            top: '10px',

            '& .del-btn': {
                marginRight: '4px',
                lineHeight: '22px'
            },

            '& .fav-btn, .del-btn': {
                float: 'right',
                width: '26px',
                height: '26px',
                border: '1px solid #d2d2d2',
                background: '#e6e6e6',
                color: '#939393',
                cursor: 'pointer',
                position: 'relative',

                '& svg': {
                    width: '18px',
                    display: 'block',
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    margin: 'auto'
                }
            }
        },

        '&.favorite': {
            '& .header-item-controls': {
                '& .fav-btn': {
                   '& svg': {
                       color: '#68AD45'
                   }
                }
            }
        }
    },

    '@keyframes show': {
        from: {opacity: 0},
        to: {opacity: 1}
    }
};
