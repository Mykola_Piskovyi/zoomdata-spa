import {assign, clone, find} from 'lodash';
import React  from 'react';
import {expect} from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
const lolex = require('lolex');

import {DATA_ITEMS} from '../../../tests/HomeModule.mock';
import { ANIMATION_TIMEOUT_150MS } from '../../../SETTINGS';
import {ThumbnailItem} from '../ThumbnailItem';
import {ScreenContainer} from '../../ScreenContainer/ScreenContainer';
import { IconButton, DeleteButton } from 'zd-components';

describe('ThumbnailItem Unit Tests: ', function() {

    let sandbox;
    let props;
    let thumbnail;
    let clock;

    const getProps = (props) => {
        let deletableItem = clone(DATA_ITEMS[0]);
        return assign({
            item: deletableItem,
            onClick: sandbox.stub(),
            onDelete: sandbox.stub(),
            onFavorite: sandbox.stub(),
            sheet: {
                classes: {}
            }
        }, props);
    };

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        props = getProps();
        clock = lolex.install();

        thumbnail = mount(<ThumbnailItem {...props}/>);
    });

    afterEach(() => {
        sandbox.restore();
        thumbnail.unmount();
        clock.uninstall();
    });

    it('should be defined: ', () => {
        expect(ThumbnailItem).to.exist;
        expect(thumbnail).to.exist;
    });

    describe('Rendering: ', () => {

        it('should render proper data item "name": ', () => {
            const {name} = props.item;
            expect(thumbnail.find('.item-name').text()).to.eql(name);
        });

        it('should have "state.hovered: false" by default', () => {
            expect(thumbnail.state('hovered')).to.be.false;
        });

        it('should have "ScreenContainer" by default ', () => {
            expect(thumbnail.find(ScreenContainer)).to.have.length(1);
        });

        it('should change state for "hovered: true" for "onMouseOver" event', () => {
            thumbnail.simulate('mouseover');
            clock.tick(ANIMATION_TIMEOUT_150MS);
            expect(thumbnail.state('hovered')).to.be.true;
        });

        it('should change state for "hovered: false" for "onMouseLeave" event', () => {
            thumbnail.setState({hovered: true});
            thumbnail.simulate('mouseleave');

            expect(thumbnail.state('hovered')).to.be.false;
        });

        describe('hovered state tests: ', () => {
            beforeEach(() => {
                thumbnail.setState({hovered: true});
            });

            it('should have "state.hovered: true" for next tests', () => {
                expect(thumbnail.state('hovered')).to.be.true;
            });

            it('should have "ScreenContainer", "IconButton", "DeleteButton" ', () => {
                expect(thumbnail.find(ScreenContainer)).to.have.length(1);
                expect(thumbnail.find('.fav-btn')).to.have.length(1);
                expect(thumbnail.find('.del-btn')).to.have.length(1)
            });

            it('should NOT have "DeleteButton" for non-deletable item ', () => {
                const notDeletableItem = find(DATA_ITEMS, {deletable: false});

                props = getProps({item: notDeletableItem});
                thumbnail = mount(<ThumbnailItem {...props}/>);
                thumbnail.setState({hovered: true});

                expect(thumbnail.find('.del-btn')).to.have.length(0);
            });

            describe('handle events: ', () => {

                it('should handle onClick() for open item within ScreenContainer', () => {
                    thumbnail
                        .setState({clickable: true})
                        .find('.open-btn')
                        .simulate('click');

                    expect(props.onClick.called).to.be.true;
                });

                it('should handle onFavorite() for open item within IconButton', () => {
                    thumbnail
                        .find('.fav-btn')
                        .simulate('click');

                    expect(props.onFavorite.called).to.be.true;
                });

                it('should handle onDelete() for open item within DeleteButton', () => {
                    thumbnail
                        .find('.del-btn')
                        .simulate('click');

                    expect(props.onDelete.called).to.be.true;
                });

            });

        });

    });

});
