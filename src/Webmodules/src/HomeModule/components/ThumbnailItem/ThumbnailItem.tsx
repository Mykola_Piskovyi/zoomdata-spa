/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';
import * as moment from 'moment';
import { bindAll } from 'lodash';
const { IconButton, DeleteButton } = require('zd-components');

import {TDataItem} from '../../model/types';
import ScreenContainer from './../ScreenContainer/ScreenContainer';
import { injectSheet } from '../../../utilities/jssSetup/JssUtils';
import ThumbnailItemStyles from './ThumbnailItemStyles';
import { ANIMATION_TIMEOUT_150MS } from '../../SETTINGS';

const classNames = require('classnames');

export interface IThumbnailItemProps {
    item: TDataItem;
    onClick(): void;
    onDelete(): void;
    onFavorite(): void;
    sheet?: any;
}

export interface IState {
    hovered?: boolean;
    clickable?: boolean;
}

export class ThumbnailItem extends React.Component<IThumbnailItemProps, IState> {
    private _mouseOverTimeout;
    constructor(options) {
        super(options);

        bindAll(this, '_onMouseOver', '_onMouseLeave');

        this.state = {clickable: false, hovered: false};
    }

    componentWillUnmount() {
        clearTimeout(this._mouseOverTimeout);
    }

    private _onMouseOver() {
        this.setState({hovered: true});

        this._mouseOverTimeout = setTimeout(() => {
            this.setState({clickable: true});
        }, ANIMATION_TIMEOUT_150MS);
    }

    private _onMouseLeave() {
        this.setState({hovered: false, clickable: false});
    }

    render() {
        const {name, thumbnailSubTitle, favorite, deletable, thumbnailImageUrl} = this.props.item;
        const {hovered, clickable} = this.state;
        const { classes } = this.props.sheet;

        const screenWrapperProps = {
            onMouseOver: this._onMouseOver,
            onMouseLeave: this._onMouseLeave
        };

        const deleteBtn = {
            key: 'del-btn',
            className: 'del-btn',
            title: 'Delete Item',
            hoverColor: '#e5683a',
            iconName: 'remove_dark',
            onClick: this.props.onDelete
        };

        const favBtn = {
            key: 'fav-btn',
            className: 'fav-btn',
            title: favorite ? 'Remove from Favorites' : 'Add to Favorites',
            iconName: favorite ? 'favorite_dark' : 'favorite_light',
            onClick: this.props.onFavorite
        };

        const AccessControls = [];

        AccessControls.push(<IconButton ref='fav' {...favBtn} />);

        if (deletable) {
            AccessControls.push(<IconButton ref='del' {...deleteBtn} />);
        }

        const screenContainerProps = {
            item: this.props.item,
            onOpen: this.props.onClick,
            hovered: hovered,
            clickable: clickable,
            src: thumbnailImageUrl
        };

        return (
            <div className={ classNames(classes['zdView-ThumbnailItem'], {favorite: favorite}) } {...screenWrapperProps} >
                <div className='thumbnail-wrapper'>
                    {hovered ? <div className='header-item-controls'>{AccessControls}</div> : '' }
                    <ScreenContainer {...screenContainerProps} />
                </div>
                <div className='item-bottom-text'>
                    <div className='item-name'>{name}</div>
                    <div className='item-sub-title'>{thumbnailSubTitle}</div>
                </div>
            </div>
        );
    }

}

export default injectSheet(ThumbnailItemStyles)(ThumbnailItem);
