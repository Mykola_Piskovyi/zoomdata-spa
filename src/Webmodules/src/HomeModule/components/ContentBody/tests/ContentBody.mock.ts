/*
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */

const DATA_ITEMS = [{
    inventoryItemId: '582dba1e3004152cd8564c34',
    type: 'DASHBOARD',
    deletable: true,
    associatedItems: [{
        inventoryItemId: '582db9473004152cd8564bdd',
        type: 'DATA_SOURCE',
        typeName: 'Data Source',
        name: 'My FLAT FILE Source',
        favorite: false
    }],
    typeName: 'Dashboard',
    label: 'My FLAT FILE Source',
    name: 'Devs positions',
    description: 'Dashboard description',
    favorite: false,
    readableDate: 'Nov 03, 2016, 07:41 PM',
    modifiedDate: '2016-11-03 19:41:45.248',
    ownerFullName: 'admin',
    ownerUserId: '581b8135c0262260be98046a',
    thumbnailImageUrl: '/zoomdata/service/screenshot/5821f3ac3004d323416f23af/ts/2016-11-09%2015:13:43.166'
}, {
    inventoryItemId: '582dba583004152cd8564c3d',
    type: 'DASHBOARD',
    deletable: true,
    associatedItems: [{
        inventoryItemId: '582db9473004152cd8564bdd',
        type: 'DATA_SOURCE',
        typeName: 'Data Source',
        name: 'My FLAT FILE Source',
        favorite: false
    }],
    typeName: 'Dashboard',
    label: 'My FLAT FILE Source',
    name: 'Dashboard name2',
    description: 'Dashboard description',
    favorite: false,
    readableDate: 'Nov 03, 2016, 07:41 PM',
    modifiedDate: '2016-11-03 19:41:45.248',
    ownerFullName: 'admin',
    ownerUserId: '581b8135c0262260be98046a',
    thumbnailImageUrl: '/zoomdata/service/screenshot/5821f3ac3004d323416f23af/ts/2016-11-09%2015:13:43.166'
}];

export { DATA_ITEMS };
