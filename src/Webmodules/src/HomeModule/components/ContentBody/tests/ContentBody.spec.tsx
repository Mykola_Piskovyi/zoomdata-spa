/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
import {expect} from 'chai';
import * as sinon from 'sinon';
import {mount, shallow} from 'enzyme';
import * as React from 'react';
import {assign} from 'lodash';
import ContentBody from '../ContentBody';
import {EFastFilterValue, ELayout, ESort, ESortOrder} from '../../../model/types';
import {DATA_ITEMS} from './ContentBody.mock';
import Thumbnails from '../../Thumbnails/Thumbnails';
import ThumbnailItem from '../../ThumbnailItem/ThumbnailItem';
import PreviewWrapper from '../../PreviewWrapper/PreviewWrapper';
import TableView from '../../Table/Table';
import NoSearchResults from '../../../ContainerView/EmptyState/NoSearchResults/NoSearchResults';
import NoRequestedItems from '../../../ContainerView/EmptyState/NoRequestedItems/NoRequestedItems';
import ScreenContainer from '../../ScreenContainer/ScreenContainer';
import LoadingSpinner from '../../LoadingSpinner/LoadingSpinner';
import {ANIMATION_TIMEOUT_150MS} from '../../../SETTINGS';
import {Button} from 'zd-components';

const lolex = require('lolex');


describe('ContentBody Unit Tests', () => {
    let wrapper;
    let sandbox;
    let props;
    let clock;

    const getProps = (options?) => {
        return assign({
            dataItems: DATA_ITEMS,
            layout: ELayout.CARDS,
            route: EFastFilterValue.ALL,
            onItemClick: sandbox.stub(),
            onSortChange: sandbox.stub(),
            onItemDelete: sandbox.stub(),
            emptyState: {
                noAccess: false,
                noDataSource: false,
                noCharts: false,
                noRequestedItems: false,
                noSearchResults: false
            },
            sortObject: {
                sort: ESort.NAME,
                sortOrder: ESortOrder.ASC
            }
        }, options);
    };

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        props = getProps();
        wrapper = mount(<ContentBody {...props} />);

        clock = lolex.install();
    });

    afterEach(() => {
        sandbox.restore();
        wrapper.unmount();

        clock.uninstall();
    });

    it('can instantiate', () => {
        expect(wrapper).to.exist;
    });

    describe('Thumbnail View', () => {
        it('can render thumbnails layout', () => {
            props = getProps({layout: ELayout.CARDS});
            wrapper = mount(<ContentBody {...props} />);

            expect(wrapper.find(Thumbnails)).to.have.length(1);
            expect(wrapper.find(TableView)).to.have.length(0);
        });

        it('can render with multiple dataItems in thumbnail view', () => {
            props = getProps({layout: ELayout.CARDS});
            wrapper = mount(<ContentBody {...props} />);
            let ThumbnailItems = wrapper.find(Thumbnails).find(ThumbnailItem);

            expect(ThumbnailItems.length).to.equal(DATA_ITEMS.length);
        });
    });

    describe('Table Layout', () => {
        beforeEach(() => {
            props = getProps({layout: ELayout.TABLE});
            wrapper = mount(<ContentBody {...props} />);
            wrapper.setState({ windowWidth: 1000 });
        });
        it('can render table layout', () => {
            expect(wrapper.find(Thumbnails)).to.have.length(0);
            expect(wrapper.find(PreviewWrapper)).to.have.length(1);
            expect(wrapper.find(TableView)).to.have.length(1);
        });

        it('can render with multiple dataItems in table view', () => {
            const TableItems = wrapper.find(TableView).find('tbody').find('tr');

            expect(TableItems.length).to.equal(DATA_ITEMS.length);
        });


    });

    it('can render with no search results', () => {
        props = getProps({
            emptyState: {
                noAccess: false,
                noDataSource: false,
                noCharts: false,
                noRequestedItems: false,
                noSearchResults: true
            }
        });
        wrapper = mount(<ContentBody {...props} />);

        expect(wrapper.find(NoSearchResults)).to.have.length(1);
    });

    it('can render with no requested items', () => {
        props = getProps({
            route: EFastFilterValue.MY_ITEMS,
            emptyState: {
                noAccess: false,
                noDataSource: false,
                noCharts: false,
                noRequestedItems: true,
                noSearchResults: false
            }
        });
        wrapper = mount(<ContentBody {...props} />);

        expect(wrapper.find(NoRequestedItems)).to.have.length(1);
    });

    it('can handle onItemClick', () => {
        props = getProps({layout: ELayout.TABLE});
        wrapper = mount(<ContentBody {...props} />);
        wrapper
            .setState({selectedItem: DATA_ITEMS[0], windowWidth: 1000 })
            .find('.preview-image-wrapper')
            .simulate('mouseover');

        let ButtonComponent = wrapper.find(ScreenContainer).find(Button);

        clock.tick(ANIMATION_TIMEOUT_150MS);

        ButtonComponent.simulate('click');
        expect(props.onItemClick.called).to.be.true;
    });

    it('can handle onItemDelete', () => {
        props = getProps({layout: ELayout.TABLE});
        wrapper = mount(<ContentBody {...props} />);
        wrapper.setState({selectedItem: DATA_ITEMS[1], windowWidth: 1000 });

        wrapper
            .find('.del-btn')
            .at(1)
            .simulate('click');

        wrapper
            .find('.source-delete-btn')
            .simulate('click');

        expect(props.onItemDelete.called).to.be.true;
    });

    it('updates state if modal opened', () => {
        props = getProps({layout: ELayout.TABLE});
        wrapper = mount(<ContentBody {...props} />);
        wrapper.setState({ windowWidth: 1000});

        expect(wrapper.state().showModal).to.be.false;

        wrapper
            .find('.del-btn')
            .first()
            .simulate('click');

        expect(wrapper.state().showModal).to.be.true;
    });

    it('updates state if modal closed', () => {
        props = getProps({layout: ELayout.TABLE});
        wrapper = mount(<ContentBody {...props} />);
        wrapper.setState({ windowWidth: 1000});

        expect(wrapper.state().showModal).to.be.false;

        wrapper
            .find('.del-btn')
            .first()
            .simulate('click');

        expect(wrapper.state().showModal).to.be.true;

        wrapper
            .find('.warning-leave-btn')
            .simulate('click');

        expect(wrapper.state().showModal).to.be.false;
    });

    it('can reset selectedItem on layout change', () => {
        wrapper = mount(<ContentBody {...props} />);
        wrapper.setProps({layout: ELayout.TABLE});

        expect(wrapper.state('selectedItem')).to.equal(null);
    });

    describe('loading spinner', () => {
        it('should show spinner if pending prop is true', () => {
            wrapper = shallow(<ContentBody {...props} />);
            wrapper.setProps({pending: true});

            expect(wrapper.find(LoadingSpinner).length).to.equal(1);
        });

        it('should not show spinner if pending prop is false', () => {
            wrapper = shallow(<ContentBody {...props} />);
            wrapper.setProps({pending: false});

            expect(wrapper.find(LoadingSpinner).length).to.equal(0);
        });
    });

});
