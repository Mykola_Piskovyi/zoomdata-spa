/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';
import { bindAll } from 'lodash';

import {ELayout, TDataItem, TSortByType, TEmptyState, EFastFilterValue } from '../../model/types';

import PreviewWrapper, {IPreviewWrapperProps} from '../PreviewWrapper/PreviewWrapper';
import Thumbnails, {IThumbnailsProps} from '../Thumbnails/Thumbnails';
import TableView, {ITableProps} from '../Table/Table';
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner';
import NoRequestedItems from '../../ContainerView/EmptyState/NoRequestedItems/NoRequestedItems';
import NoSearchResults from '../../ContainerView/EmptyState/NoSearchResults/NoSearchResults';
import {Dialog} from '../Dialog/Dialog';

const MINIMUM_WIDTH_TO_SHOW_WINDOW = 768;

export interface IContentBodyProps extends IThumbnailsProps {
    pending?: boolean;
    dataItems: Array<TDataItem>;
    layout: ELayout;
    route: EFastFilterValue;
    onItemClick(item: TDataItem): void;
    onSortChange(sortObject: TSortByType);
    onItemDelete(item: TDataItem): void;
    emptyState: TEmptyState;
    sortObject: TSortByType;
}

export interface IContentBodyState {
    selectedItem?: TDataItem;
    windowWidth?: number;
    showModal?: boolean;
}

class ContentBody extends React.Component<IContentBodyProps, IContentBodyState> {
    constructor(options) {
        super(options);

        this.state = {selectedItem: null, windowWidth: window.innerHeight, showModal: false};

        bindAll(this,
            '_onItemDelete',
            '_setSelectedItem',
            '_onWindowResize',
            '_handleOpen',
            '_handleClose',
            '_handleDelete');
    }

    componentWillReceiveProps(nextProps: IContentBodyProps) {
        if (nextProps.layout !== this.props.layout) {
            this.setState({selectedItem: null});
        }
    }

    componentDidMount() {
        window.addEventListener('resize', this._onWindowResize);
        this._onWindowResize();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._onWindowResize);
    }

    private _onWindowResize() {
        this.setState({ windowWidth: window.innerWidth });
    }

    private _handleOpen() {
        this.setState({showModal: true});
    }

    private _handleClose() {
        this.setState({showModal: false, selectedItem: null});
    }

    private _handleDelete() {
        this.props.onItemDelete(this.state.selectedItem);
        this.setState({showModal: false, selectedItem: null});
    }

    private _onItemDelete(item: TDataItem): void {
        this.setState({showModal: true, selectedItem: item});
    }

    private _setSelectedItem(selectedItem: TDataItem): void {
        this.setState({selectedItem});
    }

    private getBodyContents(): JSX.Element {
        const { layout, route } = this.props;
        const { selectedItem, windowWidth } = this.state;
        const { noRequestedItems, noSearchResults } = this.props.emptyState;

        const thumbnailsProps: IThumbnailsProps = {
            dataItems: this.props.dataItems,
            onItemClick: this.props.onItemClick,
            onItemDelete: this._onItemDelete,
            onItemFavorite: this.props.onItemFavorite
        };

        const tableProps: ITableProps = {
            dataItems: this.props.dataItems,
            selectedItem: selectedItem,
            onItemFavorite: this.props.onItemFavorite,
            onItemDelete: this._onItemDelete,
            onItemClick: this.props.onItemClick,
            onSortChange: this.props.onSortChange,
            setSelectedItem: this._setSelectedItem,
            sortObject: this.props.sortObject
        };

        const previewProps: IPreviewWrapperProps = {
            selectedItem: selectedItem,
            onItemDelete: this._onItemDelete,
            onItemClick: this.props.onItemClick
        };

        const showCards = layout === ELayout.CARDS ||
            windowWidth < MINIMUM_WIDTH_TO_SHOW_WINDOW;

        const dataContent = showCards ? getCardView(thumbnailsProps) :
            getTableView(tableProps, previewProps);

        if (noSearchResults) {
            return <NoSearchResults />;
        } else if (noRequestedItems) {
            return <NoRequestedItems route={route} />;
        } else {
            return dataContent;
        }
    }

    render(): JSX.Element {
        const { pending } = this.props;
        const { showModal, selectedItem} = this.state;
        const spinnerProps = {
            transparent: true,
            style: {
                'position': 'absolute'
            }
        };

        return (
            <div className='zdView-ContentBody'>
                { pending ? <LoadingSpinner {...spinnerProps} /> : null}
                { this.getBodyContents() }
                {selectedItem && showModal ?
                    (
                        <Dialog
                            showModal={showModal}
                            handleClose={this._handleClose}
                            handleDelete={this._handleDelete}
                            text={selectedItem.name}
                        />
                    ) : null
                }
            </div>
        );
    }
}

/**
 * Gets the TableView wrapper element
 * @param tableProps the table props
 * @param previewProps the preview props
 * @returns {any} JSX Element
 */
const getTableView = (tableProps: ITableProps,
                      previewProps: IPreviewWrapperProps): JSX.Element => {
    return (<div className='zdView-TableView'>
        <div className='table-component-wrapper'>
            <TableView {...tableProps} />
        </div>
        <div className='preview-component-wrapper'>
            <PreviewWrapper {...previewProps} />
        </div>
    </div>);
};

/**
 * Gets the card view
 * @param thumbnailsProps
 * @returns {any}
 */
const getCardView = (thumbnailsProps: IThumbnailsProps): JSX.Element => {
    return <Thumbnails {...thumbnailsProps} />;
};

export default ContentBody;
