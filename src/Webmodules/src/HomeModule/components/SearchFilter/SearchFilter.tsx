/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as _ from 'lodash';
import * as React from 'react';
const { Theme, SvgIcon, ReadOnlyText, Input } = require('zd-components');

import {injectSheet} from '../../../utilities/jssSetup/JssUtils';
import SearchFilterStyles from './SearchFilterStyles';

export interface ISearchFilterProps {
    searchLabel: string;
    onSearch(value: string): any;
    sheet?: any;
}

export const ActiveSearch = (props) => {
    const {onChange, onClose, className, searchLabel} = props;

    const inputProps = {
        className: 'search-input',
        autoFocus: true,
        type: 'text',
        placeholder: searchLabel,
        onChange: onChange
    };

    const closeProps = {
        name: 'close_clear',
        className: 'close-icon',
        onClick: onClose,
    };

    return (
        <div className={className + ' active'}>
            <Input {...inputProps} />
            <SvgIcon {...closeProps} />
        </div>
    );
};

export const InActiveSearch = (props) => {
    const {onClick, className, searchLabel} = props;
    return (
        <div className={className + ' inactive'} onClick={onClick}>
            <SvgIcon className='search-icon' name='search' />
            <ReadOnlyText className='search-title' text={searchLabel} />
        </div>
    );
};

export class SearchFilter extends React.Component<ISearchFilterProps, { active: boolean; }> {

    static defaultProps = {
        searchLabel: 'Search All'
    };

    constructor(options) {
        super(options);
        this.state = {active: false};

        _.bindAll(this, 'onSearch', '_onInActiveClick', '_onClose');
    }

    render() {
        const {active} = this.state;
        const searchLabel = this.props.searchLabel;

        const activeProps = {
            searchLabel: searchLabel,
            onChange: this.onSearch,
            onClose: this._onClose,
            className: 'search-item'
        };

        const inActiveProps = {
            searchLabel: searchLabel,
            onClick: this._onInActiveClick,
            className: activeProps.className
        };

        const {classes} = this.props.sheet;

        return (
            <Theme>
                <div className={classes['zdView-SearchFilter']}>
                    { active ?
                        <ActiveSearch {...activeProps} /> :
                        <InActiveSearch {...inActiveProps} />
                    }
                </div>
            </Theme>
        );
    }

    onSearch(e) {
        const {value, validity} = e.target;

        if (validity.valid) {
            this.props.onSearch(value);
        }
    }

    _onInActiveClick() {
        this.setState({active: true});
    }

    _onClose() {
        this.setState({active: false});
        this.props.onSearch('');
    }

}

export default injectSheet(SearchFilterStyles)(SearchFilter);
