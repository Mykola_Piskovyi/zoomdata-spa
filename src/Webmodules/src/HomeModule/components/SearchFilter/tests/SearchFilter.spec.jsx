/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import React  from 'react';
import {expect} from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';

import {SearchFilter, ActiveSearch, InActiveSearch} from '../SearchFilter';

describe('SearchFilter Unit test: ', () => {

    let sandbox;
    let props;
    let searchComponent;

    beforeEach(() => {
        sandbox = sinon.sandbox.create();

        props = {
            searchLabel: 'Search SomeLabel',
            onSearch: sandbox.stub(),
            sheet: {
                classes: {
                    'zdView-SearchFilter': {}
                }
            }
        };

        searchComponent = mount(<SearchFilter {...props}/>);
    });

    afterEach(() => {
        sandbox.restore();
        searchComponent.unmount();
    });

    it('should be defined: ', () => {
        expect(SearchFilter).to.exist;
        expect(searchComponent).to.exist;
    });

    describe('Rendering: ', () => {

        describe('"inactive" search tests: ', () => {

            it('should have "state.active: false" by default', () => {
                expect(searchComponent.state('active')).to.be.false;
            });

            it('should render "inactive" by default: ', () => {
                expect(searchComponent.find('.search-item')).to.have.length(1);
                expect(searchComponent.find(InActiveSearch)).to.have.length(1);
                expect(searchComponent.find('.search-item.inactive')).to.have.length(1);
            });

            it(`should render text "searchLabel" from props: `, () => {
                expect(searchComponent.find('.search-item').text())
                    .to.eql(props.searchLabel);
            });

            it('should render default "Search All" for non searchLabel prop: ', () => {
                let _props = _.omit(props, ['searchLabel']);
                searchComponent = mount(<SearchFilter {..._props}/>);

                expect(searchComponent.find('.search-title').text())
                    .to.eql('Search All');
            });

            it('should NOT render "active" by default: ', () => {
                expect(searchComponent.find('.search-item')).to.have.length(1);
                expect(searchComponent.find(ActiveSearch)).to.not.have.length(1);
            });

            it('click on search should change state to active "true": ', () => {
                searchComponent.find('.search-item').simulate('click');

                expect(searchComponent.state('active')).to.be.true;
                expect(searchComponent.find(InActiveSearch)).to.not.have.length(1);
                expect(searchComponent.find(ActiveSearch)).to.have.length(1);
            });

        });

        describe('"active" search tests: ', () => {

            beforeEach(() => {
                searchComponent.setState({active: true});
            });

            it('should NOT render "active" by default: ', () => {
                expect(searchComponent.state('active')).to.be.true;
                expect(searchComponent.find('.search-item')).to.have.length(1);
                expect(searchComponent.find(ActiveSearch)).to.have.length(1);
            });

            it(`should render placeholder "searchLabel" from props: `, () => {
                expect(
                    searchComponent.find('.search-item input').prop('placeholder')
                ).to.eql(props.searchLabel);
            });

            it('call onSearch() for change valid input: ', () => {
                const query = 'some query';
                searchComponent
                    .find('.search-item input')
                    .simulate('change', {
                        target: {value: query, validity: {valid: true}}
                    });

                expect(props.onSearch.called).to.be.true;
                expect(props.onSearch.getCall(0).args).to.eql([query]);
            });

            it('NOT call onSearch() for change invalid input: ', () => {
                const query = 'invalid query';

                searchComponent
                    .find('.search-item input')
                    .simulate('change', {
                        target: {value: query, validity: {valid: false}}
                    });

                expect(props.onSearch.called).to.be.false;
            });

            it('click on close icon should change state to active "false" and remove search query: ', () => {
                searchComponent.find('.search-item .close-icon').simulate('click');

                expect(searchComponent.state('active')).to.be.false;
                expect(searchComponent.find(InActiveSearch)).to.have.length(1);
                expect(props.onSearch.called).to.be.true;
                expect(props.onSearch.getCall(0).args).to.eql(['']);
            });

        });
    });

});
