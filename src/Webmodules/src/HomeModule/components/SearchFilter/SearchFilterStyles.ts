/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

export default {
    'zdView-SearchFilter': {

        '& .search-item': {

            '&.active': {

                '& .search-input': {
                    border: '1px solid #939393',
                    borderRadius: '3px',
                    boxShadow: 'inset 0 1px 1px rgba(0, 0, 0, 0.075)',
                    boxSizing: 'border-box',
                    color: '#323232',
                    height: '24px',
                    minHeight: '24px',
                    width: 'calc(100% - 10px)',
                    padding: '0 18px 0 5px',
                    transitionDuration: '0.2s',
                    transitionProperty: 'border, box-shadow',
                    margin: '4px 0 0 5px',

                    '&:focus': {
                        outline: 'none',
                        boxShadow: '0 0 5px #d6df23',
                        border: '1px solid #d6df23'
                    }
                },

                '& .close-icon': {
                    position: 'relative',
                    top: '-22px',
                    float: 'right',
                    color: '#d2d2d2',
                    cursor: 'pointer',
                    marginRight: '5px',
                    width: '20px'
                }
            },

            '&.inactive': {
                color: '#d2d2d2',
                padding: '3px 0px 7px 10px',

                '&:hover': {
                    color: '#b8b8b8',
                    cursor: 'pointer'
                },

                '& .search-icon': {
                    padding: '0px 4px',
                    width: '14px'
                },

                '& .search-title': {
                    display: 'inline-block',
                    verticalAlign: 'middle',
                    paddingLeft: '2px'
                }
            }

        }

    }
};
