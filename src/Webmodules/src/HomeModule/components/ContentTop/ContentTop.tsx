/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';
import { isEqual } from 'lodash';
const { SvgIcon } = require('zd-components');
const classNames = require('classnames');
import {injectSheet} from '../../../utilities/jssSetup/JssUtils';

import {TFastFilter, TSortByType, ELayout, TEmptyState} from '../../model/types';
import LayoutSwitcher from './LayoutSwitcher/LayoutSwitcher';
import SortingBy from '../SortingBy/SortingBy';

import ContentTopStyles from './ContentTopStyles';

export interface IContentTopProps {
    hideTopSwitcher: boolean;
    fastFilter: TFastFilter;
    sortObject: TSortByType;
    onLayoutChange(value: ELayout);
    onSortChange(sortObject: TSortByType);
    layout: ELayout;
    sheet?: any;
}

export class ContentTop extends React.Component<IContentTopProps, {}> {
    render() {
        const { layout, hideTopSwitcher } = this.props;
        const { icon, label } = this.props.fastFilter;
        const { classes } = this.props.sheet;

        const sortByProps = {
            sortObject: this.props.sortObject,
            onSortChange: this.props.onSortChange
        };

        return (
            <div className={ classes['zdView-ContentTop'] }>
                <div className='page-info'>
                    <div className='page-icon'>
                        <SvgIcon name={ icon } />
                    </div>
                    <span className='page-label'>{ label }</span>
                </div>
                { hideTopSwitcher ?
                    '' :
                    (<div>
                        <div className='layout-switcher-wrapper'>
                            <LayoutSwitcher {...this.props} />
                        </div>
                        <div className='sortby-wrapper'>
                            { isEqual(layout, ELayout.CARDS) ? <SortingBy {...sortByProps} /> : null }
                        </div>
                    </div>)
                }
            </div>
        );
    }
}

export default injectSheet(ContentTopStyles)(ContentTop);
