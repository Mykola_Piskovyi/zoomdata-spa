/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

export default {

    'zdView-ContentTop': {
        color: '#939393',
        padding: '0 10px',
        height: 'inherit',

        '& .page-info': {
            float: 'left',
            height: 'inherit',
            padding: 0,

            '& .page-icon': {
                height: 'inherit',
                float: 'left',
                marginRight: '10px',

                '& svg': {
                    width: '24px',
                    height: '24px',
                    verticalAlign: 'initial'
                }
            },

            '& .page-label': {
                fontSize: '22px',
                fontWeight: 400,
                color: '#595959',
                display: 'inline-block',
                height: 'inherit'
            }
        },

        '& .sortby-wrapper': {
            height: '24px',
            marginRight: '32px',
            float: 'right'
        },

        '& .layout-switcher-wrapper': {
            float: 'right'
        },

        '& svg': {
            overflow: 'visible'
        },

        '@media screen and (max-width: 767px)': {
            '& .page-info': {
                display: 'none'
            },
            '& .layout-switcher-wrapper': {
                display: 'none'
            },
            '& .sortby-wrapper': {
                marginRight: 0
            }
        },

        '& .hidden': {
            display: 'none'
        }
    }
};
