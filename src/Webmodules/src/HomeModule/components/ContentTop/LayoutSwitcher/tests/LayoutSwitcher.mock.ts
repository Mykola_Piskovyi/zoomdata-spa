/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import { FAST_FILTER } from '../../../../tests/HomeModule.mock';

const layoutSwitcherProps = {
    fastFilter: FAST_FILTER,
    layout: 'table',
    onLayoutChange: (layout) => {
    },
    sheet: {
        classes: {
            'zdView-LayoutSwitcher': {}
        }
    }
};

export { layoutSwitcherProps };
