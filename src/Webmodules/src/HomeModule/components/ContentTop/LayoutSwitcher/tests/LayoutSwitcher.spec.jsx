/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import React, { createElement } from 'react';
import { assign } from 'lodash';
import { mount, shallow } from 'enzyme';
import sinon from 'sinon';

import { LayoutSwitcher } from '../LayoutSwitcher.tsx';
import { layoutSwitcherProps } from './LayoutSwitcher.mock.ts';

import { ELayout} from '../../../../model/types';

describe('LayoutSwitcher Component unit test: ', () => {
    let wrapper;

    describe('WHEN LayoutSwitcher component is initialized: ', () => {
        beforeEach(() => {
            wrapper = shallow(<LayoutSwitcher {...layoutSwitcherProps} />);
        });

        afterEach(() => {
            wrapper.unmount();
        });

        it('should be defined', () => {
            expect(wrapper).to.exist;
        });
    });

    describe('WHEN LayoutSwitcher component is rendered: ', () => {
        beforeEach(() => {
            wrapper = mount(
                <LayoutSwitcher {...layoutSwitcherProps} />
            );
        });

        afterEach(() => {
            wrapper.unmount();
        });

        it('should render correctly', () => {
            expect(wrapper.children()).to.have.length(2);
            expect(wrapper.find('.cards-layout')).to.have.length(1);
            expect(wrapper.find('.table-layout')).to.have.length(1);
        });


        it('should select correct layout', () => {
            expect(wrapper.find(`.${layoutSwitcherProps.layout}-layout`).hasClass('selected')).to.equal(true);
        });
    });

    describe('WHEN LayoutSwitcher event is triggered: ', () => {
        it('_onLayoutChange should have been called', () => {
            let spy = sinon.spy();
            let props = assign({}, layoutSwitcherProps, {
                onLayoutChange: spy
            });

            wrapper = mount(<LayoutSwitcher {...props} />);
            wrapper.find('.cards-layout').simulate('click');

            expect(spy.called).to.equal(true);
        });

        afterEach(() => {
            wrapper.unmount();
        });
    });
});
