/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

export default {
    'zdView-LayoutSwitcher': {
        height: '24px',

        '& .layout-item': {
            width: '40px',
            height: '100%',
            float: 'left',
            color: '#e6e6e6',
            background: '#b2b2b2',
            textAlign: 'center',
            cursor: 'pointer',

            '&:hover': {
                background: '#999999'
            },

            '&.selected': {
                background: '#595959',

                '&:hover': {
                    background: '#323232'
                }
            },

            '&:first-child': {
                borderTopLeftRadius: '3px',
                borderBottomLeftRadius: '3px',
                borderRight: '1px solid #e6e6e6'
            },

            '&:last-child': {
                borderTopRightRadius: '3px',
                borderBottomRightRadius: '3px',
                borderLeft: '1px solid #e6e6e6'
            },

            '& .svg-icon': {
                'verticalAlign': 'middle !important',
                paddingBottom: '2px',

                '&[name="collections"]': {
                    width: '14px'
                },

                '&[name="attribute_list"]': {
                    width: '20px'
                }
            }
        }
    }
};
