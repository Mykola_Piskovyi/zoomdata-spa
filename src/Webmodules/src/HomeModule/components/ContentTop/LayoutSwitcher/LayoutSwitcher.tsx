/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';
import { bindAll, wrap, isEqual, assign } from 'lodash';

import {TLayoutItem, TFastFilter, ELayout} from '../../../model/types';
import {injectSheet} from '../../../../utilities/jssSetup/JssUtils';
import LayoutSwitcherStyles from './LayoutSwitcherStyles';
import { LAYOUT_TYPES } from '../../../SETTINGS';

const { SvgIcon } = require('zd-components');
const classNames = require('classnames');

export interface ILayoutSwitcherProps {
    fastFilter: TFastFilter;
    onLayoutChange(value: ELayout);
    layout: ELayout;
    sheet?: any;
}

export class LayoutSwitcher extends React.Component<ILayoutSwitcherProps, {}> {
    render() {
        const { layout, onLayoutChange } = this.props;
        const { classes } = this.props.sheet;

        let layoutItems = LAYOUT_TYPES.map((item: TLayoutItem, index) => {
            let isItemSelected = isEqual(layout, item.layout);
            let onClick = () => (onLayoutChange(item.layout));

            return (
                <div key={index} onClick={onClick} className={classNames(`${item.layout}-layout`, 'layout-item', {'selected': isItemSelected}) }>
                    <SvgIcon preserveAspectRatio='xMidYMid meet' className='svg-icon' name={ item.icon } />
                </div>
            );
        });

        return (
            <div className={classes['zdView-LayoutSwitcher']}>
                { layoutItems }
            </div>
        );
    }
}

export default injectSheet(LayoutSwitcherStyles)(LayoutSwitcher);

