/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

import { FAST_FILTERS, LAYOUT_TYPES, SORT_OBJECT } from '../../../tests/HomeModule.mock';
import React from 'react';
import { assign, head, noop } from 'lodash';
import { shallow, mount } from 'enzyme';
import ContentTop from '../ContentTop.tsx';
import SortingBy from '../../SortingBy/SortingBy.tsx';
import LayoutSwitcher from '../LayoutSwitcher/LayoutSwitcher.tsx';

const { SvgIcon } = require('zd-components');

describe('ContentTop Component unit test: ', () => {
    let wrapper;
    let props;

    const getProps = (options) => {
        return assign({
            fastFilter: head(FAST_FILTERS),
            layout: head(LAYOUT_TYPES).layout,
            sortObject: SORT_OBJECT,
            onLayoutChange: noop,
            onSortChange: noop,
            hideTopSwitcher: false
        }, options);
    };

    describe('WHEN ContentTop component is initialized', () => {
        beforeEach(() => {
            props = getProps();
            wrapper = shallow(<ContentTop {...props}/>);
        });

        afterEach(() => {
            wrapper.unmount();
        });

        it('should be defined: ', () => {
            expect(wrapper).to.exist;
        });
    });

    describe('WHEN ContentTop component is rendered', () => {
        beforeEach(() => {
            wrapper = mount(<ContentTop {...props}/>);
        });

        afterEach(()=> {
            wrapper.unmount();
        });

        it('should render no sortby component', () => {
            const props = getProps({layout: 'table'});
            wrapper = mount(<ContentTop {...props}/>);
            expect(wrapper.find(SortingBy)).to.have.length(0);
        });

        it('should render page info', () => {
            const { label } = props.fastFilter;

            expect(wrapper.find('.page-icon')).to.have.length(1);
            expect(wrapper.find('.page-label').text()).to.equal(label);
        });

        it('should render layout switcher', () => {
            expect(wrapper.find(LayoutSwitcher)).to.have.length(1);
        });

        it('should render sorting by', () => {
            expect(wrapper.find(SortingBy)).to.have.length(1);
        });

        it('should render no layout switcher and sortby', () => {
            props = getProps({hideTopSwitcher: true});
            wrapper = mount(<ContentTop {...props}/>);

            expect(wrapper.find(LayoutSwitcher)).to.have.length(0);
            expect(wrapper.find(SortingBy)).to.have.length(0);
        });
    });
});
