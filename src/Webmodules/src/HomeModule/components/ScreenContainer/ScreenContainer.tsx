/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

'use strict';

import * as React from 'react';
import { bindAll } from 'lodash';
import { injectSheet } from '../../../utilities/jssSetup/JssUtils';
import ScreenContainerStyles from './ScreenContainerStyles';
import { TDataItem } from '../../model/types';
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner';
import {IMAGE_LOAD_ERROR_TEXT} from '../../SETTINGS';

const classNames = require('classnames');
const { Button, SvgIcon } = require('zd-components');

export interface IScreenContainerProps {
    hovered: boolean;
    clickable: boolean;
    src: string;
    item: TDataItem;
    sheet?: any;
    onOpen(dataItem: TDataItem): void;
    withInfo?: boolean;
}

export class ScreenContainer extends React.Component<IScreenContainerProps, { loadingImage?: boolean; errorText?: string}>  {
    static defaultProps = {
        withInfo: true
    };

    constructor(options) {
        super(options);

        bindAll(this, '_onImageLoad', '_onImageError', '_onItemOpen');

        this.state = {loadingImage: true, errorText: ''};
    }

    private _onImageLoad() {
        this.setState({loadingImage: false, errorText: ''});
    }

    private _onImageError() {
        this.setState({loadingImage: false, errorText: IMAGE_LOAD_ERROR_TEXT});
    }

    private _onItemOpen() {
        const {item, onOpen, clickable} = this.props;

        if (clickable) {
            onOpen(item);
        }
    }

    render() {
        const { src, hovered, withInfo } = this.props;
        const { label, ownerFullName, description } = this.props.item;
        const { classes } = this.props.sheet;
        const { loadingImage, errorText } = this.state;

        const openBtn = {
            text: 'Open'
        };

        const imageProps = {
            src: src,
            onLoad: this._onImageLoad,
            onError: this._onImageError
        };
        const spinnerStyles = {
            height: '100%',
            position: 'absolute'
        };

        const containerProps = {
            onClick: this._onItemOpen
        };

        return (
            <div className={ classNames(classes['zdView-CardImage'], {hovered: hovered}) } {...containerProps}>

                <div className='item-open'>
                    <Button className='open-btn' { ...openBtn } />
                </div>

                <div className='card-info'>
                    { withInfo ?
                        <div className='item-info'>
                            <div className='source-and-author'>
                                <div className='cell-text'>
                                    <SvgIcon name='sources' />
                                    <span className='text item-label'>{ label }</span>
                                </div>
                                <div className='cell-text'>
                                    <SvgIcon name='user' />
                                    <span className='text item-owner'>{ ownerFullName }</span>
                                </div>
                            </div>
                        { description ?
                            <div className='cell-text info'>
                                <SvgIcon name='info_dark' />
                                <span className='text item-description'>{ description }</span>
                            </div> :
                        '' }
                        </div> :
                    '' }
                </div>

                { loadingImage ? <LoadingSpinner style={ spinnerStyles } /> : null }
                { errorText ?
                    <div className='error-text'>{errorText}</div> :
                    <img className='screen-img' {...imageProps}/>
                }
            </div>);
    }
}

export default injectSheet(ScreenContainerStyles)(ScreenContainer);
