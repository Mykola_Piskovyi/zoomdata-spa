/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */

export default {
    'zdView-CardImage': {
        background: '#FFFFFF',
        boxShadow: 'rgba(0, 0, 0, 0.2) 2px 2px 10px 0px',
        width: '100%',
        height: '0',
        overflow: 'hidden',
        paddingTop: '56.25%',
        position: 'relative',
        cursor: 'pointer',

        '& .card-info': {
            opacity: 0,
            position: 'absolute',
            top: 0,
            background: 'linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 0%, rgba(0, 0, 0, 0.88) 100%)',
            width: '100%',
            paddingTop: '56.25%',
            zIndex: 2,
            transition: 'all 150ms ease-in-out',

            '& .item-info': {
                position: 'absolute',
                textAlign: 'center',
                width: 'calc(100% - 20px)',
                bottom: '10px',
                padding: '0px 10px',
                fontSize: '12px',
                textShadow: '1px 1px rgba(0, 0, 0, 0.5)',
                color: '#d2d2d2',
                lineHeight: '20px',

                '& .source-and-author': {
                    lineHeight: 'inherit',
                    height: '18px',

                    '& .cell-text': {
                        display: 'inline-block',
                        maxWidth: 'calc(50% - 5px)',
                        whiteSpace: 'nowrap',
                        textOverflow: 'ellipsis',
                        overflow: 'hidden',
                        margin: '0 5px',

                        '&:first-child': {
                            marginLeft: 0
                        },

                        '&:last-child': {
                            marginRight: 0
                        }
                    }
                },

                '& .cell-text': {
                    display: 'inline-block',
                    margin: '0 10px',
                    lineHeight: '20px',

                    '& svg': {
                        width: '12px',
                        height: '12px',
                        paddingRight: '4px',
                        textShadow: '1px 1px rgba(0,0,0,0.5)',
                        verticalAlign: 'inherit',
                        position: 'relative',
                        top: '2px'
                    },

                    '&.info': {
                        height: '18px',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        display: '-webkit-box',
                        '-webkit-line-clamp': 1,
                        '-webkit-box-orient': 'vertical',

                        '& .item-description': {
                            wordBreak: 'break-all'
                        }
                    }
                }
            }
        },

        '&.hovered': {
            '& .card-info': {
                opacity: 1,
                transition: 'all 150ms ease-in-out',
                background: 'linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 0%, rgba(0, 0, 0, 0.88) 100%)'
            },

            '& .item-open': {
                opacity: 1,
                animation: 'movetop 150ms, show 150ms',

                '&:hover': {
                    '& + .card-info': {
                        animation: 'darken 150ms',
                        background: 'linear-gradient(to bottom, rgba(0, 0, 0, 0.2) 0%, rgba(0, 0, 0, 0.93) 100%)'
                    }
                }
            },

            '& img': {
                transform: 'scale(1.1, 1.1)',
                animation: 'zoomin 150ms'
            }
        },

        '& .item-open': {
            opacity: 0,
            textAlign: 'center',
            position: 'absolute',
            bottom: 'calc(50% - 14px)',
            width: '60px',
            left: 'calc(50% - 30px)',
            zIndex: 3,
            transition: 'all 150ms ease-in-out',

            '& .open-btn': {
                border: '1px solid #d2d2d2',
                borderRadius: '4px',
                background: 'rgba(0, 0, 0, 0.5)',
                color: '#ffffff',
                lineHeight: 'inherit',
                minWidth: '60px',
                width: '100%',
                height: '28px',
                transition: 'all 150ms ease-in-out',
                fontWeight: 'bold',

                '&:hover': {
                    background: '#323232',
                    animation: 'activateButton 150ms',
                    border: '1px solid #d6df23',
                    color: '#d6df23'
                }
            }
        },

        '& img': {
            width: '100%',
            position: 'absolute',
            margin: 'auto',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            transition: 'all 150ms ease-in-out'
        },

        '& .error-text': {
            color: '#939393',
            textTransform: 'uppercase',
            textAlign: 'center',
            position: 'absolute',
            bottom: 'calc(50% - 10px)',
            width: '100%'
        }
    },

    '@keyframes movetop': {
        from: {bottom: 'calc(50% - 30px)'},
        to: {bottom: 'calc(50% - 14px)'}
    },

    '@keyframes show': {
        from: {opacity: 0},
        to: {opacity: 1}
    },

    '@keyframes darken': {
        from: {background: 'linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 0%, rgba(0, 0, 0, 0.88) 100%)'},
        to: {background: 'linear-gradient(to bottom, rgba(0, 0, 0, 0.2) 0%, rgba(0, 0, 0, 0.93) 100%)'}
    },

    '@keyframes zoomin': {
        from: {transform: 'scale(1)'},
        to: {transform: 'scale(1.1)'}
    },

    '@keyframes activateButton': {
        from: {
            border: '1px solid #d2d2d2',
            color: '#ffffff'
        },
        to: {
            border: '1px solid #d6df23',
            color: '#d6df23'
        }
    }
};
