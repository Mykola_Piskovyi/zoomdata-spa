import {assign, clone} from 'lodash';
import React  from 'react';
import {expect} from 'chai';
import { shallow, mount } from 'enzyme';

import {DATA_ITEMS} from '../../../tests/HomeModule.mock.ts';
import {ScreenContainer} from '../ScreenContainer';

describe('ScreenContainer Unit Tests: ', () => {

    let props;
    let wrapper;
    let sandbox;

    const getProps = (props) => {
        return assign({
            src: DATA_ITEMS[0].thumbnailImageUrl,
            hovered: false,
            sheet: {
                classes: {
                    'zdView-CardImage': {'height': 0}
                }
            },
            onOpen: sandbox.stub(),
            item:DATA_ITEMS[0]
        }, props);
    };

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        props = getProps();
        wrapper = mount(<ScreenContainer {...props}/>);
    });

    afterEach(() => {
        sandbox.restore();
        wrapper.unmount();
    });

    it('should be defined ', () => {
        expect(ScreenContainer).to.exist;
        expect(wrapper).to.exist;
    });

    it('should not have "hovered" class', () => {
        expect(wrapper.find('.hovered')).to.have.length(0);
        expect(wrapper.hasClass('hovered')).to.be.false;
    });

    it('should have "hovered" class', () => {
        props = getProps({hovered: true});
        wrapper = shallow(<ScreenContainer {...props}/>);

        expect(wrapper.find('.hovered')).to.have.length(1);
        expect(wrapper.hasClass('hovered')).to.be.true;
    });

    it('should have "src" img', () => {
        expect(wrapper.find('.screen-img')).to.have.length(1);
        expect(wrapper.find('.screen-img').props().src).to.equal(props.src);
    });

    it('should render "label", "owner", "description"', () => {
        wrapper.setState({hovered: true});

        let info = wrapper.find('.item-info');

        expect(info.find('.item-label').text()).to.equal(props.item.label);
        expect(info.find('.item-owner').text()).to.equal(props.item.ownerFullName);
        expect(info.find('.item-description').text()).to.equal(props.item.description);
    });

    it('should not render "description" if prop not defined', () => {
        props.item.description = '';
        wrapper = mount(<ScreenContainer {...props}/>);
        let info = wrapper.find('.item-info');

        expect(info.find('.description')).to.have.length(0);
    });

    it('handle onClick() for click on open button', () => {
        wrapper
            .setProps({clickable: true})
            .simulate('click');

        expect(props.onOpen.called).to.be.true;
    });

    it('render without item info' , () => {
        props.withInfo = false;
        wrapper = mount(<ScreenContainer {...props}/>);

        expect(wrapper.find('.item-info')).to.have.length(0);
        expect(wrapper.find('.cell-text')).to.have.length(0);
    });
});
