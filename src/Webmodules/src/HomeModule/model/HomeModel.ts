/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import { assign, has, each, bindAll, get, isEqual } from 'lodash';
import {UserPreference} from '../../../../Api/src/models/UserPreferenceModel';
import { TFastFilter, TDataItem, TInventoryItem, EFastFilterValue, ELayout, TSortByType, ESort, ESortOrder, TUserPreferenceValue } from './types';

export type TModelProps = {
    fastFilter?: TFastFilter;
    fastFilters?: Array<TFastFilter>;
    inventory?: Array<TInventoryItem>;
    dataItems?: Array<TDataItem>;
    route?: EFastFilterValue;
    sort?: TSortByType,
    pending?: boolean;
    searchQuery?: string;
    layout?: ELayout;
    isDisabledUser?: boolean;
};

const DEFAULTS = {
    inventory: null,
    dataItems: [],
    route: EFastFilterValue.ALL,
    sort: {
        sort: ESort.NAME,
        sortOrder: ESortOrder.ASC
    },
    searchQuery: '',
    pending: true,
    layout: ELayout.CARDS,
    isDisabledUser: false
};

// TODO: extend backbone model for listen changes by controller and re-render template
export default class HomeModel {

    private _props: TModelProps;
    private _restoredUserPrefs: TUserPreferenceValue;

    constructor(args?: TModelProps) {
        bindAll(this, 'restoreUserPreferences', 'getProps');

        this._props = <TModelProps>assign({}, DEFAULTS, args);
    }

    set(key: string, value): TModelProps {
        this._props[key] = value;
        return this.getProps();
    }

    get(key: string) {
        return this._props[key];
    }

    public getProps(): TModelProps {
        return this._props;
    }

    restoreUserPreferences(userPreference: UserPreference): void {
        let instance = this;
        let sortObject = this.get('sort');
        let sortRegExp = new RegExp('sort');
        this._restoredUserPrefs = <TUserPreferenceValue>get(userPreference, 'value.value');

        each(this._restoredUserPrefs, (value, key) => {
            if (sortRegExp.test(key)) {
                sortObject[key] = value;
            } else {
                instance.set(key, value);
            }
        });

        this.set('sort', sortObject);
    }

    getUpdatedUserPreferences(): TUserPreferenceValue|null {
        const { layout, sort } = this.getProps();
        const preferences = assign({}, sort, {layout});

        return isEqual(preferences, this._restoredUserPrefs) ?
            null :
            preferences;
    }
}
