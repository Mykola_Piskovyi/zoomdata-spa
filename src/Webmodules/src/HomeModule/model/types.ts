/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

enum EAddNewValue {
    NEW_CHARTS = <any>'NEW_CHARTS',
    NEW_SOURCE = <any>'NEW_SOURCE',
    NEW_IMPORT_DASHBOARD = <any>'NEW_IMPORT_DASHBOARD'
}

enum EFastFilterValue {
    ALL = <any>'all',
    FAVORITES = <any>'favorites',
    MY_ITEMS = <any>'myitems',
    SHARED = <any>'shared'
}

enum ELayout {
    CARDS = <any>'cards',
    TABLE = <any>'table'
}

enum ESort {
    FAVORITES = <any>'FAVORITES',
    NAME = <any>'NAME',
    DESCRIPTION = <any>'DESCRIPTION',
    OWNER = <any>'OWNER',
    MODIFIED = <any>'MODIFIED_DATE',
    ASSOCIATED_ITEMS = <any>'ASSOCIATED_ITEMS'
}

enum ESortOrder {
    ASC = <any>'ASC',
    DESC = <any>'DESC',
}

enum ESortTable {
    FAVORITES = <any>'favoriteIcon',
    NAME = <any>'nameElement',
    OWNER = <any>'ownerElement',
    MODIFIED = <any>'dateElement',
    ASSOCIATED_ITEMS = <any>'dataSourceElement'
}

type TFastFilter = {
    label: string;
    icon: string;
    value: EFastFilterValue;
    searchLabel: string;
};

type TDataAssociatedItem = {
    name: string;
    favorite: boolean;
    inventoryItemId: string;
    links: Array<any>;
    type: string;
    typeName: string;
};

type TInventoryItem = {
    inventoryItemId: string;
    typeName: string;
    type: string;
    name: string;
    description: string;
    icon?: string;
    favorite: boolean;
    modifiedDate: string;
    ownerFullName: string;
    ownerUserId: string;
    associatedItems: Array<TDataAssociatedItem>;
    links: Array<any>;
};

type TDataItem = TInventoryItem & {
    readableDate?: string;
    deletable: boolean;
    thumbnailSubTitle?: string;
    label?: string;
    thumbnailImageUrl?: string;
};

type TTableColumn = {
    name: string;
    key: ESortTable | string;
    sorting: {
        sortable?: boolean;
        activeColor?: string;
        inactiveColor?: string;
        showSortToggle?: boolean;
    };
    width: number;
    minWidth?: number;
    maxWidth?: number;
};

// Added ESortTable keys
type TTableDataItem = TDataItem & {
    favoriteIcon: JSX.Element;
    nameElement: JSX.Element;
    dataSourceElement: JSX.Element;
    ownerElement: JSX.Element;
    dateElement: JSX.Element;
};

type TAddNewDialogItem = {
    title: string;
    icon: string;
    value: EAddNewValue;
};

type TLayoutItem = {
    layout: ELayout;
    icon: string;
};

type TSortByType = {
    sort: ESort;
    sortOrder: ESortOrder;
};

type TTimePatterns = {
    LAST_MODIFIED: string;
};

type TSortItem = {
    title: string;
    value: ESort;
};

type TEmptyState = {
    noAccess: boolean;
    noDataSource: boolean;
    noCharts: boolean;
    noRequestedItems: boolean;
    noSearchResults: boolean;
};

type TEmptyPageType = {
    route: EFastFilterValue;
    message: string;
    icon: string;
};

type TUserPreferenceValue = {
    layout: ELayout,
    sort: ESort,
    sortOrder: ESortOrder
};

type TUserPreference = {
    accountId: string;
    key: string;
    preferenceId?: string;
    userId: string;
    value: string;
};

export {
    EAddNewValue,
    EFastFilterValue,
    ELayout,
    ESort,
    ESortOrder,
    ESortTable,
    TFastFilter,
    TDataAssociatedItem,
    TTableColumn,
    TInventoryItem,
    TDataItem,
    TTableDataItem,
    TAddNewDialogItem,
    TLayoutItem,
    TSortByType,
    TTimePatterns,
    TSortItem,
    TUserPreferenceValue,
    TUserPreference,
    TEmptyState,
    TEmptyPageType
};
