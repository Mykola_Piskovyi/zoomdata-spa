/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import {invokeMap} from 'lodash';
import * as Rx from 'rx';

import {ModuleRouter} from '../../../Core';
import MainController, {SControllerOptions} from './controller/MainController';
import {TDataItem, EFastFilterValue} from './model/types';

export interface SRouterOptions extends SControllerOptions {
    routes: any;
}

class HomeRouter extends ModuleRouter {

    controller: MainController;
    _subscriptions: any;

    // TODO: Remove after fix spa-core ModuleRouter (remove if (!this.routes) manuallyRoute)
    routes = {
        '': '_onHome',
        'all': '_onHome',
        'favorites': '_onFavorites',
        'myitems': '_onMyItems',
        'shared': '_onShared'
    };

    constructor(options?: SRouterOptions) {
        super(options);

        this.route('', '_onHome');
        this.route('all', '_onHome');
        this.route('favorites', '_onFavorites');
        this.route('myitems', '_onMyItems');
        this.route('shared', '_onShared');

        this.controller = new MainController({
            el: options.el,
            route: options.route,
            dependency: options.dependency,
            getThumbnailImageUrl: options.getThumbnailImageUrl,
            sources: options.sources,
            userPermissions: options.userPermissions
        });

        this._subscriptions = this._applyEvents(this.controller);
    }

    private _applyEvents(controller) {
        let events = {};

        [
            controller.EVENTS.CHANGE_ROUTE,
            controller.EVENTS.GOTO_ADMIN,
            controller.EVENTS.IMPORT_DASHBOARD,
            controller.EVENTS.SORT_CHANGE,
            controller.EVENTS.CHART_OPEN,
            controller.EVENTS.VIS_OPEN,
            controller.EVENTS.CHART_DELETE,
            controller.EVENTS.CHART_FAVORITE
        ]
            .forEach(event => {
                events[event] = Rx.Observable
                    .fromEvent(controller, event)
                    .subscribe((data) => (this.trigger(event, data)));
            });

        return events;
    }

    private _onHome(): void {
        this.controller
            .setRoute(EFastFilterValue.ALL)
            .render();
    }

    private _onFavorites(): void {
        this.controller
            .setRoute(EFastFilterValue.FAVORITES)
            .render();
    }

    private _onMyItems(): void {
        this.controller
            .setRoute(EFastFilterValue.MY_ITEMS)
            .render();
    }

    private _onShared(): void {
        this.controller
            .setRoute(EFastFilterValue.SHARED)
            .render();
    }

    stop(): void {
        invokeMap(this._subscriptions, 'dispose');
        super.stopListening();
        this.controller.remove();
    }

}

export default HomeRouter;
