/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import * as _ from 'lodash';
import {Module} from '../../../Core';
import {Permissions} from '../../../Api/src/models/UserModel';

import HomeRouter, {SRouterOptions} from './HomeRouter';
import InternalUserPermissions, {InternalUserPermissionsData} from './controller/InternalUserPermissions';

export interface SHomeOptions extends SRouterOptions, InternalUserPermissionsData {
    root: string;
}

class HomeModule extends Module {

    _root: string;
    el: HTMLElement;
    containerEl: HTMLElement;
    router: HomeRouter;
    sources: Array<any>;

    private _userPermissions: InternalUserPermissions;

    constructor(options: SHomeOptions) {
        super(options);

        if (_.isUndefined(options.el)) {
            throw new Error('missing "el" in options');
        }
        if (_.isUndefined(options.root)) {
            throw new Error('missing "root" in options');
        }
        this.sources = options.sources;
        this._root = options.root;
        this.el = options.el;
        this.containerEl = document.createElement('div');

        this._userPermissions = new InternalUserPermissions({
            isDisabledUser: options.isDisabledUser,
            isAdmin: options.isAdmin,
            permissions: options.permissions
        });

        const routerOptions = _.assign({}, options, {
            el: this.containerEl,
            userPermissions: this._userPermissions
        });
        this.router = new HomeRouter(routerOptions);
    }

    saveUserPreferences(): void {
        this.router.controller.updateUserPreferences();
    }

    beforeStart() {
        // stopped after call remove. But as for start use common global Backbone.history.start - there is an error.
        // Here till be update in spa-core stop Bachbone.history with stop router.

        this.el.innerHTML = '';
        this.el.appendChild(this.containerEl);

        try {
            this.router.start({
                root: this._root
            });
        } catch (e) {

        }
    }

    beforeStop() {
        this.router.stop();
    }

    remove() {
        super.stop();
    }

    routeTo(path: string) {
        this.router.manuallyRoute(path);
    }
}

export default HomeModule;
