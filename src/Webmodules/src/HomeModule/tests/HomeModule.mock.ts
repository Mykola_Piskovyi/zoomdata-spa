/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
import * as Bottle from 'bottlejs';
import * as Rx from 'rx';

function InventoryDAO() {
    this._subject = new Rx.Subject();
    this._observable = this._subject.asObservable();
    this.updateQuery = (data) => (this);
    this.getInventoryObservable = () => (this._observable);
    // this.refresh = () => {};
    this.fetchInventory = () => {};
    this.unfavoriteItem = (type, id) => {};
    this.favoriteItem = (type, id) => {};
}

function AccountUserDAO() {
    this._subject = new Rx.Subject();
    this._observable = this._subject.asObservable();
    this.fetchUserById = (data) => {};
    this.getUserObservable = () => (this._observable);
}

function UserPreferencesDAO() {
    this._subject = new Rx.Subject();
    this._observable = this._subject.asObservable();
    this.fetchUserPreferences = () => {};
    this.getUserPreferencesObservable = () => (this._observable);
    this.updateUserPreferences = () => {};
}

const USER = {
    account: '',
    accountId: '11111',
    accounts: [],
    activeRoles: ['admin'],
    groupRoles: ['ROLE_RAW_DATA_ACCESS'],
    id: '123123',
    name: 'admin',
    roles: ['admin']
};

const dependency = new Bottle();
dependency
    .value('user', USER)
    .factory('InventoryDAO', () => (new InventoryDAO()))
    .factory('AccountUserDAO', () => (new AccountUserDAO()))
    .factory('UserPreferencesDAO', () => (new UserPreferencesDAO()));

const USER_PERMISSIONS = {
    'connections': {'manage': true},
    'sources': {'read': true, 'write': true, 'remove': true, 'createNew': true, 'manageSome': true},
    'dashboards': {'save': true, 'share': true},
    'filters': {'save': true},
    'formulas': {'edit': true, 'read': true},
    'studios': {'edit': true}
};

const USER_PREFERENCES = [{
    'preferenceId': 'preferenceId',
    'userId': USER.id,
    'accountId': USER.accountId,
    'key': 'HOME_USER_PREFERENCES',
    'value': {
        'version': '1',
        'value': {'sort': 'NAME', 'sortOrder': 'ASC', 'layout': 'cards'}
    },
    'links': []
}];

const ASSOCIATED_ITEM = {
    inventoryItemId: '581b8f73c026c7a2b34af86a',
    type: 'DATA_SOURCE',
    typeName: 'Data Source',
    name: 'Data Source name',
    links: [],
    favorite: false
};

const INVENTORY_ITEM = {
    description: '',
    favorite: false,
    inventoryItemId: '573ae633e4b01bc846a06a70',
    modifiedDate: '2016-05-18 15:04:32.696',
    name: '4_pivot_dashboard',
    ownerFullName: 'admin',
    ownerUserId: '572b6508e4b0ffb4a4d256ec',
    type: 'DASHBOARD',
    links: [],
    typeName: 'Dashboard',
    associatedItems: [{
        favorite: false,
        inventoryItemId: '5732e149e4b03368ca7b7733',
        links: [],
        name: 'My IMPALA Source',
        type: 'DATA_SOURCE',
        typeName: 'Data Source'
    }]
};

const DASHBOARD_ITEM = {
    inventoryItemId: '581b92f9c026c7a2b34af8c4',
    type: 'UBER_BARS',
    deletable: true,
    associatedItems: [{
        inventoryItemId: '581b8f73c026c7a2b34af86a',
        type: 'DATA_SOURCE',
        typeName: 'Data Source',
        name: 'Data Source name',
        links: [],
        favorite: false
    }],
    typeName: 'Bars',
    label: 'Data Source name',
    name: 'Dashboard name',
    description: 'Dashboard description',
    favorite: false,
    readableDate: 'Nov 03, 2016, 07:41 PM',
    modifiedDate: '2016-11-03 19:41:45.248',
    ownerFullName: 'admin',
    ownerUserId: '581b8135c0262260be98046a',
    thumbnailSubTitle: '',
    thumbnailImageUrl: '/zoomdata/service/screenshot/5821f3ac3004d323416f23af/ts/2016-11-09%2015:13:43.166',
    links: []
};

const DATA_ITEMS = [DASHBOARD_ITEM, {
    inventoryItemId: '582dba1e3004152cd8564c34',
    type: 'DASHBOARD',
    deletable: false,
    associatedItems: [{
        inventoryItemId: '582db9473004152cd8564bdd',
        links: [],
        type: 'DATA_SOURCE',
        typeName: 'Data Source',
        name: 'My FLAT FILE Source',
        favorite: false
    }],
    typeName: 'Dashboard',
    label: 'My FLAT FILE Source',
    name: 'Devs positions',
    description: 'Dashboard description',
    favorite: false,
    readableDate: 'Nov 03, 2016, 07:41 PM',
    modifiedDate: '2016-11-03 19:41:45.248',
    ownerFullName: 'admin',
    ownerUserId: '581b8135c0262260be98046a',
    thumbnailSubTitle: '',
    thumbnailImageUrl: '/zoomdata/service/screenshot/5821f3ac3004d323416f23af/ts/2016-11-09%2015:13:43.166',
    links: []
}, {
    inventoryItemId: '582dba583004152cd8564c3d',
    type: 'DASHBOARD',
    deletable: true,
    associatedItems: [{
        inventoryItemId: '582db9473004152cd8564bdd',
        type: 'DATA_SOURCE',
        links: [],
        typeName: 'Data Source',
        name: 'My FLAT FILE Source',
        favorite: false
    }],
    typeName: 'Dashboard',
    label: 'My FLAT FILE Source',
    name: 'Dashboard name2',
    description: 'Dashboard description',
    favorite: false,
    readableDate: 'Nov 03, 2016, 07:41 PM',
    modifiedDate: '2016-11-03 19:41:45.248',
    ownerFullName: 'admin',
    ownerUserId: '581b8135c0262260be98046a',
        thumbnailSubTitle: '',
    thumbnailImageUrl: '/zoomdata/service/screenshot/5821f3ac3004d323416f23af/ts/2016-11-09%2015:13:43.166',
    links: []
}];

const FAST_FILTERS = [{
    label: 'View All',
    icon: 'home',
    value: 'all'
}, {
    label: 'Favorites',
    icon: 'favorite_dark',
    value: 'favorites'
}, {
    label: 'My Items',
    icon: 'user',
    value: 'myitems'
}, {
    label: 'Shared With Me',
    icon: 'group',
    value: 'shared'
}];

const LAYOUT_TYPES = [{
    layout: 'cards',
    icon: 'collections'
}, {
    layout: 'table',
    icon: 'attribute_list'
}];

const SORT_OBJECT = {
    sort: 'modified',
    sortOrder: 'ASC'
};

const FAST_FILTER = {
    icon: 'home',
    label: 'View All',
    value: 'all'
};

export {
    dependency,
    USER,
    USER_PERMISSIONS,
    USER_PREFERENCES,
    FAST_FILTERS,
    LAYOUT_TYPES,
    SORT_OBJECT,
    DATA_ITEMS,
    DASHBOARD_ITEM,
    INVENTORY_ITEM,
    FAST_FILTER,
    ASSOCIATED_ITEM
};
