import {expect} from 'chai';
import sinon from 'sinon';

import HomeModule from '../HomeModule';
import HomeRouter from '../HomeRouter';
import {dependency} from './HomeModule.mock';

describe('HomeModule Unit test: ', () => {

    let home;
    let root;
    let el;
    let sandbox;
    let getThumbnailImageUrl;
    
    beforeEach(() => {
        root = 'root-path/';
        el = document.createElement('div');
        sandbox = sinon.sandbox.create();
        getThumbnailImageUrl = (item) => ('');

        home = new HomeModule({root, el, dependency, getThumbnailImageUrl});
    });

    afterEach(() => {
        sandbox.restore();
        home.remove();
    });

    it('should be defined: ', () => {
        expect(HomeModule).to.exist;
    });

    describe('initialize: ', () => {

        it('error if no "el": ', () => {
            expect(() => {
                home = new HomeModule({root: '', routes: {}});
            }).to.throw('missing "el" in options');
        });

        it('error if no "root": ', () => {
            expect(() => {
                home = new HomeModule({el: document.createElement('div'), routes: {}});
            }).to.throw('missing "root" in options');
        });

        it('no error if "root" empty string: ', () => {
            expect(() => {
                home = new HomeModule({el: document.createElement('div'), root: '', routes: {}});
            }).not.to.throw('missing "root" in options');
        });
    });

    describe('should have props and public methods: ', () =>  {
        it('_root: ', () => {
            expect(home._root).to.equal(root);
        });

        it('el: ', () => {
            expect(home.el).to.equal(el);
        });

        it('containerEl: ', () => {
            expect(home.containerEl).to.exist;
        });

        it('router: ', () => {
            expect(home.router instanceof HomeRouter).to.equal(true);
        });

        it('routeTo(): ', () => {
            expect(home.routeTo).to.exist;
        });

        it('remove(): ', () => {
            expect(home.remove).to.exist;
        });
    });

    describe('test methods: ', () => {

        it('should be stopped for remove() call: ', () => {
            let removeStub = sandbox.stub(home, 'remove');
            home.remove();

            expect(removeStub.called).to.be.true;
            expect(home.isStarted()).to.be.false;
        });

    });

});
