/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import {expect} from 'chai';
import sinon from 'sinon';

import HomeModule from '../HomeModule';
import {dependency} from './HomeModule.mock';

describe('HomeModule Integration test: ', () => {

    let root;
    let home;
    let sandbox;
    let router_start;
    let router_stop;
    let router_manuallyRoute;

    beforeEach(() => {
        root = 'root-path/';

        home = new HomeModule({
            root: root,
            el: document.createElement('div'),
            dependency: dependency,
            getThumbnailImageUrl: (item) => ('')
        });

        sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
        sandbox.restore();
        home.remove();
    });

    describe('test methods and router integration: ', () => {

        it('start and router.start with "root" arg: ', () => {
            router_start = sandbox.stub(home.router, 'start');
            home.start();

            expect(router_start.called).to.be.true;
            expect(router_start.getCall(0).args).to.eql([{root}])
        });

        it('stop and router.stop: ', () => {
            router_stop = sandbox.stub(home.router, 'stop');
            home.stop();

            expect(router_stop.called).to.be.true;
        });

        it('routeTo and router.manuallyRoute with "path" arg: ', () => {
            router_manuallyRoute = sandbox.stub(home.router, 'manuallyRoute');
            home.routeTo('somePath');

            expect(router_manuallyRoute.called).to.be.true;
            expect(router_manuallyRoute.getCall(0).args).to.eql(['somePath']);
        });

    });

});