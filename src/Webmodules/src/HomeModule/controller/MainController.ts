/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import {find, invokeMap, isEmpty, isUndefined, partialRight, assign, isNull, chain, keysIn, identity, isEqual, valuesIn} from 'lodash';
import * as Bottle from 'bottlejs';
import {TInventoryQuery} from '../../../../Api/src/daos/InventoryDAO';
import {User} from '../../../../Api/src/models/UserModel';
import {UserPreference} from '../../../../Api/src/models/UserPreferenceModel';
import {AccountUserDAO, UserModel, UserPreferencesDAO} from '../../../../Api';
import {Controller} from '../../../../Core';
import * as Rx from 'rx';

import HomeModel from '../model/HomeModel';
import {
    TDataItem, TSortByType, EFastFilterValue, TInventoryItem, EAddNewValue,
    ELayout
} from '../model/types';
import ContainerView, {SRenderViewOptions} from '../ContainerView/index';
import {FAST_FILTERS, HOME_USER_PREFERENCES} from '../SETTINGS';

import {
    getDataItems, getQuery, getBookmarkFile,
    getAddNewItems, getFastFilters, getEmptyState
} from './controllerHelpers';

import InternalUserPermissions from './InternalUserPermissions';

export interface SControllerOptions {
    el: HTMLElement;
    route?: EFastFilterValue;
    dependency?: Bottle;
    sources: Array<any>;
    getThumbnailImageUrl?(bookmark: any): string;
    userPermissions: InternalUserPermissions;
}

const EVENTS = {
    GOTO_ADMIN: 'goToAdmin',
    IMPORT_DASHBOARD: 'onImportDashboard',
    CHANGE_ROUTE: ContainerView.EVENTS.CHANGE_ROUTE,
    CHART_OPEN: ContainerView.EVENTS.CHART_OPEN,
    VIS_OPEN: ContainerView.EVENTS.VIS_OPEN,
    CHART_DELETE: ContainerView.EVENTS.CHART_DELETE,
    CHART_FAVORITE: ContainerView.EVENTS.CHART_FAVORITE
};

class MainController extends Controller {

    static EVENTS = EVENTS;
    public EVENTS = EVENTS;

    options: SControllerOptions;
    view: ContainerView;
    homeModel: HomeModel;
    userModel: UserModel;
    dependency: Bottle;
    sources: Array<any>;
    private _subscriptions: any;
    private _userPermissions: InternalUserPermissions;

    constructor(options: SControllerOptions) {
        super(options);

        if (!options.el) {
            throw new Error('missing "el" in options');
        }

        if (!options.dependency) {
            throw new Error('missing "dependency" in options');
        }

        this.el = options.el;
        this.sources = options.sources;
        this.dependency = options.dependency;
        this._userPermissions = options.userPermissions;

        this.userModel = new UserModel();
        this.homeModel = new HomeModel();
        this.view = new ContainerView({el: options.el});

        if (options.route) {
            this.setRoute(options.route);
        }

        this.userModel.setPermissions(
            this._userPermissions.getPermissions()
        );

        this.homeModel.set(
            'isDisabledUser', this._userPermissions.isDisabledUser()
        );

        this._observePermissions(this._userPermissions);

        if (!this.homeModel.get('isDisabledUser')) {
            this._observeDependencyDAO(this.dependency);
            this._observeEvents(this.view);
        }
    }

    render(): this {
        this.view.render(this._getRenderProps());
        return this;
    }

    setRoute(route: EFastFilterValue): this {
        this.homeModel.set('route', route);
        return this;
    }

    private _getRenderProps(): SRenderViewOptions {
        const {route, layout, pending, sort, searchQuery} = this.homeModel.getProps();
        const fastFilter = find(FAST_FILTERS, {value: route});
        const fastFilters = getFastFilters(this.userModel);
        const addNewItems = getAddNewItems(this.userModel, this.sources);

        const dataItems = getDataItems(
            this.homeModel,
            this.userModel,
            this.options.getThumbnailImageUrl,
            this._userPermissions.isAdmin()
        );

        const emptyState = getEmptyState(dataItems, addNewItems, this.homeModel);
        const {noRequestedItems, noSearchResults} = emptyState;
        const noItems = isEmpty(dataItems) && pending;
        const hideTopSwitcher = noRequestedItems || noSearchResults || noItems;

        return <SRenderViewOptions>assign({}, {
            searchLabel: fastFilter.searchLabel,
            initial: isNull(this.homeModel.get('inventory')),
            hideTopSwitcher,
            fastFilters,
            fastFilter,
            addNewItems,
            dataItems,
            sortObject: sort,
            route,
            pending,
            layout,
            emptyState,
            sources: this.sources
        });
    }

    private _fetchInventory() {
        const container: any = this.dependency.container;
        const {InventoryDAO} = container;

        InventoryDAO
            .updateQuery(this._getQuery())
            .fetchInventory();
    }

    private _storeSubscription(subs: Object): void {
        this._subscriptions = this._subscriptions || {};

        valuesIn(subs)
            .forEach((item: Rx.Disposable) => {
                if (!item.dispose) {
                    throw new Error('sub should be observable');
                }
            });

        chain(this._subscriptions)
            .pick(keysIn(subs))
            .invokeMap('dispose')
            .value();

        assign(this._subscriptions, subs);
    }

    private _observeDependencyDAO(dependency) {
        const container: any = dependency.container;
        const {InventoryDAO, UserPreferencesDAO, AccountUserDAO, user} = container;

        let sub = {};

        sub['AccountUserDAO'] = AccountUserDAO
            .getUserObservable()
            .select((items: Array<User>) => (find(items, {userId: user.id})))
            .distinctUntilChanged(identity, isEqual)
            .subscribe(
                (data: User) => (this.userModel.set(data)),
                (error) => {
                    // console.warn('AccountUserDAO', error);
                }
            );

        sub['UserPreferencesDAO'] = UserPreferencesDAO
            .getUserPreferencesObservable()
            .select((prefs: Array<UserPreference>) => (find(prefs, {key: HOME_USER_PREFERENCES.KEY})))
            .distinctUntilChanged(identity, isEqual)
            .subscribe((homePref: UserPreference) => {
                this.homeModel.restoreUserPreferences(homePref);
                this._fetchInventory();

            }, (error) => {
                this._fetchInventory();
            });

        sub['InventoryDAO'] = InventoryDAO
            .updateQuery(this._getQuery())
            .getInventoryObservable()
            .filter((data) => (data.type !== 'ERROR'))
            .subscribe((data: Array<TInventoryItem>) => {
                this.homeModel.set('inventory', data);

                const dataItems = getDataItems(
                    this.homeModel,
                    this.userModel,
                    this.options.getThumbnailImageUrl,
                    this._userPermissions.isAdmin()
                );
                this.homeModel.set('dataItems', dataItems);
                this.homeModel.set('pending', false);
                this.render();
            });

        sub['InventoryDAOError'] = InventoryDAO
            .getInventoryObservable()
            .filter((data) => (data.type === 'ERROR'))
            .subscribe((error) => {
                this.homeModel.set('pending', false);
                this.render();
            });

        AccountUserDAO.fetchUserById(user.id);
        UserPreferencesDAO.fetchUserPreferences();

        this._storeSubscription(sub);
    }

    private _observeEvents(view) {
        let sub = {};
        const container: any = this.dependency.container;
        const {InventoryDAO} = container;

        const {EVENTS} = view;

        sub[EVENTS.CHANGE_ROUTE] = Rx.Observable
            .fromEvent(view, EVENTS.CHANGE_ROUTE)
            .subscribe((data: EFastFilterValue) => {
                this.homeModel.set('route', data);
                this.homeModel.set('pending', true);
                this.trigger(this.EVENTS.CHANGE_ROUTE, data);
                this._fetchInventory();
            });

        sub[EVENTS.SORT_CHANGE] = Rx.Observable
            .fromEvent(view, EVENTS.SORT_CHANGE)
            .distinctUntilChanged(identity, isEqual)
            .subscribe((data: TSortByType) => {
                this.homeModel.set('sort', data);
                this.homeModel.set('pending', true);
                this.render();
                this._fetchInventory();
            });

        sub[EVENTS.SEARCH] = Rx.Observable
            .fromEvent(view, EVENTS.SEARCH)
            .subscribe((data: string) => {
                this.homeModel.set('searchQuery', data);
                this.render();
            });

        sub[EVENTS.LAYOUT_CHANGE] = Rx.Observable
            .fromEvent(view, EVENTS.LAYOUT_CHANGE)
            .subscribe((data: ELayout) => {
                this.homeModel.set('layout', data);
                this.render();
            });

        sub[EVENTS.CHART_OPEN] = Rx.Observable
            .fromEvent(view, 'onChartOpen')
            .subscribe((data: TDataItem) => {
                this.trigger(this.EVENTS.CHART_OPEN, data);
            });

        sub[EVENTS.VIS_OPEN] = Rx.Observable
            .fromEvent(view, EVENTS.VIS_OPEN)
            .subscribe((data: any) => {
                this.trigger(this.EVENTS.VIS_OPEN, data);
            });

        sub[EVENTS.CHART_DELETE] = Rx.Observable
            .fromEvent(view, 'onChartDelete')
            .subscribe((data: TDataItem) => {
                this.trigger(this.EVENTS.CHART_DELETE, data);
            });

        sub[EVENTS.CHART_FAVORITE] = Rx.Observable
            .fromEvent(view, EVENTS.CHART_FAVORITE)
            .subscribe((data: TDataItem) => {
                const {inventoryItemId, type, favorite} = data;

                this.homeModel.set('pending', true);
                this.render();

                favorite ?
                    InventoryDAO.unfavoriteItem(inventoryItemId, type) :
                    InventoryDAO.favoriteItem(inventoryItemId, type);

                this.trigger(this.EVENTS.CHART_FAVORITE, data);
            });

        sub[EVENTS.NEW_DIALOG_EVENT] = Rx.Observable
            .fromEvent(view, EVENTS.NEW_DIALOG_EVENT)
            .subscribe((value: EAddNewValue) => {
                switch (value) {
                    case EAddNewValue.NEW_SOURCE:
                        this.trigger(this.EVENTS.GOTO_ADMIN);
                        break;
                    case EAddNewValue.NEW_CHARTS:
                        console.log('SHOW POPUP BY DIALOG COMPONENT');
                        break;
                    case EAddNewValue.NEW_IMPORT_DASHBOARD:
                        this._getBookmarkFile();
                        break;
                }
            });

            sub['onBeforeUrlUnload'] = Rx.Observable
                .fromEvent(window, 'beforeunload')
                .subscribe(() => (this.updateUserPreferences()));

        this._storeSubscription(sub);
    }

    private _observePermissions(permissions: InternalUserPermissions) {
        let sub = {};

        sub['internalUserPermissions'] = Rx.Observable
            .fromEvent(permissions, 'change')
            .subscribe(() => {
                this.userModel.setPermissions(this._userPermissions.getPermissions());
                this.render();
            });

        this._storeSubscription(sub);
    }

    updateUserPreferences(): void {
        const container: any = this.dependency.container;
        const preferences = this.homeModel.getUpdatedUserPreferences();

        if (preferences) {
            const { UserPreferencesDAO } = container;
            const {KEY, VERSION} = HOME_USER_PREFERENCES;
            UserPreferencesDAO.updateUserPreferences(preferences, KEY, VERSION);
        }
    }

    remove(): any {
        invokeMap(this._subscriptions, 'dispose');
        this.updateUserPreferences();
        this.view.remove();

        return super.remove();
    }

    private _getBookmarkFile() {
        getBookmarkFile(
            (fileData: File) => { this.trigger(this.EVENTS.IMPORT_DASHBOARD, fileData); },
            (error) => { throw new Error(error); }
        );
    }

    private _getQuery(): TInventoryQuery {
        const container: any = this.dependency.container;
        const {user} = container;
        const {route, sort} = this.homeModel.getProps();

        return getQuery({user, sort, route});
    }

}

export default MainController;
