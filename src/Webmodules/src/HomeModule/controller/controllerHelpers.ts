/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import * as moment from 'moment';

import {UserModel} from '../../../../Api';
import {TInventoryQuery, EIncludedItems} from '../../../../Api/src/daos/InventoryDAO';

import { assign, partial, head, isEmpty, isNull, isEqual, chain } from 'lodash';
import HomeModel from '../model/HomeModel';
import {
    TInventoryItem, TDataItem, TFastFilter, TEmptyState, ESort,
    EFastFilterValue, TAddNewDialogItem, EAddNewValue, TSortByType
} from '../model/types';

import FileUploader from '../components/FileUploader/FileUploader';
import { TIME_PATTERNS, ADD_NEW_DIALOG_ITEMS, FAST_FILTERS } from '../SETTINGS';

export {
    getBookmarkFile,
    getDataItems,
    getQuery,
    getEmptyState,
    getFastFilters,
    getAddNewItems,
    getThumbnailSubTitle,
    getAssociatedLabel,
    convertToDataItem
};

function getAssociatedLabel(bookmark: TInventoryItem): string {
    const { associatedItems } = bookmark;

    let label = associatedItems[0] ? associatedItems[0].name : '';

    if (associatedItems.length > 1) {
        label += `...(${associatedItems.length})`;
    }
    return label;
}

function getThumbnailSubTitle(item: TInventoryItem, sortObject: TSortByType): string {
    const { sort } = sortObject;
    const { modifiedDate, ownerFullName } = item;
    const timeFromNow = moment.utc(modifiedDate).fromNow();

    switch (sort) {
        case ESort.NAME:
        case ESort.MODIFIED:
            return timeFromNow;
        case ESort.ASSOCIATED_ITEMS:
            return getAssociatedLabel(item);
        case ESort.OWNER:
            return ownerFullName;
        default:
            return timeFromNow;
    }
}

function convertToDataItem({getThumbnailImageUrl, userId, isAdmin, sort}, item: TInventoryItem): TDataItem {
    const { modifiedDate } = item;

    return assign({}, item, {
        label: getAssociatedLabel(item),
        deletable: isAdmin || (item.ownerUserId === userId),
        readableDate: moment(modifiedDate).format(TIME_PATTERNS.LAST_MODIFIED),
        thumbnailSubTitle: getThumbnailSubTitle(item, sort),
        thumbnailImageUrl: getThumbnailImageUrl(item)
    });
}

function getDataItems(homeModel: HomeModel, userModel: UserModel, getThumbnailImageUrl, isAdmin: boolean): Array<TDataItem> {
    const { inventory, searchQuery, sort } = homeModel.getProps();
    const { userId } = userModel.toJSON();

    return chain(inventory)
        .filter(inventoryItems => {
            return !isNull(inventoryItems);
        })
        .map(partial(convertToDataItem, {
            getThumbnailImageUrl,
            userId,
            isAdmin,
            sort
        }))
        .filter(item => {
            return item.name
                .toLowerCase()
                .match(searchQuery.toLowerCase());
        })
        .value();
}

function getEmptyState(items: Array<TInventoryItem>, addNewItems: Array<TAddNewDialogItem>, homeModel: HomeModel): TEmptyState {
    const {pending, searchQuery, route, isDisabledUser} = homeModel.getProps();

    let noDataItems = isEmpty(items) && !pending;
    let noSearchQuery = isEmpty(searchQuery);

    return {
        noAccess: isDisabledUser,
        noDataSource: noDataItems && isEqual(route, EFastFilterValue.ALL) && isEmpty(addNewItems),
        noCharts: noDataItems && isEqual(route, EFastFilterValue.ALL) && noSearchQuery,
        noRequestedItems: noDataItems && !isEqual(route, EFastFilterValue.ALL),
        noSearchResults: noDataItems && !noSearchQuery
    };
}

function getBookmarkFile(onSuccessCallback, onErrorCallback) {
    const fileUploader = new FileUploader();

    fileUploader
        .init()
        .getObservable()
        .subscribe(_onSuccess, _onError, _onComplete);

    function _onSuccess(event) {
        let currentTarget = event.currentTarget;
        onSuccessCallback(head(currentTarget.files));
    }

    function _onError(error) {
        onErrorCallback(error);
    }

    function _onComplete() {
        fileUploader.remove();
    }
}

function getQuery({user, sort, route}): TInventoryQuery {
    const queryConfig: TInventoryQuery = {
        accountId: user.accountId,
        sort: sort.sort,
        sortOrder: sort.sortOrder
    };

    switch (route) {
        case EFastFilterValue.ALL:
            queryConfig.includeItems = EIncludedItems.ALL;
            queryConfig.favorites = false;
            break;
        case EFastFilterValue.FAVORITES:
            queryConfig.includeItems = EIncludedItems.ALL;
            queryConfig.favorites = true;
            break;
        case EFastFilterValue.MY_ITEMS:
            queryConfig.includeItems = EIncludedItems.MINE;
            queryConfig.favorites = false;
            break;
        case EFastFilterValue.SHARED:
            queryConfig.includeItems = EIncludedItems.SHARED_WITH_ME;
            queryConfig.favorites = false;
            break;
    }

    return queryConfig;
}

// TODO: Should be based in users prefs
function getFastFilters(userModel: UserModel): Array<TFastFilter> {
    return FAST_FILTERS;
}

function getAddNewItems(userModel: UserModel, sources: Array<any>): Array<TAddNewDialogItem> {
    const canCreateSources = userModel.canCreateSources();
    const canCreateBookmarks = userModel.canCreateBookmarks();

    return ADD_NEW_DIALOG_ITEMS.filter((item) => {
        switch (item.value) {
            case EAddNewValue.NEW_CHARTS:
                return !isEmpty(sources);
            case EAddNewValue.NEW_SOURCE:
                return canCreateSources;
            case EAddNewValue.NEW_IMPORT_DASHBOARD:
                return canCreateBookmarks && canCreateSources;
            default:
                return true;
        }
    });
}
