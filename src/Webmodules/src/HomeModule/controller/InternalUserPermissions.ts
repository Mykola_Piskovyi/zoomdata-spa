/*
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
'use strict';

import {isUndefined} from 'lodash';
import {Model} from '../../../../Core';
import {Permissions} from '../../../../Api/src/models/UserModel';

export interface InternalUserPermissionsData {
    isAdmin: boolean;
    isDisabledUser: boolean;
    permissions: Permissions;
}

export default class InternalUserPermissions extends Model {

    attributes: InternalUserPermissionsData;

    constructor(options: InternalUserPermissionsData) {
        super(options);
    }

    isAdmin(isAdmin?: boolean): boolean {
        if (!isUndefined(isAdmin)) {
            this.set({isAdmin});
        }
        return !!this.get('isAdmin');
    }

    isDisabledUser(isDisabledUser?: boolean): boolean {
        if (!isUndefined(isDisabledUser)) {
            this.set({isDisabledUser});
        }
        return !!this.get('isDisabledUser');
    }

    setPermissions(permissions: Permissions): this {
        this.set({permissions});
        return this;
    }

    getPermissions(): Permissions {
        return this.get('permissions');
    }

}
