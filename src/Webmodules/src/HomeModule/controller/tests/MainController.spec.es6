/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import {assign, omit, isFunction, isBoolean, isObject, isEmpty, forIn, find, keysIn, difference} from 'lodash';
import {expect} from 'chai';
import sinon from 'sinon';
import * as Rx from 'rx';

import {UserModel} from '../../../../../Api';
import HomeModel from '../../model/HomeModel';
import ContainerView from '../../ContainerView/index';
import MainController from '../MainController';
import InternalUserPermissions from '../InternalUserPermissions';

import {FAST_FILTERS} from '../../SETTINGS';
import {EAddNewValue, EFastFilterValue} from '../../model/types';

import {dependency, DATA_ITEMS, USER_PERMISSIONS} from '../../tests/HomeModule.mock';

describe('MainController Unit Tests: ', () => {

    const getInternalUserPermissions = (options) => {
        return new InternalUserPermissions(
            assign({
                isDisabledUser: false,
                isAdmin: true,
                permissions: USER_PERMISSIONS
            }, options)
        );
    };

    const getOptions = (options = {}) => {
        return assign({
            el: document.createElement('div'),
            sources: [],
            dependency: dependency,
            isDisabledUser: false,
            getThumbnailImageUrl: sandbox.stub(),
            userPermissions: getInternalUserPermissions()
        }, options);
    };

    let sandbox;
    let options;
    let controller;

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        options = getOptions();
        controller = new MainController(options);
    });

    afterEach(() => {
        sandbox.restore();
        controller.remove();
    });

    it('should be defined', () => {
        expect(MainController).to.exist;
        expect(controller).to.exist;
    });

    describe('initialize: ', () => {

        let fetchUserByIdStub;
        let fetchUserPreferencesStub;

        beforeEach(() => {
            let {AccountUserDAO, UserPreferencesDAO} = dependency.container;
            fetchUserByIdStub = sandbox.stub(AccountUserDAO, 'fetchUserById');
            fetchUserPreferencesStub = sandbox.stub(UserPreferencesDAO, 'fetchUserPreferences');
        });

        it('should throw an error for no "el"', () => {
            expect(() => {
                let _options = omit(options, 'el');
                controller = new MainController(_options);
            }).to.throw('missing "el" in options');
        });

        it('should throw an error for no "dependency"', () => {
            expect(() => {
                let _options = omit(options, 'dependency');
                controller = new MainController(_options);
            }).to.throw('missing "dependency" in options');
        });

        it('NO subscriptions for DAO if user disabled (no requests) ', () => {
            let _options = getOptions({
                userPermissions: getInternalUserPermissions({isDisabledUser: true})
            });

            controller = new MainController(_options);

            let subscriptions = omit(controller._subscriptions, 'internalUserPermissions');

            expect(isEmpty(subscriptions)).to.be.true;
            expect(fetchUserByIdStub.called).to.be.false;
            expect(fetchUserPreferencesStub.called).to.be.false;
        });

        it('should have subscriptions for DAO if user not disabled ', () => {

            controller = new MainController(options);

            expect(isEmpty(controller._subscriptions)).to.be.false;
            expect(fetchUserByIdStub.called).to.be.true;
            expect(fetchUserPreferencesStub.called).to.be.true;
        });

    });

    describe('should have props and public methods: ', () =>  {
        it('should have "el, sources, dependency, userModel, homeModel, view"', () => {
            expect(controller.el).to.eql(options.el);
            expect(controller.sources).to.eql(options.sources);
            expect(controller.dependency).to.eql(options.dependency);
            expect(controller._userPermissions).to.exist;

            expect(controller.userModel instanceof UserModel).to.be.true;
            expect(controller.homeModel instanceof HomeModel).to.be.true;
            expect(controller.view instanceof ContainerView).to.be.true;
            expect(controller._userPermissions instanceof InternalUserPermissions).to.be.true;
        });

        // public methods
        [
            'render',
            'remove',
            'updateUserPreferences',
            'setRoute'
        ]
            .forEach((method) => {
                it(`public method "${method}()" should exist: `, () => {
                    expect(controller[method]).to.exist;
                    expect(isFunction(controller[method])).to.be.true;
                });
            });
    });

    describe('test public methods: ', () => {

        it('setRoute() should set homeModel route prop ', () => {
            controller.setRoute(EFastFilterValue.SHARED);
            expect(controller.homeModel.get('route')).to.eql(EFastFilterValue.SHARED);
        });

        it('subscriptions should be disposed for remove() call: ', () => {
            forIn(controller._subscriptions, (sub, key) => {
                expect(sub.isStopped).to.be.false;
            });

            controller.remove();

            forIn(controller._subscriptions, (sub) => {
                expect(sub.isStopped).to.be.true;
            });
        });

        it('should updateUserPreferences for remove()', () => {
            let updateStub = sandbox.stub(controller, 'updateUserPreferences');
            controller.remove();
            expect(updateStub.called).to.be.true;
        });
    });

    describe('test _storeSubscription(...): ', () => {

        it('should assign _subscriptions with subs', () => {
            const subs = {
                sub1: new Rx.Subject().asObservable().subscribe(),
                sub2: new Rx.Subject().asObservable().subscribe()
            };
            controller._storeSubscription(subs);

            expect(controller._subscriptions.sub1).to.eql(subs.sub1);
            expect(controller._subscriptions.sub2).to.eql(subs.sub2);
        });

        it('should update dispose for same object _subscriptions if exist', () => {
            const subs = {
                sub1: new Rx.Subject().asObservable().subscribe(),
                sub2: new Rx.Subject().asObservable().subscribe()
            };

            const sub1DisposeStub = sandbox.stub(subs.sub1, 'dispose', subs.sub1.dispose);
            const sub2DisposeStub = sandbox.stub(subs.sub2, 'dispose', subs.sub2.dispose);

            controller._storeSubscription(subs);

            const newSubs = {
                sub1: new Rx.Subject().asObservable().subscribe()
            };

            controller._storeSubscription(newSubs);

            expect(sub1DisposeStub.called).to.be.true;
            expect(sub2DisposeStub.called).to.be.false;
        });

        it('should throw an error if subscription not observable', () => {
            expect(() => {
                const subs = {
                    subValid: new Rx.Subject().asObservable().subscribe(),
                    subInvalid: {}
                };
                controller._storeSubscription(subs);
            }).to.throw('sub should be observable');
        });

    });

    describe('test _getRenderProps(options?) required props: ', () => {

        let opts;
        beforeEach(() => {
            opts = controller._getRenderProps();
        });

        it('has "sources"', () => {
            expect(opts.sources).to.eql(controller.sources);
        });

        it('has "fastFilters" from SETTINGS:', () => {
            expect(opts.fastFilters).to.eql(FAST_FILTERS);
        });

        it('has "emptyState" with props "noAccess, noDataSource, noCharts, noRequestedItems, noSearchResults"', () => {
            expect(isBoolean(opts.emptyState.noAccess)).to.be.true;
            expect(isBoolean(opts.emptyState.noDataSource)).to.be.true;
            expect(isBoolean(opts.emptyState.noCharts)).to.be.true;
            expect(isBoolean(opts.emptyState.noRequestedItems)).to.be.true;
            expect(isBoolean(opts.emptyState.noSearchResults)).to.be.true;
        });

        describe('test "searchLabel": ', () => {
            it('has default "Search All" for undefined', () => {
                expect(opts.searchLabel).to.eql('Search All');
            });

            it('has "Search Favorites" for homeModel "{route: favorites}"', () => {
                controller.homeModel.set('route', 'favorites');
                opts = controller._getRenderProps();

                expect(opts.searchLabel).to.eql('Search Favorites');
            });
        });

        describe('test "route": ', () => {
            it('has default "all" for undefined', () => {
                expect(opts.route).to.eql('all');
            });

            it('has "favorites" for homeModel "{route: favorites}"', () => {
                controller.homeModel.set('route', 'favorites');

                opts = controller._getRenderProps();
                expect(opts.route).to.eql('favorites');
            });
        });

        describe('test "initial": ', () => {
            it('should be "true" for first request with before get "inventory"', () => {
                expect(opts.initial).to.be.true;
            });

            it('should be "false" if "inventory" exist in homeModel', () => {
                controller.homeModel.set('inventory', []);
                opts = controller._getRenderProps();

                expect(opts.initial).to.be.false;
            });
        });

        describe('test "hideTopSwitcher": ', () => {
            it('should be "true" for first request', () => {
                expect(opts.hideTopSwitcher).to.be.true;
            });

            it('should be "false" if inventory exist', () => {
                controller.homeModel.set('inventory', DATA_ITEMS);
                opts = controller._getRenderProps();

                expect(opts.hideTopSwitcher).to.be.false;
            });

            it('should be "true" if dataItems exist and search does not match', () => {
                controller.homeModel.set('dataItems', DATA_ITEMS);
                controller.homeModel.set('pending', false);
                controller.homeModel.set('searchQuery', 'qweqwe');
                opts = controller._getRenderProps();

                expect(opts.hideTopSwitcher).to.be.true;
            });
        });

        describe('test "layout": ', () => {
            it('default "cards"', () => {
                expect(opts.layout).to.eq('cards');
            });

            it('same as in homeModel layout "table"', () => {
                controller.homeModel.set('layout', 'table');
                opts = controller._getRenderProps();

                expect(opts.layout).to.eq('table');
            });
        });

        describe('test "pending": ', () => {
            it('default "true"', () => {
                expect(opts.pending).to.eq(true);
            });

            it('same as in homeModel pending "false"', () => {
                controller.homeModel.set('pending', false);
                opts = controller._getRenderProps();

                expect(opts.pending).to.eq(false);
            });
        });

        describe('test "addNewItems": ', () => {

            const hasItem = (addNewItems, value) => (!!find(addNewItems, {value}));

            beforeEach(() => {
                controller._userPermissions.clear();
            });

            it('is empty if no sources and no user permissions', () => {
                opts = controller._getRenderProps();
                expect(opts.addNewItems).to.eql([]);
            });

            it(`has "${EAddNewValue.NEW_SOURCE}, ${EAddNewValue.NEW_IMPORT_DASHBOARD}" with user permissions and no sources`, () => {
                controller._userPermissions.setPermissions(USER_PERMISSIONS);
                opts = controller._getRenderProps();

                expect(hasItem(opts.addNewItems, EAddNewValue.NEW_SOURCE)).to.be.true;
                expect(hasItem(opts.addNewItems, EAddNewValue.NEW_IMPORT_DASHBOARD)).to.be.true;

                expect(hasItem(opts.addNewItems, EAddNewValue.NEW_CHARTS)).to.be.false;
            });

            it(`has "${EAddNewValue.NEW_CHARTS}, ${EAddNewValue.NEW_IMPORT_DASHBOARD}, ${EAddNewValue.NEW_SOURCE}" with all data and access`, () => {
                controller.sources = [{}, {}, {}];
                controller._userPermissions.setPermissions(USER_PERMISSIONS);
                opts = controller._getRenderProps();

                expect(hasItem(opts.addNewItems, EAddNewValue.NEW_CHARTS)).to.be.true;
                expect(hasItem(opts.addNewItems, EAddNewValue.NEW_IMPORT_DASHBOARD)).to.be.true;
                expect(hasItem(opts.addNewItems, EAddNewValue.NEW_SOURCE)).to.be.true;
            });

            it(`do not has "${EAddNewValue.NEW_IMPORT_DASHBOARD}" if no canCreateBookmarks permission`, () => {
                let permissions = assign({}, USER_PERMISSIONS, {dashboards: {save: false}});
                controller._userPermissions.setPermissions(permissions);
                opts = controller._getRenderProps();

                expect(hasItem(opts.addNewItems, EAddNewValue.NEW_IMPORT_DASHBOARD)).to.be.false;
            });

            it(`do not has "${EAddNewValue.NEW_SOURCE}" if no canCreateSources permission`, () => {
                let permissions = assign({}, USER_PERMISSIONS, {sources: {createNew: false}});
                controller._userPermissions.setPermissions(permissions);
                opts = controller._getRenderProps();

                expect(hasItem(opts.addNewItems, EAddNewValue.NEW_SOURCE)).to.be.false;
            });
        });

        describe('test "dataItems": ', () => {

            it('has empty "dataItems" by default', () => {
                expect(opts.dataItems).to.eql([]);
            });

            it('has "dataItems" same as in homeModel "inventory" (mapped)', () => {
                controller.homeModel.set('inventory', DATA_ITEMS);
                opts = controller._getRenderProps();

                expect(opts.dataItems.length).to.eql(DATA_ITEMS.length);
            });

            it('has empty "dataItems" if search does not match', () => {
                controller.homeModel.set('dataItems', DATA_ITEMS);
                controller.homeModel.set('searchQuery', 'qweqwe');
                opts = controller._getRenderProps();

                expect(opts.dataItems).to.eql([]);
            });
        });

        describe('test "fastFilter": ', () => {
            it('has default "Search All" for undefined', () => {
                opts = controller._getRenderProps();
                expect(opts.fastFilter).to.eql(FAST_FILTERS[0]);
            });

            it('has "Search Favorites" for "{route: favorites}"', () => {
                controller.homeModel.set('route', 'favorites');
                opts = controller._getRenderProps();

                expect(opts.fastFilter).to.eql(FAST_FILTERS[1]);
            });
        });

        describe('test "sortObject": ', () => {
            it('has same as in homeModel', () => {
                let expectedSort = controller.homeModel.get('sort');
                expect(opts.sortObject).to.eq(expectedSort);

                let newSort = {sort: 'name', sortOrder: 'ASC'};
                controller.homeModel.set('sort', newSort);
                opts = controller._getRenderProps();

                expect(opts.sortObject).to.eq(newSort);
            });
        });

    });

    describe('test _getQuery(): ', () => {

        let query;
        beforeEach(() => {
            query = controller._getQuery();
        });

        it('should be not empty', () => {
            expect(isEmpty(query)).to.be.false;
        });

        it('should return "accountId" as in user', () => {
            const {user} = controller.dependency.container;
            expect(query.accountId).to.eql(user.accountId);
        });

        it('should return "sort" as in homeModel', () => {
            const sort = controller.homeModel.get('sort');
            expect(query.sort).to.eql(sort.sort);
        });

        it('should return "sortOrder" as in homeModel', () => {
            const sort = controller.homeModel.get('sort');
            expect(query.sortOrder).to.eql(sort.sortOrder);
        });

        it('should return "includeItems"', () => {
            expect(query.includeItems).to.exist;
        });

        it('should return "favorites"', () => {
            expect(query.favorites).to.exist;
        });
    });

});
