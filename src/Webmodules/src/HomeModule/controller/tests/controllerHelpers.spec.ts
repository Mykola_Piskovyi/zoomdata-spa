/*
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
'use strict';

import { assign, isEmpty, isArray, isBoolean, head, find, clone, includes } from 'lodash';
import {expect} from 'chai';
import * as moment from 'moment';
import {
    ASSOCIATED_ITEM,
    DASHBOARD_ITEM,
    INVENTORY_ITEM,
    DATA_ITEMS,
    USER_PERMISSIONS } from '../../tests/HomeModule.mock';
import {
    getEmptyState,
    getDataItems,
    getThumbnailSubTitle,
    getAddNewItems,
    getFastFilters,
    getAssociatedLabel,
    getQuery,
    convertToDataItem
    } from '../controllerHelpers';
import { ADD_NEW_DIALOG_ITEMS, FAST_FILTERS, TIME_PATTERNS } from '../../SETTINGS';
import HomeModel from '../../model/HomeModel';
import UserModel from '../../../../../Api/src/models/UserModel';
import { EIncludedItems } from '../../../../../Api/src/daos/InventoryDAO';
import { EFastFilterValue, ESort, ESortOrder, TDataItem } from '../../model/types';

describe('controllerHelpers Unit Tests: ', () => {
    describe('test getEmptyState: ', () => {
        let emptyState;

        it('emptyState noAccess to be true', () => {
            emptyState = getEmptyState(DATA_ITEMS, ADD_NEW_DIALOG_ITEMS, new HomeModel({
                isDisabledUser: true
            }));
            expect(emptyState.noAccess).to.be.true;
        });

        it('emptyState noSearchResults to be true', () => {
            emptyState = getEmptyState([], ADD_NEW_DIALOG_ITEMS, new HomeModel({
                pending: false,
                searchQuery: 'searchQuery'
            }));
            expect(emptyState.noSearchResults).to.be.true;
        });

        it('emptyState noRequestedItems to be true', () => {
            emptyState = getEmptyState([], ADD_NEW_DIALOG_ITEMS, new HomeModel({
                pending: false,
                route: EFastFilterValue.MY_ITEMS
            }));
            expect(emptyState.noRequestedItems).to.be.true;
        });

        it('emptyState noCharts to be true', () => {
            emptyState = getEmptyState([], ADD_NEW_DIALOG_ITEMS, new HomeModel({
                pending: false
            }));
            expect(emptyState.noCharts).to.be.true;
        });

        it('emptyState noDataSource to be true', () => {
            emptyState = getEmptyState([], [], new HomeModel({
                pending: false
            }));
            expect(emptyState.noDataSource).to.be.true;
        });
    });

    describe('test getDataItems: ', () => {
        let dataItems: Array<TDataItem>;
        let homeModel = new HomeModel();
        let userModel = new UserModel();
        let isAdmin = true;

        const getThumbnailImageUrl = () => {
            return 'http://thumbnailImageUrl.com';
        };

        it('dataItems are empty array', () => {
            homeModel.set('inventory', null);
            dataItems = getDataItems(homeModel, userModel, getThumbnailImageUrl, isAdmin);
            expect(isArray(dataItems)).to.be.true;
            expect(isEmpty(dataItems)).to.be.true;
        });

        it('dataItems are returned according to searchQuery', () => {
            homeModel.set('inventory', DATA_ITEMS);
            homeModel.set('searchQuery', head(DATA_ITEMS).name);
            dataItems = getDataItems(homeModel, userModel, getThumbnailImageUrl, isAdmin);
            expect(dataItems.length).to.eql(2);
        });

        it('dataItem to contain extended values', () => {
            homeModel.set('inventory', DATA_ITEMS);
            dataItems = getDataItems(homeModel, userModel, getThumbnailImageUrl, isAdmin);

            const {thumbnailImageUrl, label, deletable, thumbnailSubTitle, readableDate} = head(dataItems);

            expect(thumbnailImageUrl).to.not.be.undefined;
            expect(label).to.not.be.undefined;
            expect(deletable).to.not.be.undefined;
            expect(thumbnailSubTitle).to.not.be.undefined;
            expect(readableDate).to.not.be.undefined;
        });
    });

    describe('test getThumbnailSubTitle: ', () => {
        let sortObject = {
            sort: ESort.NAME,
            sortOrder: ESortOrder.ASC
        };
        let thumbnailSubTitle;
        const getSortObject = (options = {}) => {
            return assign({}, sortObject, options);
        };
        const { modifiedDate, ownerFullName } = DASHBOARD_ITEM;
        const timeFromNow = moment.utc(modifiedDate).fromNow();
        const associatedLabel = getAssociatedLabel(DASHBOARD_ITEM);

        it('correct thumbnailSubTitle is assigned', () => {
            expect(getThumbnailSubTitle(DASHBOARD_ITEM, sortObject)).to.eql(timeFromNow);
            expect(getThumbnailSubTitle(DASHBOARD_ITEM, getSortObject({sort: ESort.MODIFIED}))).to.eql(timeFromNow);
            expect(getThumbnailSubTitle(DASHBOARD_ITEM, getSortObject({sort: ESort.OWNER}))).to.eql(ownerFullName);
            expect(getThumbnailSubTitle(DASHBOARD_ITEM, getSortObject({sort: ESort.ASSOCIATED_ITEMS}))).to.eql(associatedLabel);
        });

        it('default thumbnailSubTitle is assigned for unproper sort', () => {
            expect(getThumbnailSubTitle(DASHBOARD_ITEM, getSortObject({sort: 'someUnProperSorting'}))).to.eql(timeFromNow);
        });
    });

    describe('test getQuery: ', () => {
        let userModel = new UserModel({accountId: '1234567890'});
        let sortObject = {
            sort: ESort.NAME,
            sortOrder: ESortOrder.ASC
        };
        let route = EFastFilterValue.ALL;
        let queryConfig;

        const getQueryOptions = (options = {}) => {
            return assign({}, {
                user: userModel,
                route: route,
                sort: sortObject
            }, options);
        };

        it('ALL items are included, NO favorites', () => {
            queryConfig = getQuery(getQueryOptions());
            expect(queryConfig.includeItems).to.eql(EIncludedItems.ALL);
            expect(queryConfig.favorites).to.be.false;
        });

        it('ALL items are included, favorites', () => {
            queryConfig = getQuery(getQueryOptions({route: EFastFilterValue.FAVORITES}));
            expect(queryConfig.includeItems).to.eql(EIncludedItems.ALL);
            expect(queryConfig.favorites).to.be.true;
        });

        it('MINE items are included, NO favorites', () => {
            queryConfig = getQuery(getQueryOptions({route: EFastFilterValue.MY_ITEMS}));
            expect(queryConfig.includeItems).to.eql(EIncludedItems.MINE);
            expect(queryConfig.favorites).to.be.false;
        });

        it('SHARED items are included, NO favorites', () => {
            queryConfig = getQuery(getQueryOptions({route: EFastFilterValue.SHARED}));
            expect(queryConfig.includeItems).to.eql(EIncludedItems.SHARED_WITH_ME);
            expect(queryConfig.favorites).to.be.false;
        });
    });

    describe('test getFastFilters: ', () => {
        it('should return proper fastFilters', () => {
            let fastFilters = getFastFilters(new UserModel());

            expect(fastFilters.length).to.eql(FAST_FILTERS.length);
            expect(find(fastFilters, {value: EFastFilterValue.ALL})).to.not.be.undefined;
            expect(find(fastFilters, {value: EFastFilterValue.FAVORITES})).to.not.be.undefined;
            expect(find(fastFilters, {value: EFastFilterValue.MY_ITEMS})).to.not.be.undefined;
            expect(find(fastFilters, {value: EFastFilterValue.SHARED})).to.not.be.undefined;
        });
    });

    describe('test convertToDataItem: ', () => {
        let sortObject = {
            sort: ESort.NAME,
            sortOrder: ESortOrder.ASC
        };
        let userId = INVENTORY_ITEM.ownerUserId;
        let isAdmin = true;
        const getThumbnailImageUrl = (item) => {
            const { ownerUserId } = item;

            return 'http://thumbnailImageUrl.com/' + ownerUserId;
        };
        const dataItem = convertToDataItem({
            getThumbnailImageUrl: getThumbnailImageUrl,
            userId: userId,
            isAdmin: isAdmin,
            sort: sortObject
        }, INVENTORY_ITEM);

        it('should assign proper "label" value', () => {
            const { label } = dataItem;
            expect(label).to.not.be.undefined;
            expect(label).to.eql(getAssociatedLabel(INVENTORY_ITEM));
            expect(label).to.be.a('string');
        });

        it('should assing proper "deletable" value', () => {
            const { deletable } = dataItem;
            expect(deletable).to.not.be.undefined;
            expect(deletable).to.eql(isAdmin);
            expect(isBoolean(deletable)).to.be.true;
        });

        it('should assign proper "readableData" value', () => {
            const { modifiedDate } = INVENTORY_ITEM;
            const { readableDate } = dataItem;
            expect(readableDate).to.not.be.undefined;
            expect(readableDate).to.eql(moment.utc(modifiedDate).format(TIME_PATTERNS.LAST_MODIFIED));
            expect(readableDate).to.be.a('string');
        });

        it('should assign proper "thumbnailSubTitle" value', () => {
            const { thumbnailSubTitle } = dataItem;
            expect(thumbnailSubTitle).to.not.be.undefined;
            expect(thumbnailSubTitle).to.eql(getThumbnailSubTitle(INVENTORY_ITEM, sortObject));
            expect(thumbnailSubTitle).to.be.a('string');
        });

        it('should assign proper "thumbnailImageUrl" value', () => {
            const { thumbnailImageUrl } = dataItem;
            expect(thumbnailImageUrl).to.not.be.undefined;
            expect(thumbnailImageUrl).to.eql(getThumbnailImageUrl(INVENTORY_ITEM));
            expect(thumbnailImageUrl).to.be.a('string');
        });
    });

    describe('test getAssociatedLabel: ', () => {
        it('should return source name as label for 1st associatedItem', () => {
            const { associatedItems } = INVENTORY_ITEM;
            const { name } = head(associatedItems);

            expect(getAssociatedLabel(INVENTORY_ITEM)).to.eql(name);
        });

        it('should return modified label', () => {
            let inventoryItem = clone(INVENTORY_ITEM);
            inventoryItem.associatedItems.push(ASSOCIATED_ITEM);
            const { associatedItems } = inventoryItem;

            expect(includes(getAssociatedLabel(inventoryItem), '...(' + associatedItems.length + ')')).to.be.true;
        });
    });
});
