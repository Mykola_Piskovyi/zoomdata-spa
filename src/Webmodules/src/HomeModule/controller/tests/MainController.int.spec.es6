/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import {assign, isObject, difference, keysIn, cloneDeep} from 'lodash';
import {expect} from 'chai';
import sinon from 'sinon';

import MainController from '../MainController';
import InternalUserPermissions from '../InternalUserPermissions';
import {HOME_USER_PREFERENCES} from '../../SETTINGS';
import {ESort, ESortOrder, ELayout} from '../../model/types';

import {dependency, USER_PERMISSIONS, DATA_ITEMS, USER, USER_PREFERENCES} from '../../tests/HomeModule.mock';

describe('MainController Integration Tests: ', () => {

    let sandbox;
    let options;
    let controller;

    const getOptions = (options) => {
        return assign({
            el: document.createElement('div'),
            sources: [],
            dependency: dependency,
            getThumbnailImageUrl: sandbox.stub(),
            userPermissions: new InternalUserPermissions({
                isDisabledUser: false,
                isAdmin: true,
                permissions: USER_PERMISSIONS
            })
        }, options);
    };

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        options = getOptions();
        controller = new MainController(options);
    });

    afterEach(() => {
        sandbox.restore();
        controller.remove();
    });

    it('should set "route: favorites" to homeModel if have on options', () => {
        options = getOptions({'route': 'favorites'});
        controller = new MainController(options);

        expect(controller.homeModel.get('route')).to.eq('favorites');
    });

    it('call "_userPermissions.setPermissions()" should re-render and userModel updated', () => {
        controller._userPermissions.clear();

        let renderStub = sandbox.stub(controller, 'render');
        let setpermissionsStub = sandbox.stub(controller.userModel, 'setPermissions');

        controller._userPermissions.setPermissions(USER_PERMISSIONS);

        expect(setpermissionsStub.called).to.be.true;
        expect(setpermissionsStub.getCall(0).args[0]).to.eql(USER_PERMISSIONS);
        expect(renderStub.called).to.be.true;
    });

    it('"render()" should call view.render: ', () => {
        let view_render = sandbox.stub(controller.view, 'render');
        controller.render();

        expect(view_render.called).to.be.true;
    });

    it('"remove()" should call view.remove: ', () => {
        let view_remove = sandbox.stub(controller.view, 'remove');
        controller.remove();

        expect(view_remove.called).to.be.true;
    });

    describe('_fetchInventory() and InventoryDAO integration: ', () => {

        it('should call "_getQuery()"', () => {
            const getQueryStub = sandbox.stub(controller, '_getQuery');

            controller._fetchInventory();
            expect(getQueryStub.called).to.be.true;
        });

        it('should call InventoryDAO "updateQuery()" with query', () => {
            const {InventoryDAO} = controller.dependency.container;
            const updateStub = sandbox.stub(InventoryDAO, 'updateQuery', InventoryDAO.updateQuery);

            const query = controller._getQuery();
            controller._fetchInventory();

            expect(updateStub.called).to.be.true;
            expect(updateStub.getCall(0).args).to.eql([query]);
        });

        it('should call InventoryDAO "fetchInventory()"', () => {
            const {InventoryDAO} = controller.dependency.container;
            const fetchInventoryStub = sandbox.stub(InventoryDAO, 'fetchInventory');
            controller._fetchInventory();

            expect(fetchInventoryStub.called).to.be.true;
        });

    });

    describe('check subscriptions and events for cover: ', () => {
        const subEvents = [
            'AccountUserDAO',
            'UserPreferencesDAO',
            'InventoryDAO',
            'InventoryDAOError',
            'changeRoute',
            'onSearch',
            'onSortChange',
            'onLayoutChange',
            'onChartOpen',
            'onVisualizationOpen',
            'onChartDelete',
            'onChartFavorite',
            'onNewDialogEvent',
            'onBeforeUrlUnload',
            'internalUserPermissions'
        ];

        subEvents.forEach((event) => {
            it(`should have "${event}" subscription`, () => {
                expect(
                    isObject(controller._subscriptions[event])
                ).to.be.true;
            });
        });

        it(`should have ${subEvents.length} subscriptions and covered`, () => {
            expect(
                difference(keysIn(controller._subscriptions), subEvents)
            ).to.eql([]);

            expect(
                keysIn(controller._subscriptions).length
            ).to.equal(subEvents.length);
        });
    });

    describe('test events and "view and window" integration:', () => {

        let triggerStub;
        let fetchInventoryStub;
        let renderStub;
        let InventoryDAO;

        const triggerEvent = (event, data) => {
            controller.view.trigger(event, data);
        };

        beforeEach(() => {
            InventoryDAO = controller.dependency.container.InventoryDAO;
            triggerStub = sandbox.stub(controller, 'trigger', controller.trigger);
            fetchInventoryStub = sandbox.stub(InventoryDAO, 'fetchInventory');
            renderStub = sandbox.stub(controller, 'render');
        });

        describe('event "changeRoute": ', () => {
            let route = 'favorites';
            beforeEach(() => {
                triggerEvent('changeRoute', route);
            });

            it('should update homeMode "pending: true", "route"', () => {
                expect(controller.homeModel.get('route')).to.eql(route);
                expect(controller.homeModel.get('pending')).to.eql(true);
            });

            it('should trigger "changeRoute" on top', () => {
                expect(triggerStub.called).to.be.true;
                expect(triggerStub.getCall(0).args).to.eql(['changeRoute', route]);
            });

            it('should InventoryDAO.fetchInventory()', () => {
                expect(fetchInventoryStub.called).to.be.true;
            });
        });

        describe('event "onSortChange": ', () => {
            let sort;
            beforeEach(() => {
                sort = {
                    sort: ESort.NAME,
                    sortOrder: ESortOrder.DESC
                };
                triggerEvent('onSortChange', sort);
            });

            it('should update homeMode "pending: true", "sort"', () => {
                expect(controller.homeModel.get('sort')).to.eql(sort);
                expect(controller.homeModel.get('pending')).to.eql(true);
            });

            it('should re-render view', () => {
                expect(renderStub.called).to.be.true;
            });

            it('should InventoryDAO.fetchInventory()', () => {
                expect(fetchInventoryStub.called).to.be.true;
            });
        });

        describe('event "onSearch": ', () => {
            let search;
            beforeEach(() => {
                search = 'some text';
                triggerEvent('onSearch', search);
            });

            it('should update homeMode "searchQuery"', () => {
                expect(controller.homeModel.get('searchQuery')).to.eql(search);
            });

            it('should re-render view', () => {
                expect(renderStub.called).to.be.true;
            });
        });

        describe('event "onLayoutChange": ', () => {
            let layout;
            beforeEach(() => {
                layout = 'table';
                triggerEvent('onLayoutChange', layout);
            });

            it('should update homeMode "layout"', () => {
                expect(controller.homeModel.get('layout')).to.eql(layout);
            });

            it('should re-render view', () => {
                expect(renderStub.called).to.be.true;
            });
        });

        describe('event "onChartOpen": ', () => {
            it('should trigger "onChartOpen" on top with data item', () => {
                const dataItem = DATA_ITEMS[0];
                triggerEvent('onChartOpen', dataItem);

                expect(triggerStub.called).to.be.true;
                expect(triggerStub.getCall(0).args).to.eql(['onChartOpen', dataItem]);
            });
        });

        describe('event "onVisualizationOpen": ', () => {
            it('should trigger "onVisualizationOpen" on top with data item', () => {
                const visItem = {};
                triggerEvent('onVisualizationOpen', visItem);

                expect(triggerStub.called).to.be.true;
                expect(triggerStub.getCall(0).args).to.eql(['onVisualizationOpen', visItem]);
            });
        });

        describe('event "onChartDelete": ', () => {
            it('should trigger "onChartDelete" on top with data item', () => {
                const visItem = {};
                triggerEvent('onChartDelete', visItem);

                expect(triggerStub.called).to.be.true;
                expect(triggerStub.getCall(0).args).to.eql(['onChartDelete', visItem]);
            });
        });

        describe('event "onChartFavorite": ', () => {

            let item;
            let unfavoriteStub;
            let favoriteStub;
            beforeEach(() => {
                item = {
                    inventoryItemId: 'inventoryItemId',
                    type: 'type',
                    favorite: true
                };

                unfavoriteStub = sandbox.stub(InventoryDAO, 'unfavoriteItem');
                favoriteStub = sandbox.stub(InventoryDAO, 'favoriteItem');
                triggerEvent('onChartFavorite', item);
            });

            it('should update homeMode "pending: true"', () => {
                expect(controller.homeModel.get('pending')).to.eql(true);
            });

            it('should re-render view', () => {
                expect(renderStub.called).to.be.true;
            });

            it('should trigger "onChartFavorite" on top with data item', () => {
                expect(triggerStub.called).to.be.true;
                expect(triggerStub.getCall(0).args).to.eql(['onChartFavorite', item]);
            });

            it('should call InventoryDAO.unfavoriteItem if "favorite: true"', () => {
                expect(unfavoriteStub.called).to.be.true;
                expect(unfavoriteStub.getCall(0).args).to.eql([item.inventoryItemId, item.type]);
            });

            it('should call InventoryDAO.favoriteItem if "favorite: false"', () => {
                const itemFavFalse = assign({}, item, {favorite: false});
                controller.view.trigger('onChartFavorite', itemFavFalse);

                expect(favoriteStub.called).to.be.true;
                expect(favoriteStub.getCall(0).args).to.eql([item.inventoryItemId, item.type]);
            });

        });

        describe('event "onNewDialogEvent": ', () => {

            it('event value NEW_SOURCE trigger "goToAdmin" on top', () => {
                triggerEvent('onNewDialogEvent', 'NEW_SOURCE');

                expect(triggerStub.called).to.be.true;
                expect(triggerStub.getCall(0).args).to.eql(['goToAdmin']);
            });

            it('event value" NEW_IMPORT_DASHBOARD call _getBookmarkFile()', () => {
                const getBookmarkStub = sandbox.stub(controller, '_getBookmarkFile');
                triggerEvent('onNewDialogEvent', 'NEW_IMPORT_DASHBOARD');

                expect(getBookmarkStub.called).to.be.true;
            });
        });

        // README: Tests stopped after run this spec for window 'beforeunload'
        // describe('event window "beforeunload": ', () => {
        //     it('should call updateUserPreferences', () => {
        //         const updatePrefStub = sandbox.stub(controller, 'updateUserPreferences');
        //         window.dispatchEvent(new Event('beforeunload'));
        //
        //         expect(updatePrefStub.called).to.be.true;
        //     });
        // });

    });

    describe('test InventoryDAO integration:', () => {

        let InventoryDAO;
        let renderStub;
        let modelSetStub;
        beforeEach(() => {
            InventoryDAO = controller.dependency.container.InventoryDAO;
            renderStub = sandbox.stub(controller, 'render');
            modelSetStub = sandbox.stub(controller.homeModel, 'set', controller.homeModel.set);
        });

        it('should update homeModel "inventory, dataItems, pending" for changes', () => {
            let data = cloneDeep(DATA_ITEMS);
            InventoryDAO._subject.onNext(data);

            const {inventory, dataItems, pending} = controller.homeModel.getProps();

            expect(inventory).to.eql(data);
            expect(dataItems).to.not.eql(data);
            expect(dataItems.length).to.eql(data.length);
            expect(pending).to.eql(false);
        });

        it('should render for changes', () => {
            let data = [];
            InventoryDAO._subject.onNext(data);

            expect(renderStub.called).to.be.true;
        });

        it('should handle error and render with "pending: false"', () => {
            const error = 'some error';
            InventoryDAO._subject.onNext({type: 'ERROR', data: error});

            expect(modelSetStub.callCount).to.be.equal(1);
            expect(modelSetStub.getCall(0).args).to.eql(['pending', false]);
            expect(renderStub.called).to.be.true;
        });

    });

    describe('test AccountUserDAO integration:', () => {

        let AccountUserDAO;
        let renderStub;
        let userModelSetStub;
        let userData;
        beforeEach(() => {
            userData = assign({}, USER, {userId: USER.id});
            delete userData.id;
            AccountUserDAO = controller.dependency.container.AccountUserDAO;
            renderStub = sandbox.stub(controller, 'render');
            userModelSetStub = sandbox.stub(controller.userModel, 'set');
        });

        it('should update userModel for fetch user', () => {
            AccountUserDAO._subject.onNext([userData]);

            expect(userModelSetStub.called).to.be.true;
            expect(userModelSetStub.getCall(0).args[0]).to.eql(userData);
        });

        it('should update userModel for get user new prop', () => {
            let newUserData = assign({}, userData, {name: 'newName'});
            AccountUserDAO._subject.onNext([userData]);
            AccountUserDAO._subject.onNext([userData]);

            expect(userModelSetStub.callCount).to.be.equal(1);
            expect(userModelSetStub.getCall(0).args[0]).to.eql(userData);

            AccountUserDAO._subject.onNext([newUserData]);
            expect(userModelSetStub.callCount).to.be.equal(2);
            expect(userModelSetStub.getCall(1).args[0]).to.eql(newUserData);
        });

    });

    describe('test UserPreferencesDAO integration:', () => {

        const {VERSION, KEY} = HOME_USER_PREFERENCES;

        let UserPreferencesDAO;
        let restoreUserPreferencesStub;
        let fetchInventoryStub;
        let prefs;
        beforeEach(() => {
            prefs = [].concat(
                cloneDeep(USER_PREFERENCES[0]),
                assign({}, USER_PREFERENCES[0], {
                    key: 'SOME_KEY',
                    value: {version: '2', value: '1111'}
                })
            );
            UserPreferencesDAO = controller.dependency.container.UserPreferencesDAO;
            restoreUserPreferencesStub = sandbox.stub(controller.homeModel, 'restoreUserPreferences');
            fetchInventoryStub = sandbox.stub(controller, '_fetchInventory');
        });

        it(`should restore homeModel.restoreUserPreferences() for user pref. with key "${KEY}"`, () => {
            UserPreferencesDAO._subject.onNext(prefs);

            expect(restoreUserPreferencesStub.called).to.be.true;
            expect(restoreUserPreferencesStub.getCall(0).args[0]).to.eql(USER_PREFERENCES[0]);
        });

        it(`should _fetchInventory for user pref. with key "${KEY}"`, () => {
            UserPreferencesDAO._subject.onNext(prefs);
            expect(fetchInventoryStub.called).to.be.true;
        });


        it('should call UserPreferencesDAO.updateUserPreferences with needed args', () => {
            const updateUserPrefDAOStub = sandbox.stub(UserPreferencesDAO, 'updateUserPreferences');
            const sort = {
                sort: ESort.OWNER,
                sortOrder: ESortOrder.DESC
            };
            const layout = ELayout.TABLE;

            controller.homeModel.set('sort', sort);
            controller.homeModel.set('layout', layout);

            const expectedPrefs = {
                sort: ESort.OWNER,
                sortOrder: ESortOrder.DESC,
                layout
            };
            controller.updateUserPreferences();

            expect(updateUserPrefDAOStub.called).to.be.true;

            const [prefsArg, ...keyAndVersion] = updateUserPrefDAOStub.getCall(0).args;
            expect(prefsArg).to.eql(expectedPrefs);
            expect(keyAndVersion).to.eql([KEY, VERSION]);
        });

    });

});
