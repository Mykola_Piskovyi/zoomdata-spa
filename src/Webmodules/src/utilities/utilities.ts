/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import {VIS_TYPES} from './constants';

export function getVisTypeIcon(type: string): string {
    let icon = '';

    switch (type) {
        case VIS_TYPES.DASHBOARD:
            icon = 'dashboard';
            break;
        case VIS_TYPES.UBER_BARS:
            icon = 'bars_vert';
            break;
        case VIS_TYPES.MULTI_METRIC_BARS:
            icon = 'bars_vert';
            break;
        case VIS_TYPES.PIE:
            icon = 'pie';
            break;
        case VIS_TYPES.DONUT:
            icon = 'donut';
            break;
        case VIS_TYPES.FLOATING_BUBBLES:
            icon = 'bubbles_float';
            break;
        case VIS_TYPES.HEAT_MAP:
            icon = 'heatmap';
            break;
        case VIS_TYPES.TREE_MAP:
            icon = 'treemap';
            break;
        case VIS_TYPES.KPI:
            icon = 'KPIs';
            break;
        case VIS_TYPES.LINE_AND_BARS:
            icon = 'lines_bars';
            break;
        case VIS_TYPES.LINE_CHART_CONTINUOUS:
            icon = 'lines';
            break;
        case VIS_TYPES.LINE_CHART:
            icon = 'lines';
            break;
        case VIS_TYPES.BUBBLES:
            icon = 'bubbles_packed';
            break;
        case VIS_TYPES.PIVOT_TABLE:
            icon = 'table_pivot';
            break;
        case VIS_TYPES.RAW_DATA_TABLE:
            icon = 'table';
            break;
        case VIS_TYPES.SCATTERPLOT:
            icon = 'scatterplot';
            break;
        case VIS_TYPES.HISTOGRAM:
            icon = 'bars_hstgrm';
            break;
        case VIS_TYPES.WORD_CLOUD:
            icon = 'word_cloud';
            break;
        case VIS_TYPES.BOX_PLOT:
            icon = 'bars_vert_bxplt';
            break;
        case VIS_TYPES.MAP_LAT_LONG:
            icon = 'map_pin_light';
            break;
        case VIS_TYPES.US_MAP:
            icon = 'map_us_light';
            break;
        case VIS_TYPES.WORLD_MAP:
            icon = 'map_world_light';
            break;
        case VIS_TYPES.FINGERPAINT:
            icon = 'style';
            break;
        case VIS_TYPES.BLANK:
            icon = 'visualization_type';
            break;
        case VIS_TYPES.CUSTOM:
            icon = 'custom_chart';
            break;
    }

    return icon;
}