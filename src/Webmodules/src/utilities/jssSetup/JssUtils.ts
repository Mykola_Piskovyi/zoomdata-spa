const createJss = require('jss').create;
const createInjectSheet = require('react-jss').create;

const vendorPrefixer = require('jss-vendor-prefixer').default;
const camelCase = require('jss-camel-case').default;
const jssNested = require('jss-nested').default;

const jss = createJss();
jss.use(jssNested(), camelCase(), vendorPrefixer());

export const injectSheet = createInjectSheet(jss);
