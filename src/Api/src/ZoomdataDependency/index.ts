import { assign, isUndefined } from 'lodash';
import * as Bottle from 'bottlejs';
import InventoryDAO from '../daos/InventoryDAO';
import UserDAO from '../daos/UserDAO';
import AccountUserDAO from '../daos/AccountUserDAO';
import UserPreferencesDAO from '../daos/UserPreferencesDAO';

const CONTENT_TYPE_ZOOMDATA_V1_JSON = {
    'Accept': '*/*',
    'Content-Type': 'application/vnd.zoomdata.v1+json'
};
const CONTENT_TYPE_ZOOMDATA_V2_JSON = assign({}, CONTENT_TYPE_ZOOMDATA_V1_JSON, {
    'Content-Type': 'application/vnd.zoomdata.v2+json'
});

export interface ValuesConfig {
    user: {
        accountId: string;
        id: string;
    };
    entryApiUrl: string;
    contextPath: string;
    headers?: any;
}

export interface DAOs {
    InventoryDAO: InventoryDAO;
    AccountUserDAO: AccountUserDAO;
    UserPreferencesDAO: UserPreferencesDAO;
}

export interface ZDIContainer extends Bottle.IContainer, ValuesConfig, DAOs {}

export default class ZoomdataDependency extends Bottle {

    private static _instance: ZoomdataDependency;
    public container: ZDIContainer;

    constructor(name?: string) {
        super(name);

        if (ZoomdataDependency._instance) {
            throw new Error('Use ZoomdataDependency.getInstance() to get an instance.');
        }
    }

    public static getInstance() {
        if (isUndefined(this._instance)) {
            this._instance = new ZoomdataDependency();
        }

        return this._instance;
    }

    initValuesWithDAO(config: ValuesConfig) {
        // TODO: Be good to have validation for required args by each DAO

        this.value('entryApiUrl', config.entryApiUrl)
            .value('user', config.user)
            .value('contextPath', config.contextPath)
            .value('headers', config.headers);

        this._initFactoryDAO();
    }

    getDAOs(): DAOs {
        const {user, entryApiUrl} = this.container;

        if (isUndefined(user)) {
            throw new Error('"user" value should be defined');
        }

        if (isUndefined(entryApiUrl)) {
            throw new Error('"entryApiUrl" value should be defined');
        }

        return {
            InventoryDAO: this.container.InventoryDAO,
            AccountUserDAO: this.container.AccountUserDAO,
            UserPreferencesDAO: this.container.UserPreferencesDAO
        };
    }

    private _initFactoryDAO() {

        this.factory('InventoryDAO', (container: any) => {
                const {headers, user} = container;
                return InventoryDAO.getInstance({
                    uri: `${container.entryApiUrl}/api/inventory`,
                    headers: assign({}, headers, CONTENT_TYPE_ZOOMDATA_V2_JSON),
                    query: {
                        accountId: container.user.accountId
                    }
                });
            })
            // README: Means Account Users.
            // README: "api/users" restricted only for supervisor
            .factory('AccountUserDAO', (container: any) => {
                const {id, accountId, headers} = container.user;
                return AccountUserDAO.getInstance({
                    uri: `${container.entryApiUrl}/api/accounts/${accountId}/users`,
                    USERS_URI: `${container.entryApiUrl}/api/users`,
                    headers: assign({}, headers, CONTENT_TYPE_ZOOMDATA_V1_JSON),
                    accountId: accountId,
                    currentUserId: id
                });
            })
            .factory('UserPreferencesDAO', (container: any) => {
                const {headers, user} = container;
                return UserPreferencesDAO.getInstance({
                    uri: `${container.entryApiUrl}/api/preferences`,
                    headers: assign({}, headers, CONTENT_TYPE_ZOOMDATA_V2_JSON),
                    user: user
                });
            });

    }

}
