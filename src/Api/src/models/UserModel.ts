import * as ZoomdataCore from '../../../Core';

export type ModifiedData = {
    by?: {
        links: any;
        username: string;
    };
    on: string;
};

export type User = {
    administrator: boolean;
    created: ModifiedData;
    enabled: boolean;
    lastModified: ModifiedData;
    userId: string;
    username: string;
    links: any;
};

export type PermissionsOpts = {
    isDemoMode?: boolean;
};

export type Permissions = {
    connections: {
        manage: boolean;
    };
    sources: {
        read: boolean;
        write: boolean;
        remove: boolean;
        createNew: boolean;
        manageSome: boolean;
    };
    dashboards: {
        save: boolean;
        share: boolean;
    };
    filters: {
        save: boolean;
    };
    formulas: {
        edit: boolean;
        read: boolean;
    }
};

export default class UserModel extends ZoomdataCore.Model {

    attributes: User;
    // FIXMe: remove this._permissions after start use permissions based on groups
    private _permissions: Permissions;

    getRoles() {

    }

    // FIXME: Temp solution till have resources api (hateoas?)
    setPermissions(permissions: Permissions): this {
        this._permissions = permissions;
        return this;
    }

    getPermissions(options: PermissionsOpts = {}): Permissions {
        const isAdmin = this.get('administrator');
        const isDemoUser = options.isDemoMode;

        return this._permissions || {
            connections: {
                manage: isAdmin && !isDemoUser
            },
            sources: {
                read: isAdmin,
                write: isAdmin,
                remove: isAdmin,
                createNew: isAdmin,
                manageSome: isAdmin
            },
            dashboards: {
                save: isAdmin,
                share: isAdmin
            },
            filters: {
                save: isAdmin
            },
            formulas: {
                edit: isAdmin,
                read: isAdmin
            }
        };
    }

    canCreateSources(): boolean {
        return this.getPermissions().sources.createNew;
    }

    canManageSources(): boolean {
        return this.getPermissions().sources.manageSome;
    }

    canCreateBookmarks(): boolean {
        return this.getPermissions().dashboards.save;
    }

    canShareBookmarks(): boolean {
        return this.getPermissions().dashboards.share;
    }

    canSaveFilters(): boolean {
        return this.getPermissions().filters.save;
    }

    canSaveFormulas(): boolean {
        return this.getPermissions().formulas.edit;
    }
}
