import * as ZoomdataCore from '../../../Core';

export type AssociatedItem = {
    favorite: boolean;
    inventoryItemId: string;
    links?: Array<any>;
    name: string;
    type: string;
    typeName: string;
};

export enum EInventoryLinkTypes {
    UNFAVORITE = <any>'unfavorite',
    FAVORITE = <any>'favorite',
    SELF = <any>'self'
}

export type InventoryLink = {
    href: string;
    rel: EInventoryLinkTypes;
};

export type Inventory = {
    associatedItems: Array<AssociatedItem>;
    description: string;
    favorite: boolean;
    inventoryItemId: string;
    links: Array<InventoryLink>;
    modifiedDate: string;
    name: string;
    ownerFullName: string;
    ownerUserId: string;
    thumbnailDate?: string;
    type: string;
    typeName: string;
};

export class InventoryItemModel extends ZoomdataCore.Model {
    attributes: Inventory;
}