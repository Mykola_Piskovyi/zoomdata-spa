import * as ZoomdataCore from '../../../Core';

export type VersionedValue = {
    version?: string;
    value: any;
};

export type UserPreference = {
    accountId: string;
    key: string;
    links?: Array<any>;
    preferenceId?: string;
    userId: string;
    value: VersionedValue;
};

export default class UserPreferenceModel extends ZoomdataCore.Model {
    attributes: UserPreference;
}
