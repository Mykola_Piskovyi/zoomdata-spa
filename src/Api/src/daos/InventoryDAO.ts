import * as ZoomdataCore from '../../../Core';
import * as Promise from 'promise';
import * as _ from 'lodash';
import { Observable, Observer, ReplaySubject } from 'rx';
import * as traverson from 'traverson';

import { InventoryItemModel, Inventory, EInventoryLinkTypes } from '../models/InventoryItemModel';

export enum EIncludedItems {
    ALL = <any>'ALL',
    SHARED_WITH_ME = <any>'SHARED_WITH_ME',
    MINE = <any>'MINE'
}

export enum ESort {
    NAME = <any>'NAME',
    DESCRIPTION = <any>'DESCRIPTION',
    TYPE = <any>'TYPE',
    OWNER = <any>'OWNER',
    MODIFIED = <any>'MODIFIED_DATE',
    FAVORITES = <any>'FAVORITES',
    ASSOCIATED_ITEMS = <any>'ASSOCIATED_ITEMS'
}

export enum ESortOrder {
    ASC = <any>'ASC',
    DESC = <any>'DESC',
}

export type TInventoryQuery = {
    accountId?: string;
    nameQuery?: string;
    includeItems?: EIncludedItems;
    sort?: ESort;
    sortOrder?: ESortOrder;
    favorites?: boolean;
};

export type TInventoryConfig = {
    uri: string;
    headers: any;
    query?: TInventoryQuery;
};

export type RespError = {
    type: 'ERROR';
    data: any;
};

export default class InventoryDAO extends ZoomdataCore.DataAccessObject {
    private static instance: InventoryDAO;
    private observer: any;
    private subject: ReplaySubject<Inventory[]|RespError>;
    private _inventory: Inventory[];
    private config: TInventoryConfig;
    private traversal: any;

    constructor(config: TInventoryConfig) {
        super();
        if (InventoryDAO.instance) {
            throw new Error('Use InventoryDAO.getInstance() to get an instance.');
        }
        if (!config) {
            throw new Error('No config settings were provided');
        }
        this.observer = null;
        this.subject = null;
        this.config = config;
        this._inventory = [];
        InventoryDAO.instance = this;
    }

    public static getInstance(config?: TInventoryConfig): InventoryDAO {
        if (InventoryDAO.instance) {
            // if the config changed, trigger update from server
            if (!this.configsEqual(InventoryDAO.instance.config, config)) {
                InventoryDAO.instance.updateConfig(config);
            }

            return InventoryDAO.instance;
        } else {
            return new InventoryDAO(config);
        }
    }

    private static configsEqual(config: TInventoryConfig, otherConfig: TInventoryConfig): boolean {
        return _.isEqual(config, otherConfig);
    }

    updateQuery(config: TInventoryQuery): this {
        this.config.query = _.assign({}, this.config.query, config);
        return this;
    }

    updateConfig(config: TInventoryConfig) {
        this.config = config;
        this._getInventoryFromServer();
    }

    getInventoryObservable(): Observable<Inventory[]> {
        if (!this.subject) {
            this.subject = new ReplaySubject<Inventory[]>(1);
            this.observer = this.subject.asObservable();
        }

        return this.observer;
    }

    isEmpty(): boolean {
        return _.isEmpty(this._inventory);
    }

    refresh(): void {
        console.warn('Deprecated InventoryDAO.refresh, use InventoryDAO.fetchInventory()');
        this._getInventoryFromServer();
    }

    fetchInventory(): void {
        this._getInventoryFromServer();
    }

    fetchInventoryByTypeId(type: string, inventoryItemId: string): Promise<Inventory> {
        let headers = this.config.headers;

        let promise = new Promise<Inventory>((resolve, reject) => {

            traverson
                .from(`${this.config.uri}/${type}/${inventoryItemId}`)
                .json()
                .withRequestOptions({headers})
                .getResource((error: any, doc: Inventory) => {
                    if (error) {
                        this._handleServerError(error);
                        reject(error);
                    } else {
                        const {inventoryItemId} = doc;
                        const exisItemIdx = _.findIndex(this._inventory, {inventoryItemId});

                        if (exisItemIdx >= 0) {
                            this._inventory.splice(exisItemIdx, 1, doc);
                            this._handleServerResponse(this._inventory);
                        } else {
                            this.fetchInventory();
                        }

                        resolve(doc);
                    }

                });

        });

        return promise;
    }

    favoriteItem(id: string, type: string): Promise<void> {
        const item = _.find(this._inventory, {inventoryItemId: id, type: type});

        if (_.isUndefined(item)) {
            return this.fetchInventoryByTypeId(type, id)
                .then(() => {
                    this.favoriteItem(id, type);
                });
        } else {
            const href = getLinkHref(item, EInventoryLinkTypes.FAVORITE);
            return this._doFavorite(href, id, type);
        }

    }

    private _doFavorite(uri: string, id: string, type: string) {

        let message = {
            'inventoryItemId': id,
            'inventoryItemType': type
        };

        let headers = this.config.headers;

        let promise = new Promise<void>((resolve: any, reject: any) => {

            // send the request to the server
            traverson
                .from(uri)
                .json()
                .withRequestOptions({headers})
                .post(
                    message,
                    (error: string, response: any) => {
                        if (error) {
                            this._handleServerError(error);
                            reject(error);
                        } else {
                            if (response.statusCode !== 201) {
                                this._handleServerError(response.body);
                                reject(response.body);
                            } else {
                                this.fetchInventoryByTypeId(type, id);
                                resolve();
                            }
                        }
                    }
                );

        });
        return promise;

    }

    unfavoriteItem(id: string,  type: string): Promise<void> {
        const item = _.find(this._inventory, {inventoryItemId: id, type: type});

        if (_.isUndefined(item)) {
            return this.fetchInventoryByTypeId(type, id)
                .then(() => {
                    this.unfavoriteItem(id, type);
                });
        } else {
            const href = getLinkHref(item, EInventoryLinkTypes.UNFAVORITE);
            return this._doUnfavorite(href, id, type);
        }
    }

    private _doUnfavorite(url: string, id: string, type: string): Promise<void> {

        let headers = this.config.headers;

        let promise = new Promise<void>((resolve: any, reject: any) => {

            // send the request to the server
            traverson
                .from(url)
                .json()
                .withRequestOptions({headers})
                .delete(
                    (error: string, response: any) => {
                        if (error) {
                            this._handleServerError(error);
                            reject(error);
                        } else {
                            if (response.statusCode !== 204) {
                                this._handleServerError(response.body);
                                reject(response.body);
                            }
                            this.fetchInventoryByTypeId(type, id);
                            resolve();
                        }
                    }
                );

        });
        return promise;
    }

    public deleteItem(id: string) {
        // TODO: Tie to backend API instead
        this.TEMPORARY_deleteItem(id);
    }

    // TODO: Rmove this and do it on the server and trigger delete instead
    private TEMPORARY_deleteItem(id: string) {
        let inventory = this._inventory;
        let itemToDelete: number;
        for (let _i = 0; _i < inventory.length; _i++) {
            if (inventory[_i].inventoryItemId === id) {
                itemToDelete = _i;
            }
        }
        inventory.splice(itemToDelete, 1);
        this.subject.onNext(inventory);
    }

    private _getInventoryFromServer() {
        this.traversal = traverson
            .from(this.config.uri)
            .json()
            .withRequestOptions({
                headers: this.config.headers,
                qs: this._setupQueryParameters()
            })
            .getResource((error: string, document: any) => {
                if (error) {
                    this._handleServerError(error);
                } else {
                    this._handleServerResponse(document);
                }
            });
    }

    private _setupQueryParameters(): TInventoryQuery {
        let queryParameters: TInventoryQuery = {};
        const {query = {}} = this.config;

        _.assign(queryParameters, query);

        return queryParameters;
    }

    private _handleServerError(error: any) {
        if (this.subject) {
            this.subject.onNext({type: 'ERROR', data: error});
        }
    }

    private _handleServerResponse(inventory: any) {
        if (this.subject) {
            this._inventory = inventory;
            this.subject.onNext(inventory);
        }
    }

    public clearAll() {
        this._abortTraversal();
        this._clearsubject();
        this._clearObservers();
        this._clearInventory();
    }

    private _abortTraversal() {
        if (this.traversal) {
            this.traversal.abort();
        }
    }

    private _clearsubject() {
        if (this.subject) {
            this.subject.onCompleted();
            this.subject.dispose();
            this.subject = null;
        }
    }

    private _clearObservers() {
        this.observer = null;
    }

    private _clearInventory() {
        this._inventory = [];
    }

}

function getLinkHref(item: Inventory, rel: EInventoryLinkTypes): string {
    return <string>_.get(_.find(item.links, {rel}), 'href');
}

