/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import * as _ from 'lodash';
import * as ZoomdataCore from '../../../Core';
import { Observable, Observer, ReplaySubject } from 'rx';
import * as traverson from 'traverson';

import UserModel, { User } from '../models/UserModel';

export type UserInstanceConfig = {
    uri: string;
    USERS_URI: string;
    headers: any;
    accountId: string;
    currentUserId?: string;
};

export type UsersDoc = {
    data: Array<User>;
    links: any;
};

export type RespError = {
    type: 'ERROR';
    data: any;
};

export default class AccountUserDAO extends ZoomdataCore.DataAccessObject {

    private static instance: AccountUserDAO;
    private _users: Array<User>;
    private observer: Observable<User[]|RespError>;
    private subject: ReplaySubject<User[]|RespError>;
    private config: UserInstanceConfig;
    private traversal: any;

    constructor(config: UserInstanceConfig) {
        super();

        if (AccountUserDAO.instance) {
            throw new Error('Use AccountUserDAO.getInstance() to get an instance.');
        }

        this._users = [];
        this.config = config;
        this.observer = null;
        this.subject = null;

    }

    static getInstance(config: UserInstanceConfig): AccountUserDAO {
        if (!this.instance) {
            this.instance = new AccountUserDAO(config);
        }
        return this.instance;
    }

    fetchUsers() {
        const {uri, headers} = this.config;

        // TODO: Use zd hateoas manager
        this.traversal = traverson
            .from(uri)
            .json()
            .withRequestOptions({
                headers: headers
            })
            .getResource((error: any, doc: UsersDoc) => {
                if (this.subject) {
                    if (error) {
                        this._handleServerError(error);
                    } else {
                        this._users = doc.data;
                        this.subject.onNext(this._users);
                    }
                }
            });
    }

    fetchUserById(userId: string) {
        const {headers, USERS_URI} = this.config;

        if (!userId) {
            throw Error('"userId" should be defined');
        }

        // TODO: Use zd hateoas manager
        this.traversal = traverson
            .from(`${USERS_URI}/${userId}`)
            .json()
            .withRequestOptions({headers})
            .getResource((error: any, doc: User) => {
                if (error) {
                    this._handleServerError(error);
                } else {
                    const existUserIdx = _.findIndex(this._users, {userId: doc.userId});

                    existUserIdx >= 0 ?
                        this._users.splice(existUserIdx, 1, doc) :
                        this._users.push(doc);

                    this.subject.onNext(this._users);
                }
            });
    }

    fetchGroupPermissions(userId: string) {
        // TODO: Use zd hateoas manager
        const {uri, headers} = this.config;

        this.traversal = traverson
            .from(`api/users/${userId}/groups`)
            .json()
            .withRequestOptions({headers})
            .getResource((error: any, doc: any) => {

            });
    }

    updateUser(user: User) {
        // TODO: Use zd hateoas manager

        // this._users = _.reject(this._users, user);
        // this.subject.onNext(this._users);
    }

    deleteUser(user: User) {
        // TODO: Use zd hateoas manager

        // this._users = _.reject(this._users, user);
        // this.subject.onNext(this._users);
    }

    getUserObservable() {
        if (!this.subject) {
            this.subject = new ReplaySubject<User[]|RespError>(1);
            this.observer = this.subject.asObservable();
        }

        return this.observer;
    }

    refresh(userId?: string) {
        if (userId) {
            this.fetchUserById(userId);
        } else {
            this.fetchUsers();
        }
    }

    clearAll() {
        this._abortTraversal();
        this._clearSubjects();
        this._clearObservers();
    }

    private _handleServerError(error: string) {
        if (this.subject) {
            this.subject.onNext({type: 'ERROR', data: error});
        }
    }

    private _abortTraversal() {
        if (this.traversal) {
            this.traversal.abort();
            this.traversal = null;
        }
    }

    private _clearSubjects() {
        if (this.subject) {
            this.subject.onCompleted();
            this.subject.dispose();
            this.subject = null;
        }
    }

    private _clearObservers() {
        this.observer = null;
    }

}
