import UserModel from '../models/UserModel';

import * as ZoomdataCore from '../../../Core';
import * as Promise from 'promise';
import { Observable, Observer, BehaviorSubject } from 'rx';
import * as traverson from 'traverson';

export default class UserDAO extends ZoomdataCore.DataAccessObject {
    private static instance: UserDAO;
    private users: {[userId: string]: any};
    private observers: any;
    private subjects: any;

    constructor() {
        super();
        if (UserDAO.instance) {
            throw new Error('Use UserDAO.getInstance() to get an instance.');
        }
        this.users = [];
        this.observers = {};
        this.subjects = {};
        UserDAO.instance = this;
    }

    public static getInstance(): UserDAO {
        return UserDAO.instance ? UserDAO.instance : new UserDAO();
    }

    public getUserObservable(userId: string): Observable<UserModel> {
        if (!this.observers[userId]) {
            let user: UserModel = this.getOrCreateUser(userId);
            let subject = new BehaviorSubject(user);
            this.observers[userId] = subject.asObservable();
            this.subjects[userId] = subject;
        }

        return this.observers[userId];
    }

    public refresh(userId: string) {
        if (!this.subjects[userId] || !this.users[userId]) {
            throw new Error('The userId (' + userId + ') was not yet configured');
        }

        // TODO: Add code to redo the AJAX request
        this.subjects[userId].onNext(this.users[userId]);
    }

    private getOrCreateUser(userId: string): UserModel {
        let user: UserModel;
        if (this.users[userId]) {
            user = this.users[userId];
        } else {
            // TODO: Get this from the server
            let user: UserModel = new UserModel();
            // user.firstName = 'Blaine';
            // user.lastName = 'Donley';
            this.users[userId] = user;
        }
        return user;
    }

    public setUser(userId: string, user: UserModel) {
        this.subjects[userId].onNext(this.users[userId]);
    }

    public clearAll() {
        this.clearSubjects();
        this.clearObservers();
        this.clearUsers();
    }

    private clearSubjects() {
        for (let userId in this.subjects) {
            this.subjects[userId].onCompleted();
            this.subjects[userId].dispose();
        }
        this.subjects = {};
    }

    private clearObservers() {
        this.observers = {};
    }

    private clearUsers() {
        this.users = {};
    }

}