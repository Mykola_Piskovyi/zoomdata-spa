/*
 * Copyright (C) Zoomdata, Inc. 2012-2016. All rights reserved.
 */
'use strict';

import * as ZoomdataCore from '../../../Core';
import * as traverson from 'traverson';
import * as _ from 'lodash';
import { Observable, Observer, ReplaySubject } from 'rx';
import { UserPreference } from '../models/UserPreferenceModel';

const userPreferencesStorage = 'userPreferences';

export type UserPreferenceConfig = {
    uri: string;
    headers: any;
    user: any;
};

type RespError = {
    type: 'ERROR';
    data: any;
};

export default class UserPreferencesDAO extends ZoomdataCore.DataAccessObject {
    private static instance: UserPreferencesDAO;
    private traversal: any;
    private config: UserPreferenceConfig;
    private subject: ReplaySubject<UserPreference[]|RespError>;
    private observer: Observable<UserPreference[]|RespError>;
    private _userPreferences: Array<UserPreference>;
    private user: any;

    constructor(config: UserPreferenceConfig) {
        super();

        if (UserPreferencesDAO.instance) {
            throw new Error('Use UserPreferencesDAO.getInstance() to get an instance.');
        }

        this.config = config;
        this.subject = null;
        this.user = config.user;
        this._userPreferences = [];
    }

    public static getInstance(config: UserPreferenceConfig): UserPreferencesDAO {
        if (!this.instance) {
            this.instance = new UserPreferencesDAO(config);
        }
        return this.instance;
    }

    fetchUserPreferences(): void {
        const {uri, headers} = this.config;

        this.traversal = traverson
           .from(uri)
           .json()
           .withRequestOptions({headers})
           .getResource((error, response) => {
               if (error) {
                   this._handleServerError(error);
               } else {
                   this._userPreferences = response;
                   this.subject.onNext(this._userPreferences);
               }
           });
    }

    getUserPreferencesObservable() {
        if (!this.subject) {
            this.subject = new ReplaySubject<UserPreference[]|RespError>(1);
            this.observer = this.subject.asObservable();
        }

        return this.observer;
    }

    updateUserPreferences(preferences: any, key: string, version: string): void {
        if (_.isUndefined(key)) {
            throw new Error('"key" must be defined');
        }

        if (_.isUndefined(version)) {
            throw new Error('"version" must be defined');
        }

        const { uri, headers, user } = this.config;
        const prefByKey = _.find(this._userPreferences, {key});
        const userPreferences: UserPreference = _.assign({}, prefByKey, {
            accountId: user.accountId,
            userId: user.id,
            key: key,
            value: {
                version: version,
                value: preferences
            }
        });

        this.traversal = traverson
           .from(uri)
           .withRequestOptions({headers})
           .patch(userPreferences, (error: any, response: any = {}) => {
                const {status} = response;

                if (error || status !== 200) {
                    this._handleServerError(error || response);
                } else {
                    const data = JSON.parse(response.body);
                    const existPrefIdx = _.findIndex(this._userPreferences, {preferenceId: data.preferenceId});

                    existPrefIdx >= 0 ?
                       this._userPreferences.splice(existPrefIdx, 1, data) :
                       this._userPreferences.push(data);

                    this.subject.onNext(this._userPreferences);
                }
            });
    }

    deleteUserPreferenceById(preferenceId: string): void {
        const { uri, headers } = this.config;

        this.traversal = traverson
           .from(`${uri}/${preferenceId}`)
           .withRequestOptions({headers})
           .delete((error, response: any = {}) => {
               const {status} = response;
               if (error || status !== 204) {
                   this._handleServerError(error || response);
               } else {
                    const existPrefIdx = _.findIndex(this._userPreferences, {preferenceId});

                    if (existPrefIdx >= 0) {
                        this._userPreferences.splice(existPrefIdx, 1);
                        this.subject.onNext(this._userPreferences);
                    }
               }
           });
    }

    deleteUserPreferenceByKey(key: string): void {
        const prefByKey = _.find(this._userPreferences, {key});
        if (prefByKey) {
            this.deleteUserPreferenceById(prefByKey.preferenceId);
        }
    }

    clearAll() {
        this._abortTraversal();
        this._clearSubjects();
        this._clearObservers();
    }

    private _handleServerError(error: any) {
        if (this.subject) {
            this.subject.onNext({type: 'ERROR', data: error});
        }
    }

    private _abortTraversal() {
        if (this.traversal) {
            this.traversal.abort();
            this.traversal = null;
        }
    }

    private _clearSubjects() {
        if (this.subject) {
            this.subject.onCompleted();
            this.subject.dispose();
            this.subject = null;
        }
    }

    private _clearObservers() {
        this.observer = null;
    }
}
