/// <reference path='../../typings/globals/mocha/index.d.ts'/>
import * as Mocha from 'mocha';
import { expect } from 'chai';
import * as sinon from 'sinon';
import { find } from 'lodash';
import { Observable, Observer, IDisposable, AsyncSubject } from 'rx-lite';
import * as traverson from 'traverson';
import InventoryDAO, {ESortOrder, ESort, EIncludedItems, EInventoryLinkTypes} from '../../src/zoomdata-spa-api/daos/InventoryDAO';
import { InventoryItemModel } from '../../src/zoomdata-spa-api/models/InventoryItemModel';
import * as dotenv from 'dotenv';

// retrieve config values from .env
let config: any = dotenv.config();
const uri: string = config.API_URI;
const headers: any = JSON.parse(config.API_HEADERS);

describe('InventoryDAO System Tests', () => {
    let mainSandbox: any;
    let sandbox: any;
    let nextStub: any;
    let errorStub: any;
    let completeStub: any;
    let traversonStub: any;
    let main: InventoryDAO;
    let subscription: IDisposable;

    beforeEach(function() {
        sandbox = sinon.sandbox.create();
        nextStub = sandbox.stub();
        errorStub = sandbox.stub();
        completeStub = sandbox.stub();
    });

    afterEach(function() {
        if (subscription) {
            subscription.dispose();
        }

        sandbox.restore();
        main.clearAll();
    });

    describe('Inventory Queries', () => {

        let nextMock: any;
        let nextSpy: any;
        let observable: any;

        let config: any = {
            uri: uri,
            headers: headers,
            query: {
                // accountId: '578e527b3004bfd0c66b7c88',
                // nameQuery: 'Dashboard',
                // includeItems: EIncludedItems.ALL,
                // favorites: true,
                // sort: ESort.MODIFIED,
                // sortOrder: ESortOrder.DESC
            }
        };

        beforeEach(function() {
            main = InventoryDAO.getInstance(config);
            observable = main.getInventoryObservable();
        });

        it('can get all items', function(done: any) {
            // this.timeout(5000);

            let count = 0;
            nextMock = function(data: any) {
                expect(data.length).to.be.gt(0);
                done();
            };
            nextSpy = sandbox.spy(nextMock);

            config.query = {
                includeItems: EIncludedItems.ALL
            };

            subscription = observable.subscribe(
                nextSpy,
                errorStub,
                completeStub
            );

        });

        it('can query favorites', function(done: any) {
            // this.timeout(5000);

            nextMock = function(data: any) {
                expect(data.length).to.be.gt(0);
                done();
            };
            nextSpy = sandbox.spy(nextMock);

            config.query = {
                favorites: true
            };

            subscription = observable.subscribe(
                nextSpy,
                errorStub,
                completeStub
            );

        });

        it('can query shared items', function(done: any) {
            // this.timeout(5000);

            nextMock = function(data: any) {
                expect(data.length).to.be.gt(0);
                done();
            };
            nextSpy = sandbox.spy(nextMock);

            config.query = {
                includeItems: EIncludedItems.SHARED_WITH_ME
            };

            subscription = observable.subscribe(
                nextSpy,
                errorStub,
                completeStub
            );

        });

        it('query accountId');
        it('query name');
        it('query includeItems');
        it('query favorites');
        it('query sort');
        it('query sort order');
    });

    describe('Favoriting Queries', () => {

        let observable: any;

        it('can favorite', function(done: any) {
            // this.timeout(5000);

            let config: any = {
                uri: uri,
                headers: headers,
                query: {
                    // accountId: '578e527b3004bfd0c66b7c88',
                    // nameQuery: 'Dashboard',
                    includeItems: EIncludedItems.ALL,
                    // favorites: false,
                    // sort: ESort.MODIFIED,
                    // sortOrder: ESortOrder.DESC
                }
            };

            main = InventoryDAO.getInstance(config);
            observable = main.getInventoryObservable();

            let count = 0;

            let nextMock = function(data: InventoryItemModel[]) {
                count++;
                if (1 === count) {
                    expect(data.length).to.be.gt(1);
                    main.favoriteItem(data[1].inventoryItemId, data[1].type)
                    .then(
                        function(val: any) { },
                        function(reason: any) { }
                    );
                } else if (2 === count) {
                    expect(data[1].favorite).to.be.true;
                    subscription.dispose();
                    done();
                }
            };
            let nextSpy = sandbox.spy(nextMock);

            subscription = observable.subscribe(
                nextMock,
                errorStub,
                completeStub
            );

        });

        it('can unfavorite', function(done: any) {
            // this.timeout(5000);

            let config: any = {
                uri: uri,
                headers: headers,
                query: {
                    // accountId: '578e527b3004bfd0c66b7c88',
                    // nameQuery: 'Dashboard',
                    includeItems: EIncludedItems.ALL,
                    // favorites: false,
                    // sort: ESort.MODIFIED,
                    // sortOrder: ESortOrder.DESC
                }
            };

            main = InventoryDAO.getInstance(config);
            observable = main.getInventoryObservable();

            let count = 0;

            let nextMock = function(data: InventoryItemModel[]) {
                count++;
                if (1 === count) {
                    // make it favorite first
                    expect(data.length).to.be.gt(0);
                    main.favoriteItem(data[0].inventoryItemId, data[0].type)
                    .then(
                        function(val: any) {},
                        function(reason: any) {}
                    );
                } else if (2 === count) {
                    expect(data.length).to.be.gt(0);
                    let link = find(data[0].links, {rel: EInventoryLinkTypes.UNFAVORITE});
                    main.unfavoriteItem(data[0].inventoryItemId, data[0].type)
                    .then(
                        function(val: any) {},
                        function(reason: any) {}
                    );
                } else if (3 === count) {
                    expect(data[0].favorite).to.be.false;
                    subscription.dispose();
                    done();
                }
            };
            let nextSpy = sandbox.spy(nextMock);

            subscription = observable.subscribe(
                nextMock,
                errorStub,
                completeStub
            );

        });

    });

    it('can subscribe multiple times (subscribe - subscribe)');
    it('can subscribe and receive updates');
    it('can subscribe multiple and receive updates');
    it('CAN HANDLE A VARIETY OF QUERY PARAMETERS');

});
