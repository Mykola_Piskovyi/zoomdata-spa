import * as Mocha from 'mocha';
import { expect } from 'chai';
import * as sinon from 'sinon';
import { find } from 'lodash';
import { Observable, Observer, IDisposable } from 'rx';
import * as traverson from 'traverson';

import {UserPreferencesDAO} from '../../../index';

describe('UserPreferencesDAO Unit Tests: ', () => {
    let main: UserPreferencesDAO;
    let sandbox: any;
    let config: any;
    let user: any;

    let onNextStub: any;
    let onErrorStub: any;
    let onCompleteStub: any;
    let handleServerErrorStub: any;

    const createNewPref = (value: any, id: string, key: string, version: string = '1') => {
        return {
            accountId: user.accountId,
            userId: user.id,
            key: key,
            preferenceId: id,
            value: {version, value}
        };
    };

    beforeEach(() => {

        user = {
            accountId: 'accountId',
            id: 'userId'
        };
        config = {
            uri: '',
            headers: {},
            user: user
        };

        main = UserPreferencesDAO.getInstance(config);

        sandbox = sinon.sandbox.create();
        onNextStub = sandbox.stub();
        onErrorStub = sandbox.stub();
        onCompleteStub = sandbox.stub();
        handleServerErrorStub = sandbox.stub(main, '_handleServerError', main['_handleServerError']);
    });

    afterEach(() => {
        main.clearAll();
        sandbox.restore();
    });

    it('should be defined and has _userPreferences', () => {
        expect(main).to.exist;
        expect(main['_userPreferences']).to.eql([]);
    });

    it('should NOT init twice', () => {
        expect(() => {
            new UserPreferencesDAO(config);
        }).to.throw('Use UserPreferencesDAO.getInstance() to get an instance.');
    });

    it('can getUserPreferencesObservable()', () => {
        let observable: any = main.getUserPreferencesObservable();
        expect(observable).to.exist;
    });

    describe('Test fetchUserPreferences(): ', () => {

        let handler: any;
        let subscription: any;

        const createHandlerGetResource = (getResource: Function) => {
            return {
                json: () => {
                    return {
                        withRequestOptions: () => ({getResource})
                    };
                }
            };
        };

        beforeEach(() => {
            subscription = main
                .getUserPreferencesObservable()
                .subscribe(onNextStub, onErrorStub, onCompleteStub);
        });

        afterEach(() => {
            subscription.dispose();
        });

        it('should call onNext() when data is received', () => {
            let data: any = [];

            handler = createHandlerGetResource((callback: any) => {
                callback(null, data);
            });

            sandbox.stub(traverson, 'from').returns(handler);
            main.fetchUserPreferences();

            expect(onNextStub.callCount).to.be.equal(1);
            expect(onNextStub.getCall(0).args).to.eql([data]);
            expect(main['_userPreferences']).to.eql(data);
        });

        it('should call onNext() with {type: "ERROR"} for error response', () => {
            let error: any = 'some fail text';
            handler = createHandlerGetResource((callback: any) => {
                callback(error, null);
            });

            sandbox.stub(traverson, 'from').returns(handler);
            main.fetchUserPreferences();

            expect(handleServerErrorStub.called).to.be.true;
            expect(onNextStub.callCount).to.be.equal(1);
            expect(onNextStub.getCall(0).args).to.eql([{
                type: 'ERROR', data: error
            }]);
        });

    });

    describe('Test updateUserPreferences(...args): ', () => {

        let handler: any;
        let prefs: any;
        let key: string;
        let version: string;
        let subscription: any;
        let _userPreferences: any;

        const createHandlerPatch = (patch: Function) => {
            return {
                withRequestOptions: () => ({patch})
            };
        };

        beforeEach(() => {
            key = 'key1';
            version = '1';
            subscription = main
                .getUserPreferencesObservable()
                .subscribe(onNextStub, onErrorStub, onCompleteStub);
        });

        afterEach(() => {
            subscription.dispose();
        });

        it('should throw Error with no "key" argument', () => {
            expect(() => {
                main.updateUserPreferences([], undefined, undefined);
            }).to.throw('"key" must be defined');
        });

        it('should throw Error with no "version" argument', () => {
            expect(() => {
                main.updateUserPreferences([], 'someKey', undefined);
            }).to.throw('"version" must be defined');
        });

        it('should patch with correct UserPrefs object', () => {
            prefs = {some: true};
            const patchStub = sandbox.stub();
            handler = createHandlerPatch(patchStub);

            sandbox.stub(traverson, 'from').returns(handler);

            main.updateUserPreferences(prefs, key, version);

            const expectedData = {
                accountId: user.accountId,
                userId: user.id,
                key: key,
                value: {
                    version: version,
                    value: prefs
                }
            };

            expect(patchStub.called).to.be.true;
            expect(patchStub.getCall(0).args[0]).to.eql(expectedData);

        });

        it('should call onNext() when data is received', () => {
            prefs = {some: true};
            let newPrefWithId: any = createNewPref(prefs, '1', key);
            handler = createHandlerPatch((prefs: any, callback: Function) => {
                callback(null, {status: 200, body: JSON.stringify(newPrefWithId)});
            });

            sandbox.stub(traverson, 'from').returns(handler);
            main.updateUserPreferences(prefs, key, version);

            _userPreferences = main['_userPreferences'];

            expect(onNextStub.callCount).to.be.equal(1);
            expect(onNextStub.getCall(0).args).to.be.eql([_userPreferences]);
        });

        it('should call onNext() with updated (replaced) private preferences data', () => {
            let newPrefWithId: any = createNewPref({some: 'NEW_VALUE'}, '1', key);

            main['_userPreferences'] = [
                createNewPref({some: 'OLD_VALUE'}, '1', key),
                createNewPref({qqqq: false}, '2', 'key2')
            ];

            handler = createHandlerPatch((prefs: any, callback: Function) => {
                callback(null, {status: 200, body: JSON.stringify(newPrefWithId)});
            });

            sandbox.stub(traverson, 'from').returns(handler);
            main.updateUserPreferences(prefs, key, version);
            _userPreferences = main['_userPreferences'];

            expect(onNextStub.callCount).to.be.equal(1);
            expect(onNextStub.getCall(0).args[0].length).to.be.equal(2);
            expect(onNextStub.getCall(0).args[0]).to.be.eql(_userPreferences);
        });

        it('should call onNext() with updated (add new one) private preferences data', () => {
            let newPrefWithId: any = createNewPref({some: 'NEW_VALUE'}, '3', 'key3');

            main['_userPreferences'] = [
                createNewPref({some: 'OLD_VALUE'}, '1', key),
                createNewPref({qqqq: false}, '2', 'key2')
            ];

            handler = createHandlerPatch((prefs: any, callback: Function) => {
                callback(null, {status: 200, body: JSON.stringify(newPrefWithId)});
            });

            sandbox.stub(traverson, 'from').returns(handler);
            main.updateUserPreferences(prefs, key, version);
            _userPreferences = main['_userPreferences'];

            expect(onNextStub.callCount).to.be.equal(1);
            expect(onNextStub.getCall(0).args[0].length).to.be.equal(3);
            expect(onNextStub.getCall(0).args[0]).to.be.eql(_userPreferences);
        });

        it('should call onNext() with {type: "ERROR"} for error response', () => {
            let newPrefWithId: any = createNewPref({some: 'NEW_VALUE'}, '3', 'key3');
            let error: any = {status: 400, body: {}};

            handler = createHandlerPatch((prefs: any, callback: Function) => {
                callback(null, error);
            });

            sandbox.stub(traverson, 'from').returns(handler);
            main.updateUserPreferences({some: 'NEW_VALUE'}, key, version);
            _userPreferences = main['_userPreferences'];

            expect(handleServerErrorStub.called).to.be.true;
            expect(onNextStub.callCount).to.be.equal(1);
            expect(onNextStub.getCall(0).args).to.eql([{
                type: 'ERROR', data: error
            }]);
        });

    });

    describe('Test deleteUserPreferenceById(id): ', () => {
        let handler: any;
        let prefs: any;
        let key: string;
        let subscription: any;
        let _userPreferences: any;

        const createHandlerDelete = (deleteFunc: Function) => {
            return {
                withRequestOptions: () => ({delete: deleteFunc})
            };
        };

        beforeEach(() => {
            key = 'key1';
            main['_userPreferences'] = [
                createNewPref({qqqq: false}, '1', 'key1'),
                createNewPref({ssss: true}, '2', 'key2'),
                createNewPref(['a', 'b'], '3', 'key3')
            ];
            subscription = main
                .getUserPreferencesObservable()
                .subscribe(onNextStub, onErrorStub, onCompleteStub);
        });

        afterEach(() => {
            subscription.dispose();
        });

        it('should call onNext() success delete with existed preferenceId', () => {
            handler = createHandlerDelete((callback: any) => {
                callback(null, {status: 204});
            });

            sandbox.stub(traverson, 'from').returns(handler);

            let preferenceId = '1';

            main.deleteUserPreferenceById(preferenceId);
            _userPreferences = main['_userPreferences'];

            const existInPrivateStore = find(_userPreferences, {preferenceId});

            expect(onNextStub.callCount).to.be.equal(1);
            expect(onNextStub.getCall(0).args[0]).to.eql(_userPreferences);
            expect(existInPrivateStore).to.equal(undefined);
        });

        it('should NOT call onNext() success delete with not existed preferenceId in store', () => {
            handler = createHandlerDelete((callback: any) => {
                callback(null, {status: 204});
            });

            sandbox.stub(traverson, 'from').returns(handler);

            let preferenceId = '100';

            main.deleteUserPreferenceById(preferenceId);
            _userPreferences = main['_userPreferences'];

            const existInPrivateStore = find(_userPreferences, {preferenceId});

            expect(onNextStub.called).to.be.false;
            expect(existInPrivateStore).to.equal(undefined);
        });

        it('should call onNext() with {type: "ERROR"} for error response', () => {
            let error: any = {status: 400};
            handler = createHandlerDelete((callback: any) => {
                callback(null, {status: 400});
            });

            sandbox.stub(traverson, 'from').returns(handler);
            main.deleteUserPreferenceById('9');

            expect(handleServerErrorStub.called).to.be.true;
            expect(onNextStub.callCount).to.be.equal(1);
            expect(onNextStub.getCall(0).args).to.eql([{
                type: 'ERROR', data: error
            }]);
        });

    });

    describe('Test deleteUserPreferenceByKey(key): ', () => {

        beforeEach(() => {
            main['_userPreferences'] = [
                createNewPref({qqqq: false}, '1', 'key1'),
                createNewPref({ssss: true}, '2', 'key2')
            ];
        });

        it('should call deleteUserPreferenceById(id) for existed key in store', () => {
            let deteleByIdStub = sandbox.stub(main, 'deleteUserPreferenceById');

            main.deleteUserPreferenceByKey('key1');

            expect(deteleByIdStub.called).to.be.true;
            expect(deteleByIdStub.getCall(0).args[0]).to.eql('1');
        });

        it('should do NOTHING for not existed key in store', () => {
            let deteleByIdStub = sandbox.stub(main, 'deleteUserPreferenceById');

            main.deleteUserPreferenceByKey('key3');

            expect(deteleByIdStub.called).to.be.false;
        });

    });

});
