import * as Mocha from 'mocha';
import { expect } from 'chai';
import * as sinon from 'sinon';
import { find, assign, includes } from 'lodash';
import { Observable, Observer, IDisposable } from 'rx';
import * as traverson from 'traverson';
import {InventoryDAO, EInventoryLinkTypes} from '../../../index';
import { InventoryItemModel, Inventory } from '../../../src/models/InventoryItemModel';

describe('InventoryDAO Unit Tests', () => {
    let main: InventoryDAO;
    let sandbox: any;
    let nextStub: any;
    let errorStub: any;
    let completeStub: any;
    let handleServerErrorStub: any;
    let traversonStub: any;
    let config: any = {
            uri: '',
            headers: {},
            query: {}
        };

    before(function() {});

    after(function () {});

    beforeEach(function() {
        sandbox = sinon.sandbox.create();
        nextStub = sandbox.stub();
        errorStub = sandbox.stub();
        completeStub = sandbox.stub();
        main = InventoryDAO.getInstance(config);
        handleServerErrorStub = sandbox.stub(main, '_handleServerError', main['_handleServerError']);
    });

    afterEach(function() {
        main.clearAll();
        sandbox.restore();
    });

    it('can instantiate', () => {
        expect(main).to.exist;
    });

    it('can\'t instantiate twice', () => {
        expect(() => {
            new InventoryDAO(config);
        }).to.throw('Use InventoryDAO.getInstance() to get an instance.');
    });

    it('can call onNext() when data is received', function() {

        traversonStub = sandbox.stub();
        let handler = {
            json: () => {
                return {
                    withRequestOptions: () => {
                        return {
                            getResource: function(error: string, document: any) {
                                nextStub();
                            }
                        };
                    }
                };
            }
        };
        sandbox.stub(traverson, 'from').returns(handler);

        let observable: any = main.getInventoryObservable();
        let subscription: any = observable.subscribe(
            nextStub,
            errorStub,
            completeStub
        );

        main.fetchInventory();

        expect(nextStub.callCount).to.be.equal(1);
        subscription.dispose();

    });

    it('can fetchInventory', function(done) {

        let count = 0;
        let nextMock = function(data: any) {
            count++;
            if (count === 1) {
                main.fetchInventory();
            } else if (count === 2) {
                expect(nextSpy.callCount).to.be.equal(2);
                done();
            }
        };
        let nextSpy = sandbox.spy(nextMock);

        traversonStub = sandbox.stub();
        let handler = {
            json: () => {
                return {
                    withRequestOptions: () => {
                        return {
                            getResource: function(handler: any) {
                                handler(null, []);
                            }
                        };
                    }
                };
            }
        };
        sandbox.stub(traverson, 'from').returns(handler);

        let observable: any = main.getInventoryObservable();
        let subscription: any = observable.subscribe(
            nextSpy,
            errorStub,
            completeStub
        );

        main.fetchInventory();

    });

    it('should return same observable and call onNext with last subject values for new subscribe: ', () => {
        const docItem1 = <Inventory>{inventoryItemId: '1'};
        const docItem2 = <Inventory>{inventoryItemId: '2'};
        const docItem3 = <Inventory>{inventoryItemId: '3'};
        const docItem4 = <Inventory>{inventoryItemId: '4'};
        const data = [docItem1, docItem2, docItem3, docItem4];

        main.getInventoryObservable();
        main['subject'].onNext(data);

        const observable: any = main.getInventoryObservable();
        observable.subscribe(nextStub);

        expect(nextStub.called).to.be.true;
        expect(nextStub.getCall(0).args[0]).to.eql(data);

    });

    describe('Test fetchInventoryByTypeId(...): ', () => {

        let handler: any;
        let docItem: Inventory;
        let subscription: any;
        let _inventory: any;
        const getFetchItemHandler = (getResource: Function) => {
            return {
                json: () => {
                    return {
                        withRequestOptions: () => ({getResource})
                    };
                }
            };
        };

        beforeEach(() => {
            docItem = <Inventory>{
                inventoryItemId: '1'
            };
        });

        afterEach(() => {
            subscription.dispose();
        });

        it('can fetchInventoryByTypeId and then fetchInventory() if not exist', () => {

            const fetchItemsStub = sandbox.stub(main, 'fetchInventory');

            handler = getFetchItemHandler((callback: any) => {
                callback(null, docItem);
            });
            sandbox.stub(traverson, 'from').returns(handler);

            subscription = main
                .getInventoryObservable()
                .subscribe(nextStub, errorStub, completeStub);

            main['_inventory'] = [];

            main.fetchInventoryByTypeId('DASHBOARD', '1');

            expect(fetchItemsStub.called).to.be.true;
            expect(includes(_inventory, docItem)).to.be.false;
        });

        it('should update existed item', () => {
            const updatedItem = assign({}, docItem, {name: 'UPDATED'});
            handler = getFetchItemHandler((callback: any) => {
                callback(null, updatedItem);
            });
            sandbox.stub(traverson, 'from').returns(handler);

            main['_inventory'] = [docItem];
            _inventory = main['_inventory'];

            subscription = main
                .getInventoryObservable()
                .subscribe(nextStub, errorStub, completeStub);

            main.fetchInventoryByTypeId('DASHBOARD', '1');

            expect(nextStub.called).to.be.true;
            expect(nextStub.getCall(0).args[0]).to.eql([updatedItem]);
        });

        it('should call onNext() with {type: "ERROR"} for error response', () => {
            const error = {text: 'some error'};
            handler = getFetchItemHandler((callback: any) => {
                callback(error, null);
            });
            sandbox.stub(traverson, 'from').returns(handler);

            subscription = main
                .getInventoryObservable()
                .subscribe(nextStub, errorStub, completeStub);

            main.fetchInventoryByTypeId('DASHBOARD', '1');

            expect(handleServerErrorStub.called).to.be.true;
            expect(nextStub.called).to.be.true;
            expect(nextStub.getCall(0).args[0]).to.eql({type: 'ERROR', data: error});
        });
    });

    it('can favorite an item', function(done) {

        const itemId = '1234567';
        const itemType = 'DASHBOARD';
        let data = [{
            inventoryItemId: itemId,
            type: itemType,
            favorite: false,
            links: [{
                rel: EInventoryLinkTypes.FAVORITE,
                href: 'htpp://localhost...'
            }]
        }];
        let favoriteResponse = {
            statusCode: 201
        };

        let count = 0;
        let nextMock = function(data: any) {
            count++;
            if (1 === count) {
                expect(data.length).to.be.gt(0);
                main.favoriteItem(data[0].inventoryItemId, data[0].type).then(
                    () => {},
                    () => {}
                );
            } else if (2 === count) {
                expect(data[0].favorite).to.be.true;
                done();
            }
        };
        let nextSpy = sandbox.spy(nextMock);

        traversonStub = sandbox.stub();

        let handler = {
            json: () => {
                return {
                    withRequestOptions: () => {
                        return {
                            post: function(message: any, handler: any) {
                                data[0].favorite = true;
                                handler(null, favoriteResponse);
                            },
                            getResource: function(handler: any) {
                                handler(null, data);
                            }
                        };
                    }
                };
            }
        };
        sandbox.stub(traverson, 'from').returns(handler);

        let observable: any = main.getInventoryObservable();
        let subscription: any = observable.subscribe(
            nextMock,
            errorStub,
            completeStub
        );

        main.fetchInventory();

        subscription.dispose();

    });

    it('can unfavorite an item', function(done) {

        const itemId = '1234567';
        const itemType = 'DASHBOARD';
        let data = [{
            inventoryItemId: itemId,
            type: itemType,
            favorite: false,
            links: [{
                rel: EInventoryLinkTypes.UNFAVORITE,
                href: 'htpp://localhost...'
            }]
        }];

        let count = 0;
        let nextMock = function(data: Inventory[]) {
            count++;
            if (1 === count) {
                expect(data.length).to.be.gt(0);
                main.unfavoriteItem(itemId, itemType).then(
                    () => {},
                    () => {}
                );
            } else if (2 === count) {
                expect(data[0].favorite).to.be.false;
                done();
            }
        };
        let nextSpy = sandbox.spy(nextMock);

        traversonStub = sandbox.stub();

        let handler = {
            json: () => {
                return {
                    withRequestOptions: () => {
                        return {
                            delete: function(handler: any) {
                                handler(null, {statusCode: 204});
                            },
                            getResource: function(handler: any) {
                                handler(null, data);
                            }
                        };
                    }
                };
            }
        };
        sandbox.stub(traverson, 'from').returns(handler);

        let observable: any = main.getInventoryObservable();
        let subscription: any = observable.subscribe(
            nextMock,
            errorStub,
            completeStub
        );

        main.fetchInventory();

        subscription.dispose();

    });

    it('can delete an item', function(done) {

        let data = [{
            inventoryItemId: '1234567',
            favorite: false
        }];

        let count = 0;
        let nextMock = function(data: any) {
            count++;
            if (1 === count) {
                expect(data.length).to.be.gt(0);
                main.deleteItem(data[0].inventoryItemId);
            } else if (2 === count) {
                expect(data.length).to.be.equal(0);
                done();
            }
        };
        let nextSpy = sandbox.spy(nextMock);

        traversonStub = sandbox.stub();

        let handler = {
            json: () => {
                return {
                    withRequestOptions: () => {
                        return {
                            getResource: function(handler: any) {
                                handler(null, data);
                            }
                        };
                    }
                };
            }
        };
        sandbox.stub(traverson, 'from').returns(handler);

        let observable: any = main.getInventoryObservable();
        let subscription: any = observable.subscribe(
            nextMock,
            errorStub,
            completeStub
        );

        main.fetchInventory();

        subscription.dispose();

    });

    it('can update config', function(done) {

        let data = [{
            inventoryItemId: '1234567',
            favorite: false
        }];

        let newConfig: any = {
            uri: '',
            headers: {
                something: 'else'
            },
            query: {
                favorites: true
            }
        };

        let count = 0;
        let nextMock = function(data: any) {
            count++;
            if (1 === count) {
                main.updateConfig(newConfig);
            } else if (2 === count) {
                done();
            }
        };
        let nextSpy = sandbox.spy(nextMock);

        traversonStub = sandbox.stub();

        let settingCount = 0;
        let handler = {
            json: () => {
                return {
                    withRequestOptions: (values: any) => {
                        settingCount++;
                        if (2 === settingCount) {
                            // make sure the settings were updated
                            // on a subsequent request
                            expect(values.headers.something).to.be.equal('else');
                            expect(values.qs.favorites).to.be.equal(true);
                        }
                        return {
                            getResource: function(handler: any) {
                                handler(null, data);
                            }
                        };
                    }
                };
            }
        };
        sandbox.stub(traverson, 'from').returns(handler);

        let observable: any = main.getInventoryObservable();
        let subscription: any = observable.subscribe(
            nextMock,
            errorStub,
            completeStub
        );

        main.fetchInventory();

        subscription.dispose();

    });

    it('can clear the DAO', function(done: any) {

        let count = 0;
        let nextMock = function(data: any) {
            count++;
            if (1 === count) {
                expect(nextSpy.callCount).to.be.equal(1);
                main.clearAll();

                let newSubscription: any = main.getInventoryObservable().subscribe(
                    nextSpy,
                    errorStub,
                    completeStub
                );

                main.fetchInventory();

            } else if (2 === count) {
                expect(nextSpy.callCount).to.be.equal(2);
                done();
            }
        };
        let nextSpy = sandbox.spy(nextMock);

        traversonStub = sandbox.stub();
        let handler = {
            json: () => {
                return {
                    withRequestOptions: () => {
                        return {
                            getResource: function(handler: any) {
                                handler(null, {});
                            }
                        };
                    }
                };
            }
        };
        sandbox.stub(traverson, 'from').returns(handler);

        let observable: any = main.getInventoryObservable();
        let subscription: any = observable.subscribe(
            nextSpy,
            errorStub,
            completeStub
        );

        main.fetchInventory();

    });

});
