import * as Mocha from 'mocha';
import { expect } from 'chai';
import * as sinon from 'sinon';
import { Observable, Observer, IDisposable } from 'rx';
import UserDAO from '../../../src/daos/UserDAO';
import UserModel from '../../../src/models/UserModel';

describe('UserDAO Unit Tests', () => {
    let sandbox: any;
    let nextStub: any;
    let errorStub: any;
    let completeStub: any;

    beforeEach(function() {
        sandbox = sinon.sandbox.create();
        nextStub = sandbox.stub();
        errorStub = sandbox.stub();
        completeStub = sandbox.stub();
    });

    afterEach(function() {
        // if (subscription) {
        //    subscription.dispose();
        // }
    });

    it('can instantiate', () => {
        let main: UserDAO = new UserDAO();
        expect(main).to.exist;
    });

    it('can\'t instantiate twice', () => {
        expect(() => {
            new UserDAO();
        }).to.throw('Use UserDAO.getInstance() to get an instance.');
    });

    it('can instantiate through factory', () => {
        let main: UserDAO = UserDAO.getInstance();
        expect(main).to.exist;
    });

    it('can subscribe', () => {
        let observable: any;
        let subscription: IDisposable;
        const userId = 'TEST';
        let main: UserDAO = UserDAO.getInstance();
        observable = main.getUserObservable(userId);
        subscription = observable.subscribe(
            nextStub,
            errorStub,
            completeStub
        );

        // main.load(userId);
        // observable.connect();

        expect(nextStub.called).to.be.true;
        subscription.dispose();
    });

    it('can subscribe multiple times (subscribe - subscribe)', () => {
        let observable: any;
        let subscription: IDisposable;
        let otherSubscription: IDisposable;
        const userId = 'TEST';
        let main: UserDAO = UserDAO.getInstance();
        let otherNextStub: any = sandbox.stub();
        observable = main.getUserObservable(userId);

        subscription = observable.subscribe(
            nextStub,
            errorStub,
            completeStub
        );

        otherSubscription = observable.subscribe(
            otherNextStub,
            errorStub,
            completeStub
        );

        expect(nextStub.called).to.be.true;
        expect(otherNextStub.called).to.be.true;

        subscription.dispose();
        otherSubscription.dispose();
    });

    it('can subscribe and receive updates', () => {
        let observable: any;
        let subscription: IDisposable;
        const userId = 'TEST';
        let main: UserDAO = UserDAO.getInstance();
        observable = main.getUserObservable(userId);
        subscription = observable.subscribe(
            nextStub,
            errorStub,
            completeStub
        );

        const updatedUser: UserModel = new UserModel();
        main.setUser(userId, updatedUser);

        expect(nextStub.callCount).to.be.equal(2);
        subscription.dispose();
    });

    it('can subscribe multiple and receive updates', () => {
        let observable: any;
        let subscription: IDisposable;
        let otherSubscription: IDisposable;
        const userId = 'TEST';
        let main: UserDAO = UserDAO.getInstance();
        let otherNextStub: any = sandbox.stub();
        observable = main.getUserObservable(userId);

        subscription = observable.subscribe(
            nextStub,
            errorStub,
            completeStub
        );

        otherSubscription = observable.subscribe(
            otherNextStub,
            errorStub,
            completeStub
        );

        const updatedUser: UserModel = new UserModel();
        main.setUser(userId, updatedUser);

        expect(nextStub.callCount).to.be.equal(2);
        expect(otherNextStub.callCount).to.be.equal(2);

        subscription.dispose();
        otherSubscription.dispose();
    });


});
