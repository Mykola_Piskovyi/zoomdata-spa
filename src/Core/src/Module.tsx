// import { ModuleRouter } from "./ModuleRouter";

export abstract class Module {
    private _isStarted = false;
    options: any;
    // router: ModuleRouter;

    constructor(options?: any) {
        this.options = options;
    }

    initialize(): void {}

    beforeStart(): void {}

    start(): void {
        this.beforeStart();
        this._isStarted = true;
    }

    beforeStop(): void {}

    stop(): void {
        this.beforeStop();
        this._isStarted = false;
    }

    isStarted(): boolean {
        return this._isStarted;
    }

    updateEl(newEl: string): void {
        // destory existing views?
        // trigger new route?
    }

    updateNavEl(newNavEl: string): void {
        // destory existing views?
        // trigger new route?
    }
}