import { Router } from './Router';
import History from './BrowserHistory';

export abstract class ModuleRouter extends Router {
    manuallyRoute(path: string, options?: any) {

        if (!this.routes) {
            throw new Error('No routes defined in Module Router');
        }

        // execute code corresponding to route path
        History.loadUrl(path);
    }
}
