import MVC from './MVC';
import * as Backbone from 'backbone';

export abstract class Model extends Backbone.Model {}
