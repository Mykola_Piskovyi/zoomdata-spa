import MVC from './MVC';
import * as Backbone from 'backbone';

export abstract class Controller extends MVC.View<Backbone.Model> {
    constructor(options?: any) {
        super(options);
    }
}