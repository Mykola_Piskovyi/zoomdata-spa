import MVC from './MVC';
import { Model } from './Model';
import * as Backbone from 'backbone';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

export type TComponent = React.ComponentClass<any>|React.SFC<any>;

export abstract class View extends MVC.View<Backbone.Model> {
    constructor(options?: any) {
        super(options);
    }

    abstract render(data?: any): View;

    combineModelAndEventHandlers(model: any, eventHandlers: any): any {
        // return View.completeAssign(model, eventHandlers);
        // return (Object as any).assign(eventHandlers, model);
        return View.merge(model, eventHandlers);
    }

    static merge(source1: any, source2: any): any {
        for (let key in source2) {
            if (source2.hasOwnProperty(key)) source1[key] = source2[key];
        }
        return source1;
    }

    static completeAssign(target: any, source: any): any {
        let descriptors: any = Object.keys(source).reduce((descriptors: any, key: any) => {
            descriptors[key] = Object.getOwnPropertyDescriptor(source, key);
            return descriptors;
        }, {});
        let symbols = (Object as any).getOwnPropertySymbols(source);
        for (let index = 0; index < symbols.length; index++) {
            View.copyDescriptors(symbols[index], source, descriptors);
        }
        (Object as any).defineProperties(target, descriptors);
        return target;
    }

    static copyDescriptors(sym: any, source: any, descriptors: any) {
        let descriptor = Object.getOwnPropertyDescriptor(source, sym);
        if (descriptor.enumerable) {
            descriptors[sym] = descriptor;
        }
    }

    renderComponent(reactComponent: TComponent, props: any): View {
        ReactDOM.render(
            React.createElement(reactComponent as React.ComponentClass<any>, props),
            this.el
        );

        return this;
    }

    remove() {
        ReactDOM.unmountComponentAtNode(this.el);
        return super.remove();
    }
}