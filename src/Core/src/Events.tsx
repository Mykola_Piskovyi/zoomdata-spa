import * as Backbone from 'backbone';
import {extend} from 'underscore';

export default class Events {
    constructor() {
        extend(this, Backbone.Events);
    }

    on(eventName: string, callback?: Function, context?: any): any { return; }
    off(eventName?: string, callback?: Function, context?: any): any { return; }
    trigger(eventName: string, ...args: any[]): any { return; }
    bind(eventName: string, callback: Function, context?: any): any { return; }
    unbind(eventName?: string, callback?: Function, context?: any): any { return; }

    once(events: string, callback: Function, context?: any): any { return; }
    listenTo(object: any, events: string, callback: Function): any { return; }
    listenToOnce(object: any, events: string, callback: Function): any { return; }
    stopListening(object?: any, events?: string, callback?: Function): any { return; }
}