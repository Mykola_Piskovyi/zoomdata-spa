import MVC from './MVC';
import {default as BrowserHistory, HistoryOptions} from './BrowserHistory';

export abstract class Router extends MVC.AppRouter {

    // TODO: remove it, after zoomdata will start to use Application from spa-core
    public start(options?: HistoryOptions) {
        BrowserHistory.start(options);
    }

    public stop() {
        BrowserHistory.stop();
    }
}
