import * as Backbone from 'backbone';

export type HistoryOptions = Backbone.HistoryOptions;
export default Backbone.history;