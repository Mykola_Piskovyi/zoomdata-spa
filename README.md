# Zoomdata Web Components Library

### Installation
```sh
$ git clone [git-repo-url] zoomdata-spa
$ npm install
$ npm run build
```

### How to use

```typescript
// ES6, TS
import * as SPA from 'zoomdata-spa';
import {Core, Api, Webmodules} from 'zoomdata-spa';

const {ZoomdataDependency, InventoryDAO} = SPA.Api;

// Import Interface, Type or Enum from API
import {InventoryDAO, TInventoryQuery, EIncludedItems} from 'zoomdata-spa/dist/Api';

const foo = (item: EIncludedItems): TInventoryQuery => {...}
```

```javascript
// ES5
var SPA = require('zoomdata-spa');
var Webmodules = SPA.Webmodules;
var ZoomdataDependency = SPA.API.ZoomdataDependency;
```

