var path = require('path');
var webpack = require('webpack');
var Clean = require('clean-webpack-plugin');
var Copy = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var OUT_PATH = 'dist';

var WebpackConfig = require('./webpack.config.js');

module.exports = {

    entry: WebpackConfig.entry,
    output: WebpackConfig.output,

    devtool: 'eval-source-map',

    watchOptions: {
		aggregateTimeout: 300
	},

    resolve: WebpackConfig.resolve,
    module: WebpackConfig.module,
    externals: WebpackConfig.externals,

    plugins: [
		// clean all data in dist before
		new Clean([OUT_PATH]),
		new webpack.optimize.DedupePlugin(),
        new ExtractTextPlugin('main.css', {
			allChunks: true
		}),
        new Copy([
            { from: './src/Core/package.json', to: 'Core' },
            { from: './src/Api/package.json', to: 'Api' },
            { from: './src/Webmodules/package.json', to: 'Webmodules' },
            { from: './src/index.js' }
        ]),
		new webpack.DefinePlugin({
			'process.env': {'NODE_ENV': JSON.stringify(process.env.NODE_ENV)}
		})
	]

};
