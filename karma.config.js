var path = require('path');
var webpack = require('webpack');
var CleanPlugin = require('clean-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var WEBPACK_CONFIG_DEV = require('./webpack.config.dev');
var REPORT_PATH = WEBPACK_CONFIG_DEV.output.path + '/reports';

process.env.NODE_ENV = 'test';

// --TARGET=API
var FILES = {
    CORE: ['./src/Core/tests/**/*.spec.*'],
    API: ['./src/Api/tests/**/*.unit.*'],
    WEBMODULES: ['./src/Webmodules/**/tests/*.spec.*']
};

var PREPROC = {
    API: {
        './src/Api/tests/**/*.unit.*': ['webpack'],
        './src/Api/!(tests)/**/*.*': ['webpack', 'sourcemap', 'sourcemap-writer', 'coverage']
    },
    CORE: {
        './src/Core/tests/**/*.unit.*': ['webpack'],
        './src/Core/!(tests)/**/*.*': ['webpack', 'coverage']
    },
    WEBMODULES: {
        './src/Webmodules/**/tests/*.spec.*': ['webpack'],
        './src/Webmodules/**/!(tests)/*.*': ['webpack', 'coverage']
    }
};

module.exports = function(config) {

    console.log('-=== Start Test for: ' + config.TARGET + ' ===-');

    config.set({

        basePath: './',

        frameworks: ['mocha', 'chai', 'sinon'],

        files: FILES[config.TARGET],

        exclude: [
            '/node_modules/'
        ],

        webpack: {
            devtool: 'eval',
            resolve: WEBPACK_CONFIG_DEV.resolve,
            module: Object.assign(WEBPACK_CONFIG_DEV.module, {
                postLoaders: [{
                    test: /\.ts?$|\.tsx?$|\.es6?$|\.jsx?$/,
                    exclude: /(test|tests|node_modules)/,
                    loader: 'istanbul-instrumenter'
                }]
            }),
            plugins: [
                new CleanPlugin([REPORT_PATH]),
                new ExtractTextPlugin('main.css', {
                    allChunks: true
                })
            ],
            externals: {
                'jsdom': 'window',
                'cheerio': 'window',
                'react/lib/ExecutionEnvironment': true,
                'react/addons': true,
                'react/lib/ReactContext': 'window'
            }
        },

        webpackMiddleware: {
            noInfo: true,
            stats: 'errors-only'
        },

        preprocessors: PREPROC[config.TARGET],

        browsers: ['PhantomJS'],

        reporters: ['mocha', 'coverage', 'junit'],

        junitReporter: {
            outputDir: REPORT_PATH // results will be saved as $outputDir/$browserName.xml
        },

        coverageReporter: {
            dir: REPORT_PATH +'/coverage',
            reporters: [
                { type: 'html', subdir: 'html' },
                { type: 'text', subdir: 'text', file: 'text.txt' },
                { type: 'cobertura', subdir: 'cobertura', file: 'cobertura.xml' }
            ]
        },

        port: 1234,

        browserNoActivityTimeout: 16000,

        colors: true,

        logLevel: config.LOG_INFO,

        autoWatch: true,

        singleRun: false,

        concurrency: Infinity

    });
};
